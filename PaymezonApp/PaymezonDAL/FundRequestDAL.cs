﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PaymezonDAL
{
   public class FundRequestDAL
    {

       public static List<Fund_Request> Fund_Request_List_byId(long ID, string fromdate, string todate)
       {
           List<Fund_Request> listfund = new List<Fund_Request>();
           try
           {  
               using (var db = new PaymezonDBEntities1())
               {
                   if (todate == "")
                   {
                        DateTime frdate = new DateTime();
                        //frdate = Convert.ToDateTime(DateTime.ParseExact(fromdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        frdate = System.DateTime.Now.Date;

                        listfund = db.Fund_Request.Where(d => d.Is_Deleted == false && d.Status != "Success" && d.Decline_Reason == null && d.Fk_User_Id == ID && DbFunctions.TruncateTime(d.Created_Date) <= frdate.Date && DbFunctions.TruncateTime(d.Created_Date) >= frdate.Date).ToList();
                   }
                   else 
                   {
                        DateTime frdate = new DateTime();
                        frdate = Convert.ToDateTime(DateTime.ParseExact(fromdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        DateTime trdate = new DateTime();
                        trdate = Convert.ToDateTime(DateTime.ParseExact(todate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                        listfund = db.Fund_Request.Where(d => d.Is_Deleted == false && d.Status != "Success" && d.Decline_Reason == null && d.Fk_User_Id == ID && DbFunctions.TruncateTime(d.Created_Date) <= trdate.Date && DbFunctions.TruncateTime(d.Created_Date) >= frdate.Date).ToList();
                   }
                   return listfund;
                }
           }
           catch (Exception ex)
           {

               return listfund;
           }

       }

       public static UserRegistration usr_detail_byId(long ID)
       {
          
           try
           {

               using (var db = new PaymezonDBEntities1())
               {
                   return db.UserRegistrations.Where(d => d.Pk_Register_ID == ID).FirstOrDefault();
               }

           }
           catch (Exception ex)
           {
               throw ex;
           }

       }

       public static List<Fund_Request> Success_Fund_List(string fdate,string tdate)
       {
           try
           {
               List<Fund_Request> lstfnd = new List<Fund_Request>();
               if (tdate == "")
               {
                    //DateTime Frdate = new DateTime();
                    //Frdate = Convert.ToDateTime(DateTime.ParseExact(fdate, "MM/dd/yyyy", CultureInfo.InvariantCulture));
                   
                    using (var db = new PaymezonDBEntities1())
                   {
                        DateTime Frdate = System.DateTime.Today.Date;
                        lstfnd = db.Fund_Request.Where(d => d.Is_Deleted == false && d.Status == "Success" && DbFunctions.TruncateTime(d.Request_date_time) <= Frdate && DbFunctions.TruncateTime(d.Request_date_time) >= Frdate).OrderByDescending(c=> c.Created_Date).ToList();
                   }
               }
               else
               {
                    DateTime Frdate = new DateTime();
                    Frdate = Convert.ToDateTime(DateTime.ParseExact(fdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                    DateTime trdate = new DateTime();
                    trdate = Convert.ToDateTime(DateTime.ParseExact(fdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                    using (var db = new PaymezonDBEntities1())
                   {
                        lstfnd = db.Fund_Request.Where(d => d.Is_Deleted == false && d.Status == "Success" && DbFunctions.TruncateTime(d.Request_date_time)  <= trdate && DbFunctions.TruncateTime(d.Request_date_time) >= Frdate ).OrderByDescending(c => c.Request_date_time).ToList();
                   }
               }
                return lstfnd;
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
    }
}
