﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace PaymezonDAL
{
    public class RegistrationDAL
    {
        public static UserRegistration CheckUser(string userName, string pass)
        {
            UserRegistration usr = new UserRegistration();
            try
            {
                

                using (var db = new PaymezonDBEntities1())
                {
                     usr = db.UserRegistrations.Include(c => c.User_Amount).SingleOrDefault(c => c.Author_Email == userName && c.Password == pass);
                  }

            }
            catch (Exception ex)
            {
               // throw ex;
            }
            return usr;
        }

        public static UserRegistration GetUserByIP(string userName, string pass, string ip)
        {
            UserRegistration reg = new UserRegistration();
            try
            {

                using (var db = new PaymezonDBEntities1())
                {
                    reg = db.UserRegistrations.Include(c => c.User_Amount).Include(d => d.Transaction_Table)
                        .SingleOrDefault(c => c.Author_Email == userName && c.Password == pass && c.Ip_Address_Detail.Any(d => d.fk_user_Id == c.Pk_Register_ID && (d.Ip_Five == ip || d.Ip_Four == ip || d.Ip_One == ip || d.Ip_Three == ip || d.Ip_Two == ip)));
                    return reg;
                }

            }
            catch (Exception ex)
            {
                return reg;
            }
           // return reg;
        }

        public static List<UserRegistration> DDLUser(string type)
        {
            List<UserRegistration> lst = new List<UserRegistration>();
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    lst = db.UserRegistrations.AsEnumerable().Where(c => c.User_Type == type).Select(c => new UserRegistration()
                     {
                         Pk_Register_ID = c.Pk_Register_ID,
                         Label_Id = c.Label_Id
                         //Author_Name = c.Author_Name
                     }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public static List<UserRegistration> UserDetail()
        {
            List<UserRegistration> lst = new List<UserRegistration>();
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    lst = db.UserRegistrations.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public static UserRegistration CheckUserbyId(long id)
        {
            UserRegistration usr = new UserRegistration();
            try
            {
                //bool exist = false;

                using (var db = new PaymezonDBEntities1())
                {
                    // exist = db.UserRegistrations.Any(c => c.Author_Email == userName && c.Password == pass);
                    usr = db.UserRegistrations.Include(c => c.User_Amount).Include(d=> d.Transaction_Table).SingleOrDefault(c => c.Pk_Register_ID == id);

                    // return exist;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return usr;
        }
    }
}
