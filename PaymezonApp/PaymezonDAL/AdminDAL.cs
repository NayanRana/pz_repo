﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymezonDAL
{
    public class AdminDAL
    {
        public static bool CheckUser(string userName, string pass)
        {
            bool exist = false;
            try
            {
              
                using (var db = new PaymezonDBEntities1())
                {
                    exist = db.UserRegistrations.Any(c => c.Author_Email == userName && c.Password == pass);
                    return exist;
                }

            }
            catch (Exception ex)
            {
                return exist;
            }

        }
        public bool DeleteAPI(long Apiid)
        {
            using (var db = new PaymezonDBEntities1())
            {
                var data = db.Api_Detail.Where(d => d.Api_Id == Apiid).FirstOrDefault();
                if (data != null)
                {
                    data.Is_Deleted = true;

                    //data.Modified_By = 
                    //db.BATCH_MASTER.Remove(data);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool Deleteuser(long userid)
        {
            using (var db = new PaymezonDBEntities1())
            {
                var data = db.UserRegistrations.Where(d => d.Pk_Register_ID == userid).FirstOrDefault();
                if (data != null)
                {
                    data.Is_Deleted = true;
                    data.Is_Active = false;
                    data.Modified_By = 0;
                    data.Modified_Date = System.DateTime.Now;
                    //db.BATCH_MASTER.Remove(data);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool DeleteSupport(long id)
        {
            using (var db = new PaymezonDBEntities1())
            {
                var data = db.Support_Detail.Where(d => d.Pk_Support_Id == id).FirstOrDefault();
                if (data != null)
                {
                    data.Is_Deleted = true;

                    //data.Modified_By = 
                    //db.BATCH_MASTER.Remove(data);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static List<Fund_Request> FundRequestList(string fromdate, string todate)
        {
            List<Fund_Request> listfund = new List<Fund_Request>();
            try
            {
              

                using (var db = new PaymezonDBEntities1())
                {
                 
                    if (todate == "")
                    {
                         DateTime frdate = System.DateTime.Today.Date;
                        listfund = db.Fund_Request.Where(d => d.Status != "Success" && d.Decline_Reason == null && DbFunctions.TruncateTime(d.Request_date_time) <= frdate && DbFunctions.TruncateTime(d.Request_date_time) >= frdate).ToList();

                        
                    }
                    else 
                    {
                      
                        DateTime frdate = new DateTime();
                        frdate = Convert.ToDateTime(DateTime.ParseExact(fromdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                        DateTime Trdate = new DateTime();
                        Trdate = Convert.ToDateTime(DateTime.ParseExact(todate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                        listfund = db.Fund_Request.Where(d => d.Status != "Success" && DbFunctions.TruncateTime(d.Request_date_time) <= Trdate &&  DbFunctions.TruncateTime(d.Request_date_time) >= frdate  && d.Decline_Reason == null).ToList();
                    }


                    return listfund;
                }

            }
            catch (Exception ex)
            {
                return listfund;
            }

        }
        public static List<Api_Detail> Api_List()
        {
            try
            {

                using (var db = new PaymezonDBEntities1())
                {
                    return db.Api_Detail.Where(d => d.Is_Deleted == false).ToList();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public Api_Detail GetApiByid(long Apiid)
        {
            Api_Detail objsub = new Api_Detail();
            using (var db = new PaymezonDBEntities1())
            {
                objsub = db.Api_Detail.Where(d => d.Api_Id == Apiid).FirstOrDefault();

            }
            return objsub;
        }
        public Api_Detail getapibyapiname(string apiname)
        {
            Api_Detail objsub = new Api_Detail();
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    objsub = db.Api_Detail.Where(d => d.Api_Name == apiname && d.Is_Deleted == false).FirstOrDefault();
                }
                return objsub;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Fund_Request GetfndByid(long FndId)
        {
            Fund_Request objsub = new Fund_Request();
            using (var db = new PaymezonDBEntities1())
            {
                objsub = db.Fund_Request.Where(d => d.Pk_Fund_Request_Id == FndId).FirstOrDefault();
            }
            return objsub;
        }
        public string UpdateReason(Fund_Request data)
        {
            using (var db = new PaymezonDBEntities1())
            {
                var datamodify = db.Fund_Request.Where(d => d.Pk_Fund_Request_Id == data.Pk_Fund_Request_Id).FirstOrDefault();

                datamodify.Decline_Reason = data.Decline_Reason;

                db.SaveChanges();
                return "suc";
            }
        }

        public string UpdateAnswer(Support_Detail data)
        {
            using (var db = new PaymezonDBEntities1())
            {
                var datamodify = db.Support_Detail.Where(d => d.Pk_Support_Id == data.Pk_Support_Id).FirstOrDefault();

                datamodify.Answer_By = data.Answer_By;
                datamodify.Answer_Date = System.DateTime.Now;
                db.SaveChanges();
                return "suc";
            }
        }
        public string UpdateAPI(Api_Detail data)
        {
            using (var db = new PaymezonDBEntities1())
            {
                var datamodify = db.Api_Detail.Where(d => d.Api_Id == data.Api_Id).FirstOrDefault();

                datamodify.Api_Name = data.Api_Name;
                datamodify.Login_Id = data.Login_Id;
                datamodify.Password = data.Password;
                datamodify.Request_Url = data.Request_Url;
                datamodify.Response_Url = data.Response_Url;
                datamodify.Response_Type = data.Response_Type;
                datamodify.StatusCode = data.StatusCode;
                datamodify.SuccessCode = data.SuccessCode;
                datamodify.FailuerCode = data.FailuerCode;
                datamodify.PendingCode = data.PendingCode;
                datamodify.UniqueTransectionid = data.UniqueTransectionid;
                datamodify.Transectionnumber = data.Transectionnumber;
                datamodify.Message = data.Message;
                datamodify.Type = data.Type;
                datamodify.Number = data.Number;
                datamodify.Circle_Code = data.Circle_Code;
                datamodify.Pin = data.Pin;
                datamodify.User_Transaction = data.User_Transaction;
                datamodify.Format = data.Format;
                datamodify.Token = data.Token;
                datamodify.Provider = data.Provider;
                datamodify.Agent_Id = data.Agent_Id;
                db.SaveChanges();
                return "suc";
            }
        }
        public string UpdateAmount(User_Amount data)
        {
            using (var db = new PaymezonDBEntities1())
            {
                User_Amount datamodify = db.User_Amount.Where(d => d.Fk_User_ID == data.Fk_User_ID).FirstOrDefault();
                if (datamodify == null)
                {
                    User_Amount usr = new User_Amount();
                    usr.Fk_User_ID = data.Fk_User_ID;
                    usr.Total_Credit = data.Total_Credit;
                    usr.Total_Debit = 0;
                    usr.Actual_Amount = data.Total_Credit;
                    usr.Updated_Date = System.DateTime.Now;
                    db.User_Amount.Add(usr);
                    db.SaveChanges();
                    return usr.Actual_Amount.ToString();
                }
                else
                {
                    datamodify.Total_Credit = datamodify.Total_Credit + data.Total_Credit;
                    datamodify.Actual_Amount = datamodify.Actual_Amount + data.Total_Credit;
                    datamodify.Updated_Date = data.Updated_Date;
                    db.SaveChanges();
                    return datamodify.Actual_Amount.ToString();
                }
               
            }
        }
        public string UpdateFund(Fund_Request data)
        {
            using (var db = new PaymezonDBEntities1())
            {
                Fund_Request datamodify = db.Fund_Request.Where(d => d.Pk_Fund_Request_Id == data.Pk_Fund_Request_Id).FirstOrDefault();
                datamodify.Status = "Success";

                db.SaveChanges();
                return "suc";
            }
        }
    }
}
