﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymezonDAL
{
   public class CompanyDAL
    {
       public static List<Company_Detail> Company_List()
       {
           try
           {

               using (var db = new PaymezonDBEntities1())
               {
                   return db.Company_Detail.Where(d => d.Is_deleted == false).ToList();
               }

           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       public static List<Company_Detail> CompanyListorderbyname()
       {
           try
           {

               using (var db = new PaymezonDBEntities1())
               {
                   return db.Company_Detail.Where(d => d.Is_deleted == false).OrderBy(d=> d.Company_Name).ToList();
               }

           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       
       public Company_Detail GetCompanyByid(long id)
       {
           Company_Detail objsub = new Company_Detail();
           using (var db = new PaymezonDBEntities1())
           {
               objsub = db.Company_Detail.Where(d => d.Pk_Company_Id == id).FirstOrDefault();

           }
           return objsub;
       }
       public bool DeleteCompany(long id)
       {
           using (var db = new PaymezonDBEntities1())
           {
               var data = db.Company_Detail.Where(d => d.Pk_Company_Id == id).FirstOrDefault();
               if (data != null)
               {
                   data.Is_deleted = true;

                   //data.Modified_By = 
                   //db.BATCH_MASTER.Remove(data);
                   db.SaveChanges();
                   return true;
               }
               else
               {
                   return false;
               }
           }
       }

       public string UpdateCompany(Company_Detail data)
       {
           using (var db = new PaymezonDBEntities1())
           {
               var datamodify = db.Company_Detail.Where(d => d.Pk_Company_Id == data.Pk_Company_Id).FirstOrDefault();

               datamodify.Company_Name = data.Company_Name;
               datamodify.Company_Code = data.Company_Code;
               datamodify.Company_Detail1 = data.Company_Detail1;       
               db.SaveChanges();
               return "suc";
           }
       }
    }

}
