﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymezonDAL
{
    public class LogDetailDAL
    {
        public static bool AddLog(LogDetail log)
        {
            bool ty = false;
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    db.LogDetails.Add(log);
                    db.SaveChanges();
                    ty = true;
                }
            }
            catch (Exception ex)
            {
                ty = false;
            }
            return ty;
        }
    }
}
