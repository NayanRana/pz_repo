﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymezonDAL
{
   public class ALLReportDAL
    {
      
       public static List<User_Wallet> ListWallet_By_Id(long ID,string fdate,string tdate)
       {
           List<User_Wallet> listwall = new List<User_Wallet>();
           try
           {

               using (var db = new PaymezonDBEntities1())
               {
                   if (tdate == null)
                   {
                        //DateTime Frdate = new DateTime();
                        //Frdate = Convert.ToDateTime(DateTime.ParseExact(fdate, "MM/dd/yyyy", CultureInfo.InvariantCulture));
                        DateTime Frdate = System.DateTime.Today.Date;
                        listwall = db.User_Wallet.Where(d => d.Fk_User_Id == ID && d.Description.Contains("Failure") && DbFunctions.TruncateTime(d.Created_Date) <= Frdate && DbFunctions.TruncateTime(d.Created_Date) >= Frdate).OrderByDescending(c => c.Pk_User_ID).ToList();
                   }
                   else
                   {
                        DateTime Frdate = new DateTime();
                        Frdate = Convert.ToDateTime(DateTime.ParseExact(fdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        DateTime trdate = new DateTime();
                        trdate = Convert.ToDateTime(DateTime.ParseExact(tdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                        listwall = db.User_Wallet.Where(d => d.Fk_User_Id == ID && d.Description.Contains("Failure") && DbFunctions.TruncateTime(d.Created_Date) <= trdate && DbFunctions.TruncateTime(d.Created_Date) >= Frdate).OrderByDescending(c => c.Pk_User_ID).ToList();
                   }
                   return listwall;
               }

           }
           catch (Exception ex)
           {
               return listwall;
           }

       }


       public static List<Vendor_InptOutPut_ReqDetails> jioresponsereport(string fdate, string tdate)
       {
           List<Vendor_InptOutPut_ReqDetails> listjio = new List<Vendor_InptOutPut_ReqDetails>();
           try
           {

               using (var db = new PaymezonDBEntities1())
               {
                   if ( tdate == "")
                   {
                        DateTime Frdate = System.DateTime.Today.Date;
                       listjio = db.Vendor_InptOutPut_ReqDetails.Where(d => d.Service_name == "JioApi" && DbFunctions.TruncateTime(d.ResDate) <= Frdate && DbFunctions.TruncateTime(d.ResDate) >= Frdate).OrderByDescending(c => c.S_No).ToList();
                       
                   }
                   else
                   {
                        DateTime Frdate = new DateTime();
                        Frdate = Convert.ToDateTime(DateTime.ParseExact(fdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        DateTime Trdate = new DateTime();
                        Trdate = Convert.ToDateTime(DateTime.ParseExact(tdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                        listjio = db.Vendor_InptOutPut_ReqDetails.Where(d => d.Service_name == "JioApi" && DbFunctions.TruncateTime(d.ResDate) <= Trdate && DbFunctions.TruncateTime(d.ResDate) >= Frdate).OrderByDescending(c => c.S_No).ToList();
                    
                   }
                   return listjio;
               }
           }
           catch (Exception)
           {
               
               throw;
           }
       }

       public static List<Transaction_Table> listtransaction() 
       {
           try
           {
               using (var db = new PaymezonDBEntities1())
               {
                    DateTime todaydate = System.DateTime.Now.Date;
                  var listtransaction = db.Transaction_Table.AsEnumerable().Select(g => new Transaction_Table
                   {
                       Created_By = g.Created_By,
                       username = db.UserRegistrations.FirstOrDefault(d=> d.Pk_Register_ID == g.Created_By).Company_Name,
                       emailid = db.UserRegistrations.FirstOrDefault(d=> d.Pk_Register_ID == g.Created_By).Author_Email,
                       Request_Amount =db.Transaction_Table.Where(d=> d.Created_By == g.Created_By && d.Transaction_Date_Time == todaydate).Select(d=> d.Request_Amount).Sum(),
                       SuccessAmount = db.Transaction_Table.Where(d=> d.Created_By == g.Created_By && d.Status.ToUpper() =="SUCCESS" && d.Transaction_Date_Time == todaydate).Select(d=> d.Request_Amount).Sum(),
                       FailureAmount = db.Transaction_Table.Where(d=> d.Created_By == g.Created_By && d.Status.ToUpper() =="FAILED").Select(d=> d.Request_Amount).Sum(),
                       totalcredit = db.User_Wallet.Where(d=> d.Fk_User_Id == g.Created_By && d.Created_Date == todaydate ).Select(d=> d.Credit_Amount).Sum(),
                       totaldebit = db.User_Wallet.Where(d=> d.Fk_User_Id == g.Created_By && d.Created_Date == todaydate ).Select(d=> d.Debit_Amount).Sum(),
                       Total_Commission =db.Transaction_Table.Where(d=> d.Created_By == g.Created_By && d.Status.ToUpper() =="SUCCESS" && d.Transaction_Date_Time == todaydate).Select(d=> d.Total_Commission).Sum(),
                      Final_Bal_Amount = db.User_Amount.FirstOrDefault(d=> d.Fk_User_ID == g.Created_By).Actual_Amount
                  

                   }).ToList();

                  return listtransaction;

               }

           }
           catch (Exception ex)
           {
               
               throw ex;
           }
       }
       public static object AccountStatement()
       {

           try
           {
               using (var db = new PaymezonDBEntities1())
               {
                   var statement = db.Transaction_Table.Select(g => new Transaction_Table
                   {
                       Transaction_Date_Time = g.Transaction_Date_Time,
                       Transcatio_Id = g.Transcatio_Id,
                       Service_Name = g.Service_Name,
                       MobileNo = g.MobileNo,
                       Net_Amount = g.Net_Amount,
                       Status = g.Status,
                       //creditamount = db.User_Amount.Where(d=> d.Fk_User_ID == Id && d.de).Select()
                       Final_Bal_Amount = g.Final_Bal_Amount,
                                        }).ToList();

                   return statement;
               }
           }
           catch (Exception ex)
           {

               throw ex;
           }
       }
       public static object AccountStatement_ById(long Id)
       {

           try
           {
               using (var db = new PaymezonDBEntities1())
               {
                   var statement = db.Transaction_Table.Where(d => d.Created_By == Id).Select(g => new Transaction_Table
                   {
                       Transaction_Date_Time = g.Transaction_Date_Time,
                       Transcatio_Id = g.Transcatio_Id,
                       Service_Name = g.Service_Name,
                       MobileNo = g.MobileNo,
                       Net_Amount = g.Net_Amount,
                       Status = g.Status,
                      // creditamount = db.User_Amount.Where(d=> d.Fk_User_ID == Id && d.de).Select()
                       Final_Bal_Amount = g.Final_Bal_Amount,




                   }).ToList();

                   return statement;
               }
           }
           catch (Exception ex)
           {
               
               throw ex;
           }
       }
       public static List<Transaction_Table> opwise_by_id(long Id)
       {
           List<Transaction_Table> tran = new List<Transaction_Table>();
           try
           {
               

               using (var db = new PaymezonDBEntities1())
               {
                   tran = db.Transaction_Table.AsEnumerable().Where(d => d.Created_By == Id).GroupBy(p => p.Service_Name).Select(g => new Transaction_Table
                   {
                      // Transaction_Date_Time = g.Single().Transaction_Date_Time,
                       Service_Name = g.Key,
                       Status = g.Count().ToString(),
                       successcount = g.Where(p => p.Status.ToUpper() == "SUCCESS").Count(),
                       SuccessAmount = g.AsEnumerable().Where(p => p.Status.ToUpper() == "SUCCESS").Sum(c => c.Request_Amount),
                       FailureAmount = g.Where(p => p.Status.ToUpper() == "FAILED" || p.Status.ToUpper() == "FAIL").Sum(c => c.Request_Amount),
                       PendingAmount = g.Where(p => p.Status.ToUpper() == "PENDING").Sum(c => c.Request_Amount),
                       CommisionAmount = g.Where(p => p.Status.ToUpper() == "SUCCESS").Sum(c => c.Total_Commission)
                       
                   }).ToList();

               }

           }
           catch (Exception ex)
           {
               throw ex;
           }
           return tran;
       }
       public static List<Transaction_Table> account_report_byID(long Id)
       {
           List<Transaction_Table> tran = new List<Transaction_Table>();
           try
           {


               using (var db = new PaymezonDBEntities1())
               {
                   tran = db.Transaction_Table.AsEnumerable().Where(d => d.Created_By == Id).GroupBy(p => p.Service_Name).Select(g => new Transaction_Table
                   {
                       Service_Name = g.Key,
                       Status = g.Count().ToString()
                   }).ToList();

               }

           }
           catch (Exception ex)
           {
               throw ex;
           }
           return tran;
       }
    }
}
