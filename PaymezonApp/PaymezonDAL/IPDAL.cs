﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PaymezonDAL
{
    public class IPDAL
    {
        public static List<Ip_Address_Detail> ListIP_By_Id(long ID)
        {
            try
            {

                using (var db = new PaymezonDBEntities1())
                {
                    return db.Ip_Address_Detail.Where(d => d.Is_Deleted == false && d.Pk_Ip_Id == ID).ToList();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public Ip_Address_Detail GetIpByid(long id)
        {
            Ip_Address_Detail objsub = new Ip_Address_Detail();
            using (var db = new PaymezonDBEntities1())
            {
                objsub = db.Ip_Address_Detail.Where(d => d.Pk_Ip_Id == id).FirstOrDefault();

            }
            return objsub;
        }
        public string UpdateIP(Ip_Address_Detail data)
        {
            using (var db = new PaymezonDBEntities1())
            {
                var datamodify = db.Ip_Address_Detail.Where(d => d.Pk_Ip_Id == data.Pk_Ip_Id).FirstOrDefault();

                datamodify.Ip_One = data.Ip_One;
                datamodify.Ip_Two = data.Ip_Two;
                datamodify.Ip_Three = data.Ip_Three;
                datamodify.Ip_Four = data.Ip_Four;
                datamodify.Ip_Five = data.Ip_Five;
                datamodify.Modify_By = data.Modify_By;
                datamodify.ModifyDate = data.ModifyDate;
                db.SaveChanges();
                return "suc";
            }
        }

        public bool CheckIP(string userName, string pass)
        {
            bool isExt = false;
            using (var db = new PaymezonDBEntities1())
            {
                try
                {
                    long id = db.UserRegistrations.SingleOrDefault(c => c.Author_Email == userName && c.Password == pass).Pk_Register_ID;                    
                    isExt =  db.Ip_Address_Detail.Any(d => d.fk_user_Id == id);                      
                }
                catch (Exception ex)
                {
                    //return false;
                }

            }
            return isExt;
        }
    }
}
