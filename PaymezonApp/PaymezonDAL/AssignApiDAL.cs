﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace PaymezonDAL
{
    public partial class Assign_Api
    {
        public string opName { get; set; }
        public string NormalApi { get; set; }
        public string ApiByAmount { get; set; }
        public string EmergencyApi { get; set; }

    }
    public class AssignApiDAL
    {

     
        public static List<Assign_Api> ListAssignApi()
        {
            List<Assign_Api> listAssignApi = new List<Assign_Api>();

            try
            {

                using (var db = new PaymezonDBEntities1())
                {
                    listAssignApi = db.Assign_Api.AsEnumerable().Select(c => new Assign_Api
                    {
                        Pk_Assign_Api_Id = c.Pk_Assign_Api_Id,
                        Grater_than_Amount = c.Grater_than_Amount,
                        Operator_Id = c.Operator_Id,
                        opName = db.Company_Detail.AsEnumerable().FirstOrDefault(e => e.Pk_Company_Id == c.Operator_Id && e.Is_deleted == false).Company_Name,
                        NormalApi = db.Api_Detail.AsEnumerable().FirstOrDefault(e => e.Api_Id == c.Normal_Api_Id).Api_Name,
                        ApiByAmount = db.Api_Detail.AsEnumerable().FirstOrDefault(e => e.Api_Id == c.Api_Id_by_Amount).Api_Name,
                        EmergencyApi = db.Api_Detail.AsEnumerable().FirstOrDefault(e => e.Api_Id == c.Emergency_Api_Id).Api_Name,
                        Is_Deleted = c.Is_Deleted,
                    }).ToList();

                }


            }
            catch (Exception ex)
            {
                return listAssignApi;
            }
            return listAssignApi;
        }

        public static bool checkoperatorforalreadyassign(int? opid)
        {
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    List<Assign_Api> asslist = new List<Assign_Api>();
                    asslist = db.Assign_Api.Where(d => d.Operator_Id == opid).ToList();
                    if (asslist.Count() > 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }

                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }
        public static bool checkoperatorforassign(int? id)
        {
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    VendorDiversion vd = new VendorDiversion();
                    List<VendorDiversion> vdlist = new List<VendorDiversion>();
                    List<Assign_Api> asslist = new List<Assign_Api>(); 
                    vd = db.VendorDiversions.Where(d => d.OperatorID == id).FirstOrDefault();

                    string opcode = vd.Operator;

                   var vdlists = db.VendorDiversions.Where(d => d.Operator == opcode).Select(c=> c.OperatorID).ToList();
                   asslist = db.Assign_Api.Where(d => vdlists.Contains(d.Operator_Id.Value)).ToList();
                   if (asslist.Count() > 0)
                   {
                       return false;
                   }
                   else
                   {
                       return true;
                   }

                   
                }


                return true;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public static List<VendorDiversion> VendorList()
        {
            List<VendorDiversion> listvendor = new List<VendorDiversion>();

            try
            {

                using (var db = new PaymezonDBEntities1())
                {

                    listvendor = db.VendorDiversions.ToList();
                }


            }
            catch (Exception ex)
            {
                return listvendor;
            }
            return listvendor;
        }

        public static List<Api_Detail> ListApi()
        {
            List<Api_Detail> listapidetail = new List<Api_Detail>();
            try
            {

                using (var db = new PaymezonDBEntities1())
                {
                    listapidetail = db.Api_Detail.Where(d=>d.Is_Deleted == false).ToList();
                }

            }
            catch (Exception ex)
            {
                return listapidetail;
            }
            return listapidetail;

        }
        public Assign_Api GetAssignApiByid(long id)
        {
            Assign_Api objsub = new Assign_Api();
            using (var db = new PaymezonDBEntities1())
            {
                objsub = db.Assign_Api.Where(d => d.Pk_Assign_Api_Id == id).FirstOrDefault();

            }
            return objsub;
        }
        public bool DeleteAssignApi(long id)
        {
            using (var db = new PaymezonDBEntities1())
            {
                var data = db.Assign_Api.Where(d => d.Pk_Assign_Api_Id == id).FirstOrDefault();
                if (data != null)
                {
                    if (data.Is_Deleted == false)
                    {
                        data.Is_Deleted = true;
                    }
                    else
                    {
                        data.Is_Deleted = false;
                    }
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static int getvendoridbycmpnyid(int? id)
        {
            int vendrid = 0;
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    Company_Detail op = new Company_Detail();
                    VendorDiversion vndr = new VendorDiversion();
                     op = db.Company_Detail.SingleOrDefault(d => d.Pk_Company_Id == id && d.Is_deleted == false);
                     string opcodde = op.Company_Code.ToString();
                     vndr = db.VendorDiversions.FirstOrDefault(d => d.Operator == opcodde);
                     vendrid = vndr.OperatorID;
               }
                return vendrid;
            }
            catch (Exception)
            {
                return vendrid = 0;
                
            }
            
        }
        public string AddAssignApi(Assign_Api data)
        {
            try
            {

                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                Assign_Api usr = new Assign_Api();
                usr.Operator_Id = data.Operator_Id;
                usr.Grater_than_Amount = data.Grater_than_Amount;
                usr.Api_Id_by_Amount = data.Api_Id_by_Amount;
                usr.Normal_Api_Id = data.Normal_Api_Id;
                usr.Emergency_Api_Id = data.Emergency_Api_Id;
                usr.Is_Deleted = true;
                usr.Created_By = 0;
                usr.Created_Date = System.DateTime.Now;

                db.Assign_Api.Add(usr);
                db.SaveChanges();
                return "suc";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateAssignApi(Assign_Api data)
        {
            using (var db = new PaymezonDBEntities1())
            {
                var datamodify = db.Assign_Api.Where(d => d.Pk_Assign_Api_Id == data.Pk_Assign_Api_Id).FirstOrDefault();

                datamodify.Operator_Id = data.Operator_Id;
                datamodify.Grater_than_Amount = data.Grater_than_Amount;
                datamodify.Api_Id_by_Amount = data.Api_Id_by_Amount;
                datamodify.Normal_Api_Id = data.Normal_Api_Id;
                datamodify.Emergency_Api_Id = data.Emergency_Api_Id;
                datamodify.Modified_By = data.Modified_By;
                datamodify.Modified_Date = data.Modified_Date;
                db.SaveChanges();
                return "suc";
            }
        }

        public static string GetApiDetail(decimal? amt, string opId)
        {
            string ApiName = "";
            try
            {
                using (var db = new PaymezonDBEntities1())
                {



                    // List<Assign_Api> lst = db.Assign_Api.Where(c => c.VendorDiversion.Operator.ToLower() == opId.ToLower() && c.Is_Deleted == true).Include(c => c.Api_Detail).Include(c => c.Api_Detail2).ToList();
                    //if (lst.FirstOrDefault().Grater_than_Amount <= amt)
                    //    ApiName = lst.FirstOrDefault().Api_Detail.Api_Name;
                    //else
                    //    ApiName = lst.FirstOrDefault().Api_Detail2.Api_Name;
                    //return ApiName;


                    //below  this code changed for  vendor diversion id to companydetail By DJ

                    //List<Transaction_Table> lst1 = new List<Transaction_Table>();
                  

                    Company_Detail comd = db.Company_Detail.SingleOrDefault(d => d.Company_Code == opId && d.Is_deleted == false);
                    DateTime date1 = System.DateTime.Now;
                    DateTime result = date1.AddMinutes(-5);
                    DateTime currdate = System.DateTime.Today.Date;

                   var lst1 = db.Transaction_Table.Where(d => d.Status == "PENDING" &&  d.Service_Name.ToLower() == comd.Company_Code.ToLower() && d.Transaction_Date_Time <= result && DbFunctions.TruncateTime(d.Transaction_Date_Time) == currdate).AsEnumerable().ToList();


                    if (comd != null)
                    {
                        List<Assign_Api> lst = db.Assign_Api.Where(c => c.Company_Detail.Company_Code.ToLower() == opId.ToLower() && c.Is_Deleted == true).Include(c => c.Api_Detail).Include(c => c.Api_Detail2).Include(c=> c.Api_Detail1).ToList();

                        if (lst.FirstOrDefault().Grater_than_Amount <= amt)
                            ApiName = lst.FirstOrDefault().Api_Detail.Api_Name;

                        else if(lst1.Count() > 5 )

                            ApiName = lst.FirstOrDefault().Api_Detail1.Api_Name;

                        else
                            ApiName = lst.FirstOrDefault().Api_Detail2.Api_Name;
                        return ApiName;

                    }
                    else 
                    {
                        return ApiName = "Sorry..Your Operator is Blocked.";
                    } 
                }
            }
            catch (Exception ex)
            {
                LogDetail log = new LogDetail();
                log.Create_Date = System.DateTime.Now;
                log.Log_Detail = ex.Message.ToString();
                log.Page_Name = "AssignapiDAL_GetApiDetail";

                LogDetailDAL.AddLog(log);

                return ApiName = "Sorry..Your Operator is Blocked.";
            }
        }
    }
}
