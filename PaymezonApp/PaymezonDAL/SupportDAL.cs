﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymezonDAL
{
    public class SupportDAL
    {
        public static List<Support_Detail> MSG_List_byId(long ID, string fromdate ,string todate)
        {

            List<Support_Detail> listsupporst = new List<Support_Detail>();
            try
            {

                using (var db = new PaymezonDBEntities1())
                {

                    if ( todate == "")
                    {
                        //   DateTime frdate = System.DateTime.Now.Date;
                        DateTime frdate = new DateTime();
                        //frdate = Convert.ToDateTime(DateTime.ParseExact(fromdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        frdate = System.DateTime.Now.Date;
                        listsupporst = db.Support_Detail.Where(d => d.Created_By == ID && d.Query_Type != null && d.Is_Deleted == false && d.Created_Date <= frdate && d.Created_Date >= frdate).OrderByDescending(c => c.Created_Date).ToList();

                    }
                    else 
                    {
                        DateTime frdate = new DateTime();
                        frdate = Convert.ToDateTime(DateTime.ParseExact(fromdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        DateTime trdate = new DateTime();
                        trdate = Convert.ToDateTime(DateTime.ParseExact(todate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                        listsupporst = db.Support_Detail.Where(d => d.Created_By == ID && d.Query_Type != null && d.Is_Deleted == false && d.Created_Date <= trdate && d.Created_Date >= frdate ).OrderByDescending(c => c.Pk_Support_Id).ToList();

                    }
                    return listsupporst;

                }

            }
            catch (Exception ex)
            {
                return listsupporst;
            }

        }
        public static List<Support_Detail> MSG_List(string fromdate)
        {
            List<Support_Detail> lst = new List<Support_Detail>();
            try
            {
                
               
                using (var db = new PaymezonDBEntities1())
                {
                    if (fromdate == "")
                    {
                        DateTime frdate = System.DateTime.Today.Date;
                        lst = db.Support_Detail.AsEnumerable().Where(d => d.Answer_By == null && d.Query_Type != null && d.Is_Deleted == false ).ToList();
                        lst = lst.OrderByDescending(c => c.Pk_Support_Id).Select(c =>
                             new Support_Detail()
                             {
                                 Amount = c.Amount,
                                 //Company_name = db.Company_Detail.FirstOrDefault(e => e.Pk_Company_Id == c.Company).Company_Name,
                                 Company_name = c.Company_name,
                                 Pk_Support_Id = c.Pk_Support_Id,
                                 Created_Date = c.Created_Date,
                                 Token_no = c.Token_no,
                                 Content_Detail = c.Content_Detail,
                                 username = db.UserRegistrations.FirstOrDefault(d => d.Pk_Register_ID == c.Created_By).Company_Name.ToString(),
                                 Mobile_Number = c.Mobile_Number,
                                 User_type = c.User_type,
                                 Query_Type = c.Query_Type,
                                 Date = c.Date
                             }).ToList();
                    }
                    else
                    {
                        
                        DateTime frdate = new DateTime();
                        frdate = Convert.ToDateTime(DateTime.ParseExact(fromdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                        //DateTime trdate = new DateTime();
                        //trdate = Convert.ToDateTime(DateTime.ParseExact(todate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                        lst = db.Support_Detail.AsEnumerable().Where(d => d.Answer_By == null && DbFunctions.TruncateTime(d.Created_Date) <= frdate && DbFunctions.TruncateTime(d.Created_Date) >= frdate && d.Query_Type != null && d.Is_Deleted == false).ToList();
                        lst = lst.OrderByDescending(c => c.Pk_Support_Id).Select(c =>
                             new Support_Detail()
                             {
                                 Amount = c.Amount,
                                 //    Company_name = db.Company_Detail.FirstOrDefault(e => e.Pk_Company_Id == c.Company).Company_Name,
                                 Company_name = c.Company_name,
                                 Pk_Support_Id = c.Pk_Support_Id,
                                 Created_Date = c.Created_Date,
                                 Token_no = c.Token_no,
                                 Content_Detail = c.Content_Detail,
                                 username = db.UserRegistrations.FirstOrDefault(d => d.Pk_Register_ID == c.Created_By).Company_Name.ToString(),
                                 Mobile_Number = c.Mobile_Number,
                                 User_type = c.User_type,
                                 Query_Type = c.Query_Type,
                                 Date = c.Date
                             }).ToList();   
                    }                 
                }
                return lst;
            }
            catch (Exception ex)
            {
                return lst;
            }

        }


        public static List<Support_Detail> MSG_List_Noti(int display, int lenght)
        {
            try
            {
                List<Support_Detail> lst = new List<Support_Detail>();
                using (var db = new PaymezonDBEntities1())
                {
                    lst = db.Support_Detail.AsEnumerable().Where(d => d.Answer_By == null && d.Query_Type != null && d.Is_Deleted == false).ToList();
                   
                    lst = lst.OrderByDescending(c => c.Pk_Support_Id).Select(c =>
                       new Support_Detail()
                       {
                           Amount = c.Amount,
                          // Company_name = db.Company_Detail.SingleOrDefault(e => e.Pk_Company_Id == c.Company).Company_Name,
                           Pk_Support_Id = c.Pk_Support_Id,
                           Created_Date = c.Created_Date,
                           Token_no = c.Token_no,
                           Content_Detail = c.Content_Detail,
                           Mobile_Number = c.Mobile_Number,
                           User_type = c.User_type,
                           Query_Type = c.Query_Type,
                           Date = c.Date
                       }).Skip(display).Take(lenght).ToList();
                }
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<Support_Detail> GetsinglesupportByid(string TokenId)
        {
            try
            {
                //Support_Detail objsub = new Support_Detail();
                using (var db = new PaymezonDBEntities1())
                {
                    return db.Support_Detail.Where(d => d.Token_no == TokenId).ToList();

                }
                //return objsub;

            }

            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
