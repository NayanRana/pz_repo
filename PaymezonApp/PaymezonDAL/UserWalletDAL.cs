﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymezonDAL
{
   public class UserWalletDAL
    {
       public static List<User_Wallet> ListWallet_By_Id(long ID,string fdate, string tdate)
       {

           List<User_Wallet> listusrwallet = new List<User_Wallet>();
           try
           {

               using (var db = new PaymezonDBEntities1())
               {
                   if ( tdate == "")
                   {
                        DateTime Frdate = new DateTime();
                        //Frdate = Convert.ToDateTime(DateTime.ParseExact(fdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        Frdate = System.DateTime.Now.Date;
                        listusrwallet = db.User_Wallet.Where(d => d.Fk_User_Id == ID && DbFunctions.TruncateTime(d.Created_Date) <= Frdate && DbFunctions.TruncateTime(d.Created_Date) >= Frdate).OrderByDescending(c => c.Created_Date).ToList();
                   }
                   else
                   {
                        DateTime Frdate = new DateTime();
                        Frdate = Convert.ToDateTime(DateTime.ParseExact(fdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                        DateTime Trdate = new DateTime();
                        Trdate = Convert.ToDateTime(DateTime.ParseExact(tdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                        listusrwallet = db.User_Wallet.Where(d => d.Fk_User_Id == ID && DbFunctions.TruncateTime(d.Created_Date) <= Trdate&& DbFunctions.TruncateTime(d.Created_Date) >= Frdate ).OrderByDescending(c => c.Created_Date).ToList();
                   }

                   return listusrwallet;
               }

           }
           catch (Exception ex)
           {
               return listusrwallet;
           }

       }
       public static List<User_Wallet> ListWallet(string fdate, string tdate)
       {
           try
           {
                List<User_Wallet> lstwl = new List<User_Wallet>();
               using (var db = new PaymezonDBEntities1())
               {
                    

                   if (tdate == "")
                   {
                        //DateTime Frdate = new DateTime();
                        //Frdate = Convert.ToDateTime(DateTime.ParseExact(fdate, "MM/dd/yyyy", CultureInfo.InvariantCulture));
                        DateTime Frdate = System.DateTime.Today.Date;
                        lstwl = db.User_Wallet.Where(d => DbFunctions.TruncateTime(d.Created_Date) <= Frdate.Date && DbFunctions.TruncateTime(d.Created_Date) >= Frdate.Date).AsEnumerable().OrderByDescending(c => c.Pk_User_ID).ToList();
                   }
                   else
                   {
                        DateTime Frdate = new DateTime();
                        Frdate = Convert.ToDateTime(DateTime.ParseExact(fdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        DateTime trdate = new DateTime();
                        trdate = Convert.ToDateTime(DateTime.ParseExact(tdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                        lstwl = db.User_Wallet.Where(d => DbFunctions.TruncateTime(d.Created_Date) <= trdate.Date && DbFunctions.TruncateTime(d.Created_Date) >= Frdate.Date).AsEnumerable().OrderByDescending(c => c.Pk_User_ID).ToList();
                   }

                    return lstwl;

               }

           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       public static decimal? GetUserBalance(long id)
       {
           try
           {

               using (var db = new PaymezonDBEntities1())
               {
                   return db.User_Amount.SingleOrDefault(d => d.Fk_User_ID == id).Actual_Amount;
               }

           }
           catch (Exception ex)
           {
               return 0;
           }

       }
    }
}
