﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymezonDAL
{
    public class RechargeDAL
    {
        public static List<Recharge_Detail> RCG_List_byId(long ID, string fromdate, string todate)
        {
            List<Recharge_Detail> listrecharge = new List<Recharge_Detail>();
            try
            {


                using (var db = new PaymezonDBEntities1())
                {



                    if(todate =="" )
                    {
                        DateTime Frdate = new DateTime();
                        // Frdate = Convert.ToDateTime(DateTime.ParseExact(fromdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        Frdate = System.DateTime.Now.Date;
                        listrecharge = db.Recharge_Detail.Where(d => d.Recharge_By == ID && DbFunctions.TruncateTime(d.Recharge_Date) <= Frdate && DbFunctions.TruncateTime(d.Recharge_Date) >= Frdate).AsEnumerable().OrderByDescending(c => c.Pk_Recharge_Id).ToList();
                    }
                    else
                    {
                        DateTime Frdate = new DateTime();
                        Frdate = Convert.ToDateTime(DateTime.ParseExact(fromdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                        DateTime Trdate = new DateTime();
                        Trdate = Convert.ToDateTime(DateTime.ParseExact(todate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                        listrecharge = db.Recharge_Detail.Where(d => d.Recharge_By == ID && DbFunctions.TruncateTime(d.Recharge_Date) <= Trdate && DbFunctions.TruncateTime(d.Recharge_Date) >= Frdate).OrderByDescending(c => c.Pk_Recharge_Id).ToList();
                    }
                    return listrecharge;
                }

            }
            catch (Exception ex)
            {
                   return listrecharge;
            }

        }
        public static List<Recharge_Detail> pendingBAL()
        {
            try
            {
                List<Recharge_Detail> lstrec = new List<Recharge_Detail>();
                using (var db = new PaymezonDBEntities1())
                {
                    lstrec= db.Recharge_Detail.Where(d => d.Status == "Pending").OrderByDescending(c => c.Pk_Recharge_Id).ToList();

                }
                return lstrec;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        public static string changestatus(string TXID,string status)
        {
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    var datamodify = db.Recharge_Detail.Where(d => d.Transaction_Id == TXID).FirstOrDefault();

                    datamodify.Status = status;
                 
                    db.SaveChanges();
                    return "suc";
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        public static string InsertRecharge(Recharge_Detail rech)
        {
            try
            {
                using (var db = new PaymezonDBEntities1())
                {

                    decimal per = 0;
                    string operatorcode = db.commision_Detail.FirstOrDefault(d => d.Operator_Name == rech.Operator_Code || d.Operator_Code == rech.Operator_Code).Operator_Code.ToString();
                   //  string id = db.CommissionPlans.FirstOrDefault(d => d.Fk_User_Id == rech.Fk_RegisterId).Fk_Commision_Id.ToString();
                   //  decimal ids = Convert.ToDecimal(id);
                    try
                    {//this code change for before commision get by plan ID now commision shuld be get by user id (Dhananjay)
                        string perc = db.commision_Detail.SingleOrDefault(c => c.Operator_Code == rech.Operator_Code && c.Fk_User_Id == rech.Fk_RegisterId).Commision_Per;
                      //   string perc = db.commision_Detail.SingleOrDefault(c => c.Operator_Code == rech.Operator_Code && c.Plan_Id == db.CommissionPlans.FirstOrDefault(d => d.Fk_User_Id == rech.Fk_RegisterId).Fk_Commision_Id).Commision_Per;
                      
                        if (perc == null)
                        {
                      
                            per = 0;
                        }
                        else
                        {
                            per = Convert.ToDecimal(perc);
                        }
                    }
                    catch (Exception ex)
                    {
                        per = 0;
                    }
                    db.Recharge_Detail.Add(rech);
                   
                    User_Amount amt = db.User_Amount.SingleOrDefault(c => c.Fk_User_ID == rech.Fk_RegisterId);
                    Transaction_Table tt = new Transaction_Table();
                    tt.Reference_Number = rech.Ref_number;
                    tt.Action_On_Amount = "Dr";
                    tt.MobileNo = Convert.ToInt64(rech.Mobile_no);
                    tt.Status = rech.Status;
                    tt.Total_Commission = (rech.Amount * per / 100);
                    tt.Request_Amount = rech.Amount;
                    tt.Net_Amount = rech.Amount - tt.Total_Commission;
                    tt.Transaction_Date_Time = rech.Recharge_Date;
                    tt.Service_Name = rech.Operator_Code;
                    tt.Service_Vendor = rech.API_Name;
                    tt.Request_IP_Address = rech.ipAdd;
                    tt.Api_transaction_Id = rech.APi_Transaction_Id;
                    tt.c_txn_id = rech.C_Tranx_ID;
                    tt.Amount_Before_Due_Date = amt.Actual_Amount;
                    tt.Amount_After_Deu_Date = amt.Actual_Amount - tt.Net_Amount;
                    tt.Final_Bal_Amount = amt.Actual_Amount - tt.Net_Amount;
                    //tt.LowerLevel = rech.APi_Transaction_Id;
                    tt.User_Type = rech.User_Type;
                    tt.Created_By = rech.Fk_RegisterId;
                    tt.Created_Date = System.DateTime.Now;
                    tt.Ref_No = rech.Transaction_Id;
                    db.Transaction_Table.Add(tt);
                    User_Wallet uw = new User_Wallet();
                   
                        uw.Created_By = rech.Fk_RegisterId;
                        uw.Created_Date = System.DateTime.Now;
                        uw.Debit_Amount = tt.Net_Amount;
                        uw.before_balance = amt.Actual_Amount;
                        uw.after_balance = amt.Actual_Amount - tt.Net_Amount;

                        uw.Description = "Recharge done on " + rech.Mobile_no + " of Rs." + rech.Amount + " and TxnID id " + rech.Transaction_Id;
                        uw.Fk_User_Id = rech.Fk_RegisterId;
                        uw.Fund_Amount = null;
                        uw.Label_Id = rech.Agent_Id;
                        uw.User_Name = db.UserRegistrations.SingleOrDefault(c => c.Pk_Register_ID == rech.Fk_RegisterId).Author_Name;
                        uw.Api_Name = rech.API_Name;
                        db.User_Wallet.Add(uw);

                        amt.Actual_Amount = amt.Actual_Amount - tt.Net_Amount;
                        amt.Total_Debit = amt.Total_Debit + tt.Net_Amount;
                        amt.Updated_Date = System.DateTime.Now;
                    
                
                   
                    db.SaveChanges();
                  
                    return amt.Actual_Amount.ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
                
            }
        }


    }
}
