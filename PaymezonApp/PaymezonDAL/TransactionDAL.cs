﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Globalization;

namespace PaymezonDAL
{
    public class TransactionDAL
    {
        public static List<Transaction_Table> Transaction_List(long id,string fdate, string tdate)
        {
            try
            {
                List<Transaction_Table> lst = new List<Transaction_Table>();
                using (var db = new PaymezonDBEntities1())
                {
                    if(tdate == "")
                    {
                        DateTime Frdate = new DateTime();
                        //Frdate = Convert.ToDateTime(DateTime.ParseExact(fdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        Frdate = System.DateTime.Now.Date;

                        lst = db.Transaction_Table.Where(d => d.Created_By == id && DbFunctions.TruncateTime(d.Created_Date) <= Frdate && DbFunctions.TruncateTime(d.Created_Date) >= Frdate).AsEnumerable().OrderByDescending(c => c.Transcatio_Id)
                        .Select(c => new Transaction_Table()
                        {
                            Created_Date = c.Created_Date,
                            Service_Name = db.Company_Detail.SingleOrDefault(s => s.Company_Code == c.Service_Name && s.Is_deleted == false).Company_Name,
                            Api_transaction_Id = c.Api_transaction_Id,  
                            MobileNo = c.MobileNo,
                            Net_Amount = c.Net_Amount,
                            Reference_Number = c.Reference_Number,
                            Request_Amount = c.Request_Amount,
                            Request_IP_Address = c.Request_IP_Address,
                            Status = c.Status,
                            Amount_Before_Due_Date = c.Amount_Before_Due_Date,
                            Final_Bal_Amount = c.Final_Bal_Amount,
                            Transcatio_Id = c.Transcatio_Id,
                            Ref_No = c.Ref_No,
                           // Ref_No = db.Recharge_Detail.OrderByDescending(s=> s.Pk_Recharge_Id).FirstOrDefault(e => e.Ref_number == c.Reference_Number).Transaction_Id,
                           // Update_Date = db.Recharge_Detail.OrderByDescending(s => s.Pk_Recharge_Id).FirstOrDefault(e => e.Ref_number == c.Reference_Number).Response_Date,
                        }).ToList();
                    }
                    else
                    {
                        DateTime Frdate = new DateTime();
                        Frdate = Convert.ToDateTime(DateTime.ParseExact(fdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                        DateTime Trdate = new DateTime();
                        Trdate = Convert.ToDateTime(DateTime.ParseExact(tdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                        lst = db.Transaction_Table.Where(d => d.Created_By == id && DbFunctions.TruncateTime(d.Created_Date) <= Trdate && DbFunctions.TruncateTime(d.Created_Date) >= Frdate).AsEnumerable().OrderByDescending(c => c.Transcatio_Id)
                        .Select(c => new Transaction_Table()
                        {
                            Created_Date = c.Created_Date,
                            Service_Name = db.Company_Detail.SingleOrDefault(s => s.Company_Code == c.Service_Name && s.Is_deleted == false).Company_Name,
                            Api_transaction_Id = c.Api_transaction_Id,
                            MobileNo = c.MobileNo,
                            Net_Amount = c.Net_Amount,
                            Reference_Number = c.Reference_Number,
                            Request_Amount = c.Request_Amount,
                            Request_IP_Address = c.Request_IP_Address,
                            Status = c.Status,
                            Amount_Before_Due_Date = c.Amount_Before_Due_Date,
                            Final_Bal_Amount = c.Final_Bal_Amount,
                            Transcatio_Id = c.Transcatio_Id,
                            Ref_No = c.Ref_No,
                            // Ref_No = db.Recharge_Detail.OrderByDescending(s=> s.Pk_Recharge_Id).FirstOrDefault(e => e.Ref_number == c.Reference_Number).Transaction_Id,
                            // Update_Date = db.Recharge_Detail.OrderByDescending(s => s.Pk_Recharge_Id).FirstOrDefault(e => e.Ref_number == c.Reference_Number).Response_Date,
                        }).ToList();
                    }
                  
                }
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static List<Transaction_Table> Transaction_Listforvishnuapi()
        {
            try
            {
                List<Transaction_Table> listtran = new List<Transaction_Table>();
                 using (var db = new PaymezonDBEntities1())
                 {
                     listtran = db.Transaction_Table.AsEnumerable().Where(d => d.Status == "PENDING" && d.Service_Vendor == "VishnuAPI").Select(c => new Transaction_Table
                     {
                         Ref_No = c.Ref_No,
                         MobileNo = c.MobileNo,
                         Request_Amount = c.Request_Amount,
                         Service_Name = db.VendorDiversions.FirstOrDefault(p => p.Operator == c.Service_Name && p.ServiceProvider == "VishnuAPI").OperatorCode
                     }).ToList();

                     return listtran;
                 }

            }
            catch (Exception ex) 
            {
                
                throw ex;
            }
        }
        public static List<Transaction_Table> Transaction_Listdata(string fdate,string tdate)
        {
            try
            {
                List<Transaction_Table> lst = new List<Transaction_Table>();
                using (var db = new PaymezonDBEntities1())
                {
                    if (tdate == "")
                    {
                        DateTime Frdate = System.DateTime.Today.Date;
                        lst = db.Transaction_Table.Where(d => DbFunctions.TruncateTime(d.Transaction_Date_Time)  <= Frdate.Date && DbFunctions.TruncateTime(d.Transaction_Date_Time) >= Frdate.Date).AsEnumerable().OrderByDescending(c => c.Transcatio_Id)
                                              .Select(c => new Transaction_Table()
                                              {
                                                  Created_Date = c.Created_Date,
                                                  username = db.UserRegistrations.SingleOrDefault(s=> s.Pk_Register_ID == c.Created_By).Company_Name,
                                                  Service_Name = db.Company_Detail.SingleOrDefault(s => s.Company_Code == c.Service_Name && s.Is_deleted == false).Company_Name,
                                                  c_txn_id = c.c_txn_id,
                                                  MobileNo = c.MobileNo,
                                                  Api_transaction_Id = c.Api_transaction_Id,
                                                  Net_Amount = c.Net_Amount,
                                                  Reference_Number = c.Reference_Number,
                                                  Request_Amount = c.Request_Amount,
                                                  Request_IP_Address = c.Request_IP_Address,
                                                  Status = c.Status,
                                                  Amount_Before_Due_Date = c.Amount_Before_Due_Date,
                                                  Final_Bal_Amount = c.Final_Bal_Amount,
                                                  Transcatio_Id = c.Transcatio_Id,
                                                  Ref_No = c.Ref_No,
                                                  Service_Vendor = c.Service_Vendor,
                                                  // Ref_No = db.Recharge_Detail.OrderByDescending(s=> s.Pk_Recharge_Id).FirstOrDefault(e => e.Ref_number == c.Reference_Number).Transaction_Id,
                                                  // Update_Date = db.Recharge_Detail.OrderByDescending(s => s.Pk_Recharge_Id).FirstOrDefault(e => e.Ref_number == c.Reference_Number).Response_Date,
                                              }).ToList();
                    }
                    else
                    {
                        DateTime Frdate = new DateTime();
                        Frdate = Convert.ToDateTime(DateTime.ParseExact(fdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        DateTime toodate = new DateTime();
                        toodate = Convert.ToDateTime(DateTime.ParseExact(tdate, "dd/MM/yyyy", CultureInfo.InvariantCulture));

                        lst = db.Transaction_Table.Where(d => DbFunctions.TruncateTime(d.Transaction_Date_Time) <= toodate.Date && DbFunctions.TruncateTime(d.Transaction_Date_Time) >= Frdate.Date).AsEnumerable().OrderByDescending(c => c.Transcatio_Id)
                                                                     .Select(c => new Transaction_Table()
                                                                     {
                                                                         Created_Date = c.Created_Date,
                                                                         username = db.UserRegistrations.SingleOrDefault(s => s.Pk_Register_ID == c.Created_By).Company_Name,
                                                                        Service_Name = db.Company_Detail.SingleOrDefault(s => s.Company_Code == c.Service_Name && s.Is_deleted == false).Company_Name,                                                  
                                                                         c_txn_id = c.c_txn_id,
                                                                         MobileNo = c.MobileNo,
                                                                         Api_transaction_Id = c.Api_transaction_Id,
                                                                         Net_Amount = c.Net_Amount,
                                                                         Reference_Number = c.Reference_Number,
                                                                         Request_Amount = c.Request_Amount,
                                                                         Request_IP_Address = c.Request_IP_Address,
                                                                         Status = c.Status,
                                                                         Transcatio_Id = c.Transcatio_Id,
                                                                         Amount_Before_Due_Date = c.Amount_Before_Due_Date,
                                                                         Final_Bal_Amount = c.Final_Bal_Amount,
                                                                         Ref_No = c.Ref_No,
                                                                         Service_Vendor = c.Service_Vendor,
                                                                         // Ref_No = db.Recharge_Detail.OrderByDescending(s=> s.Pk_Recharge_Id).FirstOrDefault(e => e.Ref_number == c.Reference_Number).Transaction_Id,
                                                                         // Update_Date = db.Recharge_Detail.OrderByDescending(s => s.Pk_Recharge_Id).FirstOrDefault(e => e.Ref_number == c.Reference_Number).Response_Date,
                                                                     }).ToList();
                    }
                   
                }
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static string updateapiidbytxnid(string txnid, string apiid)
        {
            try
            {
             //   Transaction_Table tt = new Transaction_Table(); 
                   using (var db = new PaymezonDBEntities1())
                {
                   var tt = db.Transaction_Table.AsEnumerable().FirstOrDefault(d => d.Ref_No == txnid);
                    tt.Api_transaction_Id = apiid;
                    db.SaveChanges();

                    return "Succ";
                   }
                
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public static List<Transaction_Table> checkpendingtransactionofanshapi()
        {
            try
            {
                List<Transaction_Table> lst = new List<Transaction_Table>();
                using (var db = new PaymezonDBEntities1())
                {
                    DateTime date1 = System.DateTime.Now;
                    DateTime result = date1.AddMinutes(-5);
                    lst = db.Transaction_Table.Where(d => d.Status == "PENDING" && d.Transaction_Date_Time <= result && d.Service_Vendor == "AnshApi").ToList();
                    return lst;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public static List<Transaction_Table> checkpendingtransaction()
        {
            try
            {
                 List<Transaction_Table> lst = new List<Transaction_Table>();
                 using (var db = new PaymezonDBEntities1())
                 {
                     DateTime date1 = System.DateTime.Now;
                     DateTime result = date1.AddMinutes(-5);
                     //DateTime dt = System.DateTime.Now 
                     lst = db.Transaction_Table.Where(d => d.Status == "PENDING" && d.Transaction_Date_Time <= result).ToList();
                     return lst;
                 }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public static string checkRech(decimal mobile, decimal amt)
        {
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    DateTime? tt = db.Transaction_Table.OrderByDescending(d => d.Transcatio_Id).FirstOrDefault(c => c.Request_Amount == amt && c.MobileNo == mobile).Transaction_Date_Time;
                    if (tt != null)
                    {
                        DateTime? dt = tt;
                        DateTime dtNow = System.DateTime.Now;
                        ///var hours = (dt - dtNow).TotalMinutes;
                        TimeSpan varTime = ((DateTime)dtNow - (DateTime)dt);
                        if (varTime.Minutes > 2)
                        {
                            return "Succ";
                        }
                        else
                        {
                            return "Please wait for 5 min.";
                        }
                    }
                    else
                    {
                        return "Succ";
                    }


                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static string UpdateStatus(string status, string txnId, string opId)
        {
            try
            {
                string msg = "";

                using (var db = new PaymezonDBEntities1())
                {
                    if (status == "SUCCESS")
                    {
                        Recharge_Detail rec = db.Recharge_Detail.Include(d => d.UserRegistration).FirstOrDefault(c => c.Transaction_Id == txnId);
                        rec.Status = "SUCCESS";
                        rec.Response_Date = System.DateTime.Now;
                        db.SaveChanges();


                        Transaction_Table tt = db.Transaction_Table.FirstOrDefault(c => c.Reference_Number == rec.Ref_number);

                        tt.Status = "SUCCESS";
                        db.SaveChanges();

                        msg = rec.UserRegistration.Return_URL + "?tid=" + rec.C_Tranx_ID + "&status=1&opid=" + opId;
                    }
                    else if (status == "FAILED")
                    {
                        Recharge_Detail rec = db.Recharge_Detail.Include(d => d.UserRegistration).FirstOrDefault(c => c.Transaction_Id == txnId);
                        if (rec.Status == "PENDING")
                        {
                            rec.Status = "FAILED";
                            rec.Response_Date = System.DateTime.Now;
                            db.SaveChanges();
                        }
                        else
                        {
                            msg = rec.UserRegistration.Return_URL + "?tid=" + rec.C_Tranx_ID + "&status=FAILED&opid=" + opId + "&Amount=" + rec.Amount;
                        }
                        Transaction_Table tt = db.Transaction_Table.FirstOrDefault(c => c.Reference_Number == rec.Ref_number);
                        if (tt.Status == "PENDING")
                        {
                            tt.Status = "FAILED";
                            

                            User_Amount usrAmt = db.User_Amount.FirstOrDefault(c => c.Fk_User_ID == rec.Fk_RegisterId);
                            
                            User_Wallet walt = new User_Wallet();
                            walt.Created_By = rec.Fk_RegisterId;
                            walt.Created_Date = System.DateTime.Now;
                            walt.Credit_Amount = tt.Net_Amount;
                            walt.before_balance = usrAmt.Actual_Amount;
                            walt.after_balance = usrAmt.Actual_Amount + tt.Net_Amount;
                            //uw.Description = "Recharge done on " + rech.Mobile_no + " of Rs." + rech.Amount + " and TxnID id " + rech.Transaction_Id;
                            walt.Description = "Transaction Failure on " + tt.MobileNo + "of Rs." + tt.Net_Amount + " Transaction Id " + rec.Transaction_Id;
                            walt.Label_Id = rec.Agent_Id;
                            walt.Fk_User_Id = rec.Fk_RegisterId;
                            walt.User_Name = rec.UserRegistration.Author_Name;
                            db.User_Wallet.Add(walt);

                            usrAmt.Actual_Amount = usrAmt.Actual_Amount + tt.Net_Amount;
                            usrAmt.Total_Credit = usrAmt.Total_Credit + tt.Net_Amount;
                            usrAmt.Updated_Date = System.DateTime.Now;
                          
                            db.SaveChanges();
                        }
                        else 
                        {
                            msg = rec.UserRegistration.Return_URL + "?tid=" + rec.C_Tranx_ID + "&status=FAILED&opid=" + opId + "&Amount=" + rec.Amount;

                        }
                        msg = rec.UserRegistration.Return_URL + "?tid=" + rec.C_Tranx_ID + "&status=FAILED&opid=" + opId + "&Amount=" + rec.Amount;
                    }
                }
                return msg;

            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string UpdateStatusOfAnsh(string status, string Mobno)
        {
            try
            {
                string msg = "";
                decimal mob = Convert.ToDecimal( Mobno);
                using (var db = new PaymezonDBEntities1())
                {
                    if (status == "SUCCESS")
                    {
                        Recharge_Detail rec = db.Recharge_Detail.OrderByDescending(c=> c.Pk_Recharge_Id).FirstOrDefault(c => c.Mobile_no == mob && c.Status == "Pending");
                        rec.Status = "SUCCESS";
                        rec.Response_Date = System.DateTime.Now;
                        db.SaveChanges();


                        Transaction_Table tt = db.Transaction_Table.SingleOrDefault(c => c.Reference_Number == rec.Ref_number);
                        tt.Status = "SUCCESS";
                        db.SaveChanges();

                        msg = rec.UserRegistration.Return_URL + "?tid=" + rec.C_Tranx_ID + "&status=SUCCESS";
                    }
                    else if (status == "PENDING")
                    {
                        Recharge_Detail rec = db.Recharge_Detail.OrderByDescending(c => c.Pk_Recharge_Id).FirstOrDefault(c => c.Mobile_no == mob && c.Status == "Pending");
                        rec.Status = "PENDING";
                        rec.Response_Date = System.DateTime.Now;
                        db.SaveChanges();


                        Transaction_Table tt = db.Transaction_Table.SingleOrDefault(c => c.Reference_Number == rec.Ref_number);
                        tt.Status = "PENDING";
                        db.SaveChanges();

                        msg = rec.UserRegistration.Return_URL + "?tid=" + rec.C_Tranx_ID + "&status=PENDING";
                     }
                    else
                    {
                        Recharge_Detail rec = db.Recharge_Detail.OrderByDescending(c => c.Pk_Recharge_Id).FirstOrDefault(c => c.Mobile_no == mob && c.Status == "Pending");
                        rec.Status = "FAILED";
                        rec.Response_Date = System.DateTime.Now;
                        db.SaveChanges();


                        Transaction_Table tt = db.Transaction_Table.SingleOrDefault(c => c.Reference_Number == rec.Ref_number);
                        if (tt.Status == "PENDING")
                        {
                            User_Amount usrAmt = db.User_Amount.SingleOrDefault(c => c.Fk_User_ID == rec.Fk_RegisterId);
                            tt.Status = "FAILED";
                            db.SaveChanges();

                            User_Wallet walt = new User_Wallet();
                            walt.Created_By = rec.Fk_RegisterId;
                            walt.Created_Date = System.DateTime.Now;
                            walt.Credit_Amount = rec.Amount;
                            walt.before_balance = usrAmt.Actual_Amount;
                            walt.after_balance = usrAmt.Actual_Amount + tt.Net_Amount;
                            walt.Description = "Transaction Failure on " + tt.MobileNo + " of Rs. " + rec.Amount + " Transaction Id " + rec.Transaction_Id;
                            walt.Label_Id = rec.Agent_Id;
                            walt.Fk_User_Id = rec.Fk_RegisterId;
                            walt.User_Name = rec.UserRegistration.Author_Name;
                            db.User_Wallet.Add(walt);
                            db.SaveChanges();

                        
                            usrAmt.Actual_Amount = usrAmt.Actual_Amount + rec.Amount;
                            usrAmt.Total_Credit = usrAmt.Total_Credit + rec.Amount;
                            usrAmt.Updated_Date = System.DateTime.Now;
                            db.SaveChanges();

                        }
                        else
                        {
                            msg = rec.UserRegistration.Return_URL + "?tid=" + rec.C_Tranx_ID + "&status=FAILED" + "|" + rec.Amount;
                        }
                      


                        msg = rec.UserRegistration.Return_URL + "?tid=" + rec.C_Tranx_ID + "&status=FAILED" + "|" + rec.Amount;


                    }
                }
                return msg;

            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string Getrequrl(string txid) 
        {
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    //string usrid = db.Recharge_Detail.SingleOrDefault(d => d.Transaction_Id == txid).Fk_RegisterId();
                    string responseurl = db.UserRegistrations.FirstOrDefault(c => c.Recharge_Detail.FirstOrDefault(d=> d.Transaction_Id==txid).Fk_RegisterId == c.Pk_Register_ID).Return_URL;
                    return responseurl;
                }

            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

        public static Transaction_Table CheckStatusbyctxnid(string ctxnid)
        {
            Transaction_Table txntble = new Transaction_Table();
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    //string usrid = db.Recharge_Detail.SingleOrDefault(d => d.Transaction_Id == txid).Fk_RegisterId();
                    txntble = db.Transaction_Table.Where(d => d.c_txn_id == ctxnid || d.Api_transaction_Id == ctxnid).OrderByDescending(d=> d.Transcatio_Id).FirstOrDefault();
                    return txntble;
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static string gettxnbymobno(string Mobno)
     {
        decimal mobilenumber = Convert.ToDecimal(Mobno);
         try 
	        {
                using (var db = new PaymezonDBEntities1())
                {
                    //string usrid = db.Recharge_Detail.SingleOrDefault(d => d.Transaction_Id == txid).Fk_RegisterId();
                    //string txnid = db.Recharge_Detail.OrderByDescending(d => d.Pk_Recharge_Id).FirstOrDefault(c => c.Ref_number == mobno).Ref_number;
                    string txnid = db.Recharge_Detail.OrderByDescending(d => d.Pk_Recharge_Id).FirstOrDefault(c => c.Mobile_no == mobilenumber).Transaction_Id;
                    //string txnid = db.Recharge_Detail.FirstOrDefault(c => c.Mobile_no == mobno).Transaction_Id;
                    return txnid;
                }
		
	        }
            	catch (Exception ex)
	        {
		
		        throw ex;
	        }
        
    }
        public static string Gettxnidbyapitxnid(string txid)
        {
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                     string txnid = db.Recharge_Detail.OrderByDescending(d => d.Pk_Recharge_Id).FirstOrDefault(c => c.APi_Transaction_Id == txid).Transaction_Id;
                     return txnid;
                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }
        public static string Getctxid(string txid)
        {
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    //string usrid = db.Recharge_Detail.SingleOrDefault(d => d.Transaction_Id == txid).Fk_RegisterId();
                    string responseurl = db.Recharge_Detail.FirstOrDefault(c => c.Transaction_Id == txid).C_Tranx_ID;
                    return responseurl;
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
