﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymezonDAL
{
  public  class ProfileDAL
    {

      public UserRegistration Editprofile(long ID)
      {
          UserRegistration objsub = new UserRegistration();
          try
          {
              
              using (var db = new PaymezonDBEntities1())
              {
                  objsub = db.UserRegistrations.Where(d => d.Pk_Register_ID == ID).FirstOrDefault();
              }
             
          }
          catch (Exception ex)
          {

              throw ex;
          }

          return objsub;
      }
      public string Updateprofile(UserRegistration data)
      {
          using (var db = new PaymezonDBEntities1())
          {
              var datamodify = db.UserRegistrations.Where(d => d.Pk_Register_ID == data.Pk_Register_ID).FirstOrDefault();


              datamodify.Author_Email = data.Author_Email;
              datamodify.Author_MobNo = data.Author_MobNo;
              datamodify.Author_Name = data.Author_Name;
              datamodify.City = data.City;
              datamodify.Company_Name = data.Company_Name;
              datamodify.Copyright = data.Copyright;
              datamodify.Domain_Name = data.Domain_Name;
              datamodify.Label_Id = data.Label_Id;
             // datamodify.Password = data.Password;
              datamodify.Helpdesk_Email = data.Helpdesk_Email;

              datamodify.Mobile_Number = data.Mobile_Number;
              datamodify.Office_Address = data.Office_Address;
              datamodify.PAN_Num = data.PAN_Num;
              datamodify.Panel_Title = data.Panel_Title;
              datamodify.Pincode = data.Pincode;
              datamodify.Portal_FName = data.Portal_FName;

              datamodify.Portal_MName = data.Portal_MName;
              datamodify.Modified_Date = data.Modified_Date;
              datamodify.Modified_By = data.Modified_By;
              datamodify.Return_URL = data.Return_URL;
              db.SaveChanges();
              return "suc";
          }
      }


    
    }
}
