﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymezonDAL
{
    public class CommisionDAL
    {

        public static List<commision_Detail> List_Commision()
        {
            List<commision_Detail> lstcomm = new List<commision_Detail>();
            try
            {


                using (var db = new PaymezonDBEntities1())
                {
                    //lstcomm = db.commision_Detail.GroupBy(d => new { d.Plan_Id, d.Plan_Name }).ToList();
                    lstcomm = db.commision_Detail.OrderByDescending(d => d.Pk_Comm_Id).ToList();

                }
            }
            catch (Exception ex)
            {
                return lstcomm;
            }

            return lstcomm;

        }
        public static List<commision_Detail> List_CommisionByPlanname()
        {
            try
            {
                List<commision_Detail> lstcomm = new List<commision_Detail>();

                using (var db = new PaymezonDBEntities1())
                {

                    var comm = db.commision_Detail.GroupBy(p => new { p.Plan_Name, p.Plan_Id }).ToList();

                    lstcomm = db.commision_Detail.Select(o => new { Plan_Name = o.Plan_Name, Plan_Id = o.Plan_Id })
                               .GroupBy(p => new { p.Plan_Name, p.Plan_Id }).ToList()
                               .Select(fl => new commision_Detail { Plan_Name = fl.Key.Plan_Name, Plan_Id = fl.Key.Plan_Id })
                               .ToList();

                    return lstcomm;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static List<Company_Detail> getcommisionlistbyplanid(long planid)
        {
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    db.Configuration.ProxyCreationEnabled = false;
                    List<commision_Detail> commlist = db.commision_Detail.Where(d => d.Plan_Id == planid).ToList();
                    List<Company_Detail> compDet = db.Company_Detail.Where(d => d.Is_deleted == false).ToList();

                    foreach (var item in compDet)
                    {
                        commision_Detail sst = commlist.SingleOrDefault(c => c.Operator_Code == item.Company_Code);

                        item.commper = sst == null ? 0 : Convert.ToDecimal(sst.Commision_Per);
                    }

                    //string[] companycode = db.commision_Detail.Where(d => d.Plan_Id == planid).Select(e => e.Operator_Code ).ToArray();

                    //List<Company_Detail> comDet = db.Company_Detail.Where(c => companycode.Contains(c.Company_Code)).Select(g => new Company_Detail 
                    //{

                    //Company_Name =g.Company_Name,
                    //Company_Code =g.Company_Code

                    //}).ToList();

                    return compDet;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        public static List<Company_Detail> getcommisionlistbyuserid(long userid)
        {
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    db.Configuration.ProxyCreationEnabled = false;
                    List<commision_Detail> commlist = db.commision_Detail.Where(d => d.Fk_User_Id == userid).ToList();
                    List<Company_Detail> compDet = db.Company_Detail.Where(d => d.Is_deleted == false).ToList();
                    foreach (var item in compDet)
                    {
                        commision_Detail sst = commlist.SingleOrDefault(c => c.Operator_Code == item.Company_Code);
                        item.commper = sst == null ? 0 : Convert.ToDecimal(sst.Commision_Per);
                    }
                    return compDet;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        public static List<commision_Detail> List_CommisionByPackage(long ID)
        {
            List<commision_Detail> lstcomm = new List<commision_Detail>();
            try
            {


                using (var db = new PaymezonDBEntities1())
                {
                    //lstcomm = db.commision_Detail.GroupBy(d => new { d.Plan_Id, d.Plan_Name }).ToList();

                    //var planid = db.CommissionPlans.SingleOrDefault(c => c.Fk_User_Id == ID);
                    lstcomm = db.commision_Detail.Where(d => d.Fk_User_Id == ID).OrderBy(d=> d.Operator_Name).ToList();

                }
            }
            catch (Exception ex)
            {
                return lstcomm;
            }

            return lstcomm;

        }
        public static List<commision_Detail> updatecommbyid(long userid)
        {
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    var commisionid = db.CommissionPlans.SingleOrDefault(d => d.Fk_User_Id == userid).Fk_Commision_Id;
                    List<commision_Detail> listcomm = new List<commision_Detail>();

                    listcomm = db.commision_Detail.Where(d => d.Plan_Id == commisionid).ToList();

                    return listcomm;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static string Editcommision(commision_Detail commision)
        {
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    try
                    {
                        commision_Detail datamodify = db.commision_Detail.Where(d => d.Operator_Code == commision.Operator_Code && d.Plan_Id == commision.Plan_Id).FirstOrDefault();

                   
                   
                    if (datamodify != null)
                    {
                       

                        datamodify.Commision_Per = commision.Commision_Per;
                        datamodify.Modify_By = commision.Modify_By;
                        datamodify.Modify_Date = commision.Modify_Date;
                        datamodify.Operator_Code = commision.Operator_Code;

                        db.SaveChanges();
                       

                    }
                    else 
                    
                    {
                        commision_Detail Comm = new commision_Detail();
                        var newplan = db.commision_Detail.Where(d=> d.Plan_Id == commision.Plan_Id).FirstOrDefault();

                        Comm.Plan_Name = newplan.Plan_Name;
                        Comm.Operator_Name = datamodify.Operator_Name;
                        Comm.Operator_Code = commision.Operator_Code;
                        Comm.Commision_Per = commision.Commision_Per;
                        Comm.Plan_Id = newplan.Plan_Id;
                        Comm.Created_By = 0;
                        Comm.Created_Date = System.DateTime.Now;
                       
                        db.commision_Detail.Add(Comm);
                        db.SaveChanges();
                       
                    }
                    }
                    catch (Exception)
                    {

                        commision_Detail Comm = new commision_Detail();
                        var newplan = db.commision_Detail.Where(d => d.Plan_Id == commision.Plan_Id).FirstOrDefault();

                        Comm.Plan_Name = newplan.Plan_Name;
                        Comm.Operator_Name = newplan.Operator_Name;
                        Comm.Operator_Code = commision.Operator_Code;
                        Comm.Commision_Per = commision.Commision_Per;
                        Comm.Plan_Id = newplan.Plan_Id;
                        Comm.Created_By = 0;
                        Comm.Created_Date = System.DateTime.Now;

                        db.commision_Detail.Add(Comm);
                        db.SaveChanges();
                    }
                   
                }
                return "succ";
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
           
        }

        public static string editcommisionlist(List<commision_Detail> Commision)
        {
            try
            {
                using (var db = new PaymezonDBEntities1())
                {

                    foreach (var item in Commision)
                    {

                        try
                        {
                            commision_Detail datamodify = db.commision_Detail.SingleOrDefault(d => d.Operator_Code == item.Operator_Code && d.Fk_User_Id == item.Fk_User_Id);
                            if (datamodify != null)
                            {
                                datamodify.Plan_Name = "NA";
                                datamodify.Operator_Name = item.Operator_Name;
                                datamodify.Operator_Code = item.Operator_Code;
                                datamodify.Commision_Per = item.Commision_Per;
                                datamodify.Fk_User_Id = item.Fk_User_Id;
                                datamodify.Modify_By = 0;
                                datamodify.Modify_Date = System.DateTime.Now;
                                datamodify.Operator_Code = item.Operator_Code;                                
                                db.SaveChanges();
                            }
                            else
                            {
                                commision_Detail Comm = new commision_Detail();
                              //var newplan = db.commision_Detail.Where(d => d.Fk_User_Id == item.Fk_User_Id).FirstOrDefault();
                                Comm.Plan_Name = "NA";
                                Comm.Operator_Name = item.Operator_Name;
                                Comm.Operator_Code = item.Operator_Code;
                                Comm.Commision_Per = item.Commision_Per;
                                Comm.Fk_User_Id = item.Fk_User_Id;
                                Comm.Plan_Id = 0;
                                Comm.Created_By = 0;
                                Comm.Created_Date = System.DateTime.Now;
                                db.commision_Detail.Add(Comm);
                                db.SaveChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                   

                }
                return "succ";
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public static string addnewcommisionfornewuser(int userid)
        {
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    db.Configuration.ProxyCreationEnabled = false;
                    List<Company_Detail> compDet = db.Company_Detail.Where(d => d.Is_deleted == false).ToList();
                    commision_Detail Commision = new commision_Detail();   
                    foreach (var item in compDet)
                    {
                        Commision.Plan_Name = "NA";
                        Commision.Operator_Name = item.Company_Name;
                        Commision.Operator_Code = item.Company_Code;
                        Commision.Commision_Per = "0";
                        Commision.Plan_Id = null;
                        Commision.Fk_User_Id = userid;
                        Commision.Created_By = 0;
                        Commision.Created_Date = System.DateTime.Now;
                        db.commision_Detail.Add(Commision);
                        db.SaveChanges();
                    }
                    
                }
                return "Succ";
            }
            catch (Exception ex)
            {
                LogDetail log = new LogDetail();
                log.Create_Date = System.DateTime.Now;
                log.Log_Detail = ex.ToString();
                log.Page_Name = "addnewcommisionfornewuser";
                LogDetailDAL.AddLog(log);
                return "fail";
            }
        }
        public static int getplanidbyuserid(long userid) 
        {
             try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    long? id;

                 CommissionPlan cid = db.CommissionPlans.SingleOrDefault(d => d.Fk_User_Id == userid);

                 if ( cid == null)
                    {
                       
                        return 0;

                    }
                    else
                    {
                      
                        int planid = unchecked((int)cid.Fk_Commision_Id);

                        return planid;

                    }
                  
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
