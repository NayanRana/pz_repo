﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymezonDAL
{
 public class VendorDiversionDAL
    {
     public static List<Company_Detail> listvendorbyapiname(string apiname)
     {  
         try
         { 
             using (var db = new PaymezonDBEntities1())
             {
                  //List<VendorDiversion> listvendor = new List<VendorDiversion>();
                 //lstcomm = db.commision_Detail.GroupBy(d => new { d.Plan_Id, d.Plan_Name }).ToList();
                // listvendor = db.VendorDiversions.AsEnumerable().Where(d => d.ServiceProvider == apiname).ToList();

                 db.Configuration.ProxyCreationEnabled = false;
                 List<VendorDiversion> vendorlist = db.VendorDiversions.AsEnumerable().Where(d => d.ServiceProvider == apiname).ToList();
                 List<Company_Detail> compDet = db.Company_Detail.Where(d=>d.Is_deleted == false).ToList();

                 foreach (var item in compDet)
                 {
                     VendorDiversion sst = vendorlist.AsEnumerable().SingleOrDefault(c => c.Operator == item.Company_Code);
                     item.ServiceType = sst == null ? "" : sst.ServiceType;
                     item.OperatorCode = sst == null ? "" : sst.OperatorCode;
                     item.RechargeType = sst == null ? "" : sst.RechargeType;
                 }
                 return compDet;

                

            
             } 
         }
         catch (Exception ex)
         {
             return null;
         }
         
        
     }


     public static string  addoreditvendorlist(List<VendorDiversion> listdata)
     {
         try
         {
             using (var db = new PaymezonDBEntities1())
             {

                 foreach (var item in listdata)
                 {

                     try
                     {
                         VendorDiversion datamodify = new VendorDiversion();
                         datamodify = db.VendorDiversions.SingleOrDefault(d => d.ServiceProvider == item.ServiceProvider && d.Operator == item.Operator);
                         if (datamodify != null)
                         {
                             datamodify.ServiceType = item.ServiceType;
                             datamodify.RechargeType = item.RechargeType;
                             datamodify.OperatorCode = item.OperatorCode;
                             db.SaveChanges();
                         }
                         else
                         {
                             VendorDiversion vendordiversion = new VendorDiversion();
                             vendordiversion.Operator = item.Operator;
                             vendordiversion.ServiceProvider = item.ServiceProvider;
                             vendordiversion.ServiceType = item.ServiceType;
                             vendordiversion.AvailabilityIn = "E";
                             vendordiversion.Status = false;
                             vendordiversion.Remark = item.ServiceType;
                             vendordiversion.ServiceProviderSms = item.ServiceProvider;
                             vendordiversion.OperatorCode = item.OperatorCode;
                             vendordiversion.RechargeType = item.RechargeType;
                             vendordiversion.ServiceCode = null;
                             db.VendorDiversions.Add(vendordiversion);
                             db.SaveChanges();
                         }
                     }
                     catch (Exception ex)
                     {
                         throw ex;
                     }
                 }


             }
             return "succ";

         }
         catch (Exception ex)

         {
             
             throw;
         }
     }
    }
}
