﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PaymezonDAL;
using PaymezonBAL;
using PaymezonApp;
using PaymezonApp.Models;
using System.Data.Entity;
using System.Web.Routing;

namespace PaymezonApp.Controllers
{
    public class CommisionController : Controller
    {
        
        public ActionResult Index()
        {

            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            CommisionViewModels cmmsinlist = new CommisionViewModels();


            cmmsinlist.List_Commision = CommisionBAL.List_Commision();
            cmmsinlist.List_Company = CompanyBAL.CompanyList();
            cmmsinlist.lstComm = CommisionBAL.List_CommisionByPlanname();

            return View(cmmsinlist);
        }
        public ActionResult CommisionReport()
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            CommisionViewModels cmmsinlist = new CommisionViewModels();
            cmmsinlist.List_Commision = CommisionBAL.List_Commision();
            return View(cmmsinlist);
        }
        public ActionResult editcommofuser(long userid)
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            CommisionViewModels Comm = new CommisionViewModels();
          //  Comm.List_Commision = CommisionBAL.updatecommbyid(userid);
            Comm.Plan_Id = CommisionBAL.getplanidbyuserid(userid);
            int? planid = Comm.Plan_Id;
            return Redirect("../Commision/Index?userid=" + userid);
        }

          [HttpPost]
        public ActionResult EditCommision(List<commision_Detail> lstData)
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            commision_Detail Commision = new commision_Detail();
            CommisionBAL.editcommisionlist(lstData);
            return RedirectToAction("Adminuser","Admin","");
        }
        [HttpPost]
        public ActionResult AddCommision(List<commision_Detail> lstData)
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            commision_Detail Commision = new commision_Detail();
            var userplanId = db.commision_Detail.Max(o => o.Plan_Id) + 1;

            if (userplanId == null)
            {
                userplanId = 1;
            }
            else
            {
                userplanId = db.commision_Detail.Max(o => o.Plan_Id) + 1;
                string userid = lstData.Select(d => d.Plan_Name).FirstOrDefault();

                CommissionPlan cmplan = new CommissionPlan();
                cmplan.Fk_Commision_Id = userplanId;
                cmplan.Fk_User_Id = Convert.ToInt64(userid);
                db.CommissionPlans.Add(cmplan);
                db.SaveChanges();

            }
            foreach (var item in lstData)
            {
                Commision.Plan_Name = item.Plan_Name;
                Commision.Operator_Name = item.Operator_Name;
                Commision.Operator_Code = item.Operator_Code;
                Commision.Commision_Per = item.Commision_Per;
                Commision.Plan_Id = userplanId;
                Commision.Created_By = 1;
                Commision.Created_Date = System.DateTime.Now;
                db.commision_Detail.Add(Commision);
                db.SaveChanges();
            }
            return RedirectToAction("../Commision/Index");
        }

        public ActionResult CommisionPackage()
        {
            long ID = Convert.ToInt64(Session["U_ID"]);
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            CommisionViewModels cmmsinlist = new CommisionViewModels();
            cmmsinlist.List_Commision = CommisionBAL.List_CommisionByPackage(ID);
            return View(cmmsinlist);

        }
        [HttpGet]
        public object GetcommisionList(int? start, int length, long userid)
        {

            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            CommisionViewModels cmmsinlist = new CommisionViewModels();
            //this code change for first time table load by plan id but now it's load by userid flow should be changed (By Dhananjay)
            var obj = CommisionBAL.getcommisionlistbyuserid(userid);
            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = obj;
            d.iTotalDisplayRecords = obj.Count();
            d.iTotalRecords = obj.Count();
            d.sEcho = "";
            return Json(d, JsonRequestBehavior.AllowGet);

            //if (planid == 0)
            //{
            //    var obj = CompanyBAL.CompanyList();
            //    DatatablePaggeing d = new DatatablePaggeing();
            //    d.aaData = CompanyBAL.CompanyList();
            //    d.iTotalDisplayRecords = obj.Count();
            //    d.iTotalRecords = obj.Count();
            //    d.sEcho = "";
            //    return Json(d, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    var obj = CommisionBAL.getcommisionlistbyplanid(planid);
            //    DatatablePaggeing d = new DatatablePaggeing();
            //    d.aaData = obj;
            //    d.iTotalDisplayRecords = obj.Count();
            //    d.iTotalRecords = obj.Count();
            //    d.sEcho = "";
            //    return Json(d, JsonRequestBehavior.AllowGet);
            //}
        }
    }
}