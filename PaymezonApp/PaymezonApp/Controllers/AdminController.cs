﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PaymezonBAL;
using PaymezonApp.Models;
using PaymezonDAL;
using System.Globalization;
using System.Net;
using System.IO;


namespace PaymezonApp.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }

     
        public ActionResult Dashboard()
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            RechargeViewModels obj = new RechargeViewModels();
            obj.transaction = Utility.CountTransactiondetail();
            obj.list_transaction = Utility.liststatusbyapiname();
            obj.RCG_List = Utility.counttransactionbyserviceproviderforadmin();
            obj.countrech_List = Utility.counttransactionbyapiforadmin();
            return View(obj);
        }

        public ActionResult AccountStatement()
        {
            try
            {
                if (Session["U_ID"] != null)
                {
                    long ID = Convert.ToInt64(Session["U_ID"]);
                    return View(ALLReportBAL.AccountStatement_ById(ID));
                }
                else
                {
                    return View(ALLReportBAL.AccountStatement());
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }
         [HttpGet]
        public object GetdailyreportList(int? start, int length, long User_ID, string date)
        {

            TransactionStatus objstts = new TransactionStatus();
            DatatablePaggeing d = new DatatablePaggeing();
            d.aaData = Utility.CountTransactionList(User_ID, date);
            d.iTotalDisplayRecords = 1;
            d.iTotalRecords = 1;
            d.sEcho = "";
            return Json(d, JsonRequestBehavior.AllowGet);
        }

        public ActionResult dailyureportforadmin()
        {
            try
            {
                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                TransactionModel obj = new TransactionModel();
                //obj.transanctionlist = ALLReportBAL.listtransaction();
                TransactionStatus obj1 = new TransactionStatus();
               
                obj1.userregistrationlist = db.UserRegistrations.Where(c => c.Is_Deleted == false).ToList();
                return View(obj1);
                
            }
            catch (Exception  ex)
            {
                 return View();
            }
        }
        public ActionResult FundRequestService()
        {

            string fromdate = System.DateTime.Today.Date.ToString();
            string todate = "";
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            AdminBAL FndRqList = new AdminBAL();
            Models.FundRequestViewModels md = new FundRequestViewModels();

            md.LstFund_Req = FndRqList.FundRequestlist(fromdate,todate);
            return View(md);
        }

        [HttpPost]
        public ActionResult FundRequestService(FundRequestViewModels reg)
        {


            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            AdminBAL FndRqList = new AdminBAL();
            Models.FundRequestViewModels md = new FundRequestViewModels();
            if (ModelState.IsValid)
            {
             
                md.LstFund_Req = FndRqList.FundRequestlist(reg.fromdate,reg.todate);
                return View(md);
            }
            else
            {
                reg.fromdate = System.DateTime.Today.Date.ToString();
                reg.todate = "";
                md.LstFund_Req = FndRqList.FundRequestlist(reg.fromdate,reg.todate);
                md.message = "Please Select Valid Date..!";
                return View(md);
            }
           

        }


        public PartialViewResult DeclineReasonOf_Fund(int FndId)
        {

            DeclineResonOfFundViewModels obj = new DeclineResonOfFundViewModels();
            Fund_Request fr = AdminBAL.GetfndByid(FndId);
            obj.Decline_Reason = fr.Decline_Reason;
            obj.Pk_Fund_Request_Id = fr.Pk_Fund_Request_Id;
            return PartialView(obj);
        }
        public PartialViewResult deletedata(long id, string pagename)
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            Models.RegisterViewModels md = new RegisterViewModels();
            return PartialView(md); 
        }
        public PartialViewResult changestatus(string txnid, string pagename, string status)
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            Models.RegisterViewModels md = new RegisterViewModels();
            return PartialView(md);
        }
        [HttpPost]
        public ActionResult changestatus(Models.RegisterViewModels reg)
        {

            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            UserRegistration checkpass = new UserRegistration();
            checkpass = db.UserRegistrations.Where(d => d.Password == reg.Password && d.Label_Id == "ADM0001").FirstOrDefault();
            if (checkpass != null)
            {
                if (reg.Logo == "transactionreport")
                {
                    string str = TransactionBAL.UpdateStatus(reg.Domain_Name,reg.Company_Name, "2");
                    return Redirect("../Admin/TransactionReport");
                }
            }
            else
            {
                return Redirect("../Admin/Index");
            }
            return Redirect("../Admin/Index");
        }

        public ActionResult UpdateStatusbyadmin(string txnid, string status)
        {
            try
            {
                 string str = TransactionBAL.UpdateStatus(status,txnid, "2");
                 if (str != null)
                 {
                     string requsturl = TransactionBAL.Getrequrl(txnid);

                     string clienttxid = TransactionBAL.Getctxid(txnid);

                     string retUrl = requsturl + "?ctxnid=" + clienttxid + "&status=" + status + "&apitransactionID=Manual";
                     //string str = TransactionBAL.UpdateStatus(Transtype, txnid, opr_id);
                     HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
                     string dataString = "";
                     string respons;
                     request.Method = "GET";
                     request.Timeout = 180000;
                     request.ReadWriteTimeout = 180000;
                     request.KeepAlive = false;
                     request.Proxy = null;
                     request.ServicePoint.ConnectionLeaseTimeout = 60000;
                     request.ServicePoint.MaxIdleTime = 60000;
                     HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
                     Stream s = (Stream)responsee.GetResponseStream();
                     StreamReader readStream = new StreamReader(s);
                     dataString = readStream.ReadToEnd();
                     respons = dataString;
                     responsee.Close();
                     s.Close();
                     readStream.Close();
                 }
                 
                 return Redirect("../Admin/TransactionReport");
            }
            catch (Exception)
            {
                
                throw;
            }
        }
       [HttpPost]
        public ActionResult deletedata(Models.RegisterViewModels reg)
       {
        
        //reg.Pk_Register_ID = deleteid;
        //reg.Logo = pagename;

        PaymezonDBEntities1 db = new PaymezonDBEntities1();
        UserRegistration checkpass = new UserRegistration();
        checkpass = db.UserRegistrations.Where(d => d.Password == reg.Password && d.Label_Id == "ADM0001").FirstOrDefault();
        if (checkpass != null)
        {
            if (reg.Logo == "Adminuser")
            {
                AdminBAL.Deleteuser(reg.Pk_Register_ID);
                return Redirect("../Admin/Adminuser");
            }
            else if (reg.Logo == "API")
            {
                AdminBAL.DeleteAPI(reg.Pk_Register_ID);
                return RedirectToAction("../Admin/API");
            }
            else if (reg.Logo == "DeleteCompany")
            {
                CompanyBAL.DeleteCompany(reg.Pk_Register_ID);
                return RedirectToAction("../Company/index");
            }
            else if (reg.Logo == "DeleteSupport")
            {
                AdminBAL.DeleteSupport(reg.Pk_Register_ID);
                return RedirectToAction("../Admin/SupportSystem");
 
            }
            else if (reg.Logo == "AccpetFund")
            {
                Fund_Request fr = AdminBAL.GetfndByid(reg.Pk_Register_ID);
               // PaymezonDBEntities1 db = new PaymezonDBEntities1();
                User_Wallet usr = new User_Wallet();
                usr.Fk_User_Id = fr.Fk_User_Id;
                usr.User_Name = fr.Name;
                usr.Label_Id = fr.User_Id;
                usr.Fund_Amount = fr.Amount;
                usr.Credit_Amount = fr.Amount;
                usr.Debit_Amount = 0;
                usr.Description = "Fund Requested On date" + fr.Request_date_time + "of " + fr.Amount + " is Approved And Will be tarnsffered to your A/C.";
                usr.Created_Date = System.DateTime.Now;
                usr.Created_By = '1';

                User_Amount amnt = new User_Amount();
                amnt.Fk_User_ID = fr.Fk_User_Id;
                amnt.Total_Credit = fr.Amount;
                amnt.Updated_Date = System.DateTime.Now;
                string amt = AdminBAL.UpdateAmount(amnt);
                Session["Amt"] = amt;

                Fund_Request fndsuccess = new Fund_Request();
                fndsuccess.Pk_Fund_Request_Id = reg.Pk_Register_ID;
                AdminBAL.UpdateFund(fndsuccess);


                db.User_Wallet.Add(usr);
                db.SaveChanges();
                return RedirectToAction("../Admin/FundRequestService");

            }
            else if (reg.Logo == "DeleteAssignApi")
            {
                AssignApiBAL.DeleteAssignApi(reg.Pk_Register_ID);
                return RedirectToAction("../AssignApi/Index");
            }
            else if (reg.Logo == "ActiveApI")
            {
                AssignApiBAL.DeleteAssignApi(reg.Pk_Register_ID);
                return RedirectToAction("../AssignApi/Index");
            }
          
        }
        else 
        {
            return Redirect("../Admin/Index");
        }
        return Redirect("../Admin/Index");
       }

        [HttpPost]
        public ActionResult AccpetFund(Models.FundRequestViewModels reg, int FndId)
        {
            try
            {
                Fund_Request fr = AdminBAL.GetfndByid(FndId);

                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                User_Wallet usr = new User_Wallet();
                usr.Fk_User_Id = fr.Fk_User_Id;
                usr.User_Name = fr.Name;
                usr.Label_Id = fr.User_Id;
                usr.Fund_Amount = fr.Amount;
                usr.Credit_Amount = fr.Amount;
                usr.Debit_Amount = 0;
                usr.Description = "Fund Requested On date" + fr.Request_date_time + "of " + fr.Amount + " is Approved And Will be tarnsffered to your A/C.";
                usr.Created_Date = System.DateTime.Now;
                usr.Created_By = '1';

                User_Amount amnt = new User_Amount();
                amnt.Fk_User_ID = fr.Fk_User_Id;
                amnt.Total_Credit = fr.Amount;
                amnt.Updated_Date = System.DateTime.Now;
                string amt= AdminBAL.UpdateAmount(amnt);
                Session["Amt"] = amt;

                Fund_Request fndsuccess = new Fund_Request();
                fndsuccess.Pk_Fund_Request_Id = FndId;
                AdminBAL.UpdateFund(fndsuccess);


                db.User_Wallet.Add(usr);
                db.SaveChanges();
                return RedirectToAction("../Admin/FundRequestService");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ActionResult PushTransferService()
        {
            RegistrationBAL regBal = new RegistrationBAL();
            PushTransferViewModels modl = new PushTransferViewModels();
            modl.lstAgnt = regBal.DDLUser("4").ToList();
            modl.lstDis = regBal.DDLUser("3").ToList();
            modl.lstWL = regBal.DDLUser("1").ToList();
            return View(modl);
        }
        public PartialViewResult UserRegistration()
        {

            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            RegistrationBAL regBal = new RegistrationBAL();
            Models.RegisterViewModels md = new RegisterViewModels();
          //  List<commision_Detail> lst = CommisionBAL.List_Commision();
            md.lstComm = CommisionBAL.List_CommisionByPlanname();
          //  md.lstComm = lst;
            md.lstAgnt = regBal.DDLUser("4").ToList();
            md.lstDis = regBal.DDLUser("3").ToList();
            md.lstMDis = regBal.DDLUser("2").ToList();
            md.lstWL = regBal.DDLUser("1").ToList();
            return PartialView(md);
        }

        public ActionResult Adminuser()
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            RegistrationBAL regBal = new RegistrationBAL();
            Models.RegisterViewModel md = new RegisterViewModel();
            md.lstUsers = db.UserRegistrations.Where(c => c.Is_Deleted == false).ToList();
            md.lstUser = db.UserRegistrations.AsEnumerable().Where(c => c.Is_Deleted == false).Select(e=> new UserRegistration() 
            {
                Pk_Register_ID = e.Pk_Register_ID,
                Author_Name = e.Author_Name,
                Author_Email = e.Author_Email,
                Password = e.Password,
                Label_Id =e.Label_Id,
                Panel_Title = db.User_Amount.FirstOrDefault(d => d.Fk_User_ID == e.Pk_Register_ID).Actual_Amount.ToString() 

            }).ToList();
            md.lstAgnt = regBal.DDLUser("4").ToList();
            md.lstDis = regBal.DDLUser("3").ToList();
            md.lstMDis = regBal.DDLUser("2").ToList();
            md.lstWL = regBal.DDLUser("1").ToList();
            return View(md);
        }
        [HttpPost]
        public ActionResult Adminuser(RegisterViewModel reg)
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            RegistrationBAL regBal = new RegistrationBAL();

            Models.RegisterViewModel md = new RegisterViewModel();

            md.lstUsers = db.UserRegistrations.Where(c => c.Is_Deleted == false).ToList();

            if (reg.userid == null || reg.userid == 0)
            {
                md.lstUser = db.UserRegistrations.Where(c => c.Is_Deleted == false).Select(e => new UserRegistration()
                {
                    Pk_Register_ID = e.Pk_Register_ID,
                    Author_Name = e.Author_Name,
                    Author_Email = e.Author_Email,
                    Password = e.Password,
                    Label_Id = e.Label_Id,
                    Panel_Title = db.User_Amount.FirstOrDefault(d => d.Fk_User_ID == e.Pk_Register_ID).Actual_Amount.ToString()
                }).ToList();
            }
            else
            {
                md.lstUser = db.UserRegistrations.AsEnumerable().Where(c => c.Is_Deleted == false && c.Pk_Register_ID == reg.userid).Select(e => new UserRegistration()
                {
                    Pk_Register_ID = e.Pk_Register_ID,
                    Author_Name = e.Author_Name,
                    Author_Email = e.Author_Email,
                    Password = e.Password,
                    Label_Id = e.Label_Id,
                    Panel_Title = db.User_Amount.FirstOrDefault(d => d.Fk_User_ID == e.Pk_Register_ID).Actual_Amount.ToString()
                }).ToList();
            }
           
            md.lstAgnt = regBal.DDLUser("4").ToList();
            md.lstDis = regBal.DDLUser("3").ToList();
            md.lstMDis = regBal.DDLUser("2").ToList();
            md.lstWL = regBal.DDLUser("1").ToList();
            return View(md);
        }
        [HttpPost]
        public ActionResult AdminLogin(Models.AdminViewModels log)
        {
            if (log.UserName == "admin" && log.Password == "admin")
            {
                return RedirectToAction("/Dashboard");

            }

            //if (log.UserName != "" && log.Password != "")
            //{
            //    AdminBAL balReg = new AdminBAL();

            //    bool exist = balReg.CheckUser(log.UserName, log.Password);  //db.UserRegistrations.Any(c => c.Author_Email == log.UserName && c.Password == log.Password);
            //    if (exist == true)
            //    {
            //        return RedirectToAction("/Index");
            //    }
            //    else
            //    {
            //        return RedirectToAction("/Login");
            //    }
            //}
            else
            {
                return RedirectToAction("/Index");
            }

        }

        public ActionResult Edituserforadmin(long userid)
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            RegistrationBAL regBal = new RegistrationBAL();
            Models.RegisterViewModels md = new RegisterViewModels();
            md.lstComm = CommisionBAL.List_CommisionByPlanname();          
           
            md.dalusrreg = ProfileBAL.Editprofile(userid);

            return PartialView(md);
        }
        public ActionResult Edituserbyadmin(Models.RegisterViewModels reg)
        {
            try
            {
                long id = Convert.ToInt64(Session["U_ID"]);
                if (id == null || id == 0)
                {
                    id = 0;
                }
                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                UserRegistration usr = new UserRegistration();
                usr.Pk_Register_ID = reg.dalusrreg.Pk_Register_ID;
                usr.Label_Id = reg.dalusrreg.Label_Id;
                usr.Password = reg.dalusrreg.Password;
                usr.Author_Email = reg.dalusrreg.Author_Email;
                usr.Author_MobNo = reg.dalusrreg.Author_MobNo;
                usr.Author_Name = reg.dalusrreg.Author_Name;
                usr.City = reg.dalusrreg.City;
                usr.Company_Name = reg.dalusrreg.Company_Name;
                usr.Copyright = reg.dalusrreg.Copyright;
                usr.Domain_Name = reg.dalusrreg.Domain_Name;
                //  usr.Fk_Country_Id = reg.dalusrreg.Fk_Country_Id;
                // usr.Fk_State_Id = reg.dalusrreg.Fk_State_Id;
                usr.Helpdesk_Email = reg.dalusrreg.Helpdesk_Email;
                //  usr.Logo = reg.dalusrreg.Logo;
                usr.Mobile_Number = reg.dalusrreg.Mobile_Number;
                usr.Office_Address = reg.dalusrreg.Office_Address;
                usr.PAN_Num = reg.dalusrreg.PAN_Num;
                usr.Panel_Title = reg.dalusrreg.Panel_Title;
                usr.Pincode = reg.dalusrreg.Pincode;
                usr.Portal_FName = reg.dalusrreg.Portal_FName;
                //  usr.Portal_LName = reg.dalusrreg.Portal_LName;
                usr.Portal_MName = reg.dalusrreg.Portal_MName;
                usr.Modified_Date = System.DateTime.Now;
                usr.Modified_By = id;
                usr.Return_URL = reg.dalusrreg.Return_URL;
                ProfileBAL.Updateprofile(usr);
               
                return RedirectToAction("../Admin/Adminuser");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Deleteuser(long userid)
        {
            
            AdminBAL.Deleteuser(userid);
            return RedirectToAction("../Admin/Adminuser");

        }
        public ActionResult PendingTransactions()
        {
            return View();
        }

        public ActionResult RefundTransaction()
        {
            return View();
        }

        public ActionResult RecoReport()
        {
            return View();
        }

        public ActionResult SuccFailurReport()
        {
            return View();
        }

        public ActionResult Reports()
        {
            return View();
        }
        [HttpPost]
        public JsonResult APILIST()
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            AdminBAL Api_Detail_List = new AdminBAL();
            Models.ApiViewModels md = new ApiViewModels();
            md.Api_List_Detail = Api_Detail_List.Api_List();
            return Json(md);
        }

        public ActionResult API()
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            AdminBAL Api_Detail_List = new AdminBAL();
            Models.ApiViewModels md = new ApiViewModels();

            md.Api_List_Detail = Api_Detail_List.Api_List();
            return View(md);
        }
        public PartialViewResult CreateApi()
        {
            return PartialView();
        }
        public PartialViewResult EditAPI(Int32 Apiid)
        {

            ApiViewModels obj = new ApiViewModels();
            obj.dalApi = AdminBAL.GetApiByid(Apiid);
            return PartialView(obj);
        }
        [HttpPost]
        public ActionResult AddAPI(Models.ApiViewModels reg)
        {
            try
            {

                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                Api_Detail usr = new Api_Detail();
                usr.Api_Name = reg.Api_Name;
                usr.Login_Id = reg.Login_Id;
                usr.User_Id = reg.User_Id;
                usr.User_Name = reg.User_Name;
                usr.Password = reg.Password;
                usr.Response_Url = reg.Response_Url;
                usr.Request_Url = reg.Request_Url;
                usr.Response_Type = reg.Response_Type;
                usr.StatusCode = reg.StatusCode;
                usr.SuccessCode = reg.SuccessCode;
                usr.FailuerCode = reg.FailuerCode;
                usr.PendingCode = reg.PendingCode;
                usr.UniqueTransectionid = reg.UniqueTransectionid;
                usr.Transectionnumber = reg.Transectionnumber;
                usr.Message = reg.Message;
                usr.Type = reg.Type;
                usr.Amount = reg.Amount;
                usr.Operator_code = reg.Operator_code;
                usr.Number = reg.Number;
                usr.Client_Id = reg.Client_Id;
                usr.Circle_Code = reg.Circle_Code;
                usr.Pin = reg.Pin;
                usr.Account = reg.Account;
                usr.User_Transaction = reg.User_Transaction;
                usr.Transaction_Id = reg.Transaction_Id;
                usr.Format = reg.Format;
                usr.Version = reg.Version;
                usr.Token = reg.Token;
                usr.Provider = reg.Provider;
                usr.Agent_Id = reg.Agent_Id;
                usr.Company = reg.Company;
                usr.Order_Id = reg.Order_Id;
                usr.Std_Code = reg.Std_Code;
                usr.Optional_1 = reg.Optional_1;
                usr.Optional_2 = reg.Optional_2;
                usr.Other_Value = reg.Other_Value;
                usr.Is_Deleted = false;
                db.Api_Detail.Add(usr);
                db.SaveChanges();
                return RedirectToAction("../Admin/API");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public ActionResult DeleteAPI(long Apiid)
        {

            AdminBAL.DeleteAPI(Apiid);
            return RedirectToAction("../Admin/API");

        }

        [HttpPost]

        public ActionResult AddDeclineReason(Models.DeclineResonOfFundViewModels reg)
        {
            try
            {

                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                Fund_Request usr = new Fund_Request();
                usr.Pk_Fund_Request_Id = reg.Pk_Fund_Request_Id;
                usr.Decline_Reason = reg.Decline_Reason;

                AdminBAL.UpdateReason(usr);

                return RedirectToAction("../Admin/FundRequestService");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public ActionResult EditAPI(Models.ApiViewModels reg)
        {
            try
            {

                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                Api_Detail usr = new Api_Detail();
                usr.Api_Id = reg.dalApi.Api_Id;
                usr.Api_Name = reg.dalApi.Api_Name;
                usr.Login_Id = reg.dalApi.Login_Id;
                usr.Password = reg.dalApi.Password;
                usr.Response_Url = reg.dalApi.Response_Url;
                usr.Request_Url = reg.dalApi.Request_Url;
                usr.Response_Type = reg.dalApi.Response_Type;
                usr.StatusCode = reg.dalApi.StatusCode;
                usr.SuccessCode = reg.dalApi.SuccessCode;
                usr.FailuerCode = reg.dalApi.FailuerCode;
                usr.PendingCode = reg.dalApi.PendingCode;
                usr.UniqueTransectionid = reg.dalApi.UniqueTransectionid;
                usr.Transectionnumber = reg.dalApi.Transectionnumber;
                usr.Message = reg.dalApi.Message;
                usr.Type = reg.dalApi.Type;
                usr.Number = reg.dalApi.Number;
                usr.Circle_Code = reg.dalApi.Circle_Code;
                usr.Pin = reg.dalApi.Pin;
                usr.User_Transaction = reg.dalApi.User_Transaction;
                usr.Format = reg.dalApi.Format;
                usr.Token = reg.dalApi.Token;
                usr.Provider = reg.dalApi.Provider;
                usr.Agent_Id = reg.dalApi.Agent_Id;
                AdminBAL.UpdateAPI(usr);
                //return SuccessResponse("Event Updated Successfully");
                //db.Api_Detail.Add(usr);
                //db.SaveChanges();
                return RedirectToAction("../Admin/API");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public ActionResult SupportSystem()
        {
           // long ID = Convert.ToInt64(Session["U_ID"]);
            string fromdate = "";
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            SupportViewModels msglist = new SupportViewModels();
            msglist.List_SupportDetail = SupportBAL.MSG_LIST(fromdate);
            return View(msglist);

        }
        [HttpPost]
        public ActionResult SupportSystem(SupportViewModels reg)
        {
            // long ID = Convert.ToInt64(Session["U_ID"]);
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            SupportViewModels msglist = new SupportViewModels();
            if (ModelState.IsValid)
            {
                msglist.List_SupportDetail = SupportBAL.MSG_LIST(reg.fromdate);
                return View(msglist);
            }
            else
            {
                reg.fromdate = "";
                msglist.List_SupportDetail = SupportBAL.MSG_LIST(reg.fromdate);
                msglist.message = "Please Select or Enter Valid From Date..!";
                return View(msglist);
            }

        }
        public PartialViewResult ReplySupport(string TokenId)
        {

            SupportViewModels obj = new SupportViewModels();
            obj.Token_no = TokenId;
            obj.SupportDetail_byTokenID = SupportBAL.GetsinglesupportByid(TokenId);
            return PartialView(obj);
        }


        [HttpPost]

        public ActionResult ReplySupport(Models.SupportViewModels reg)
        {
            try
            {

                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                Support_Detail usr = new Support_Detail();

                usr.Token_no = reg.Token_no;

                usr.Content_Detail = reg.Content_Detail;
                usr.Answer_By = 0;
                usr.Answer_Date = System.DateTime.Now;
                usr.Is_Deleted = false;
                db.Support_Detail.Add(usr);
                db.SaveChanges();

                return RedirectToAction("../Admin/SupportSystem");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult DeleteSupport(long id)
        {

            AdminBAL.DeleteSupport(id);
            return RedirectToAction("../Admin/SupportSystem");

        }

        public ActionResult TransactionReport()
        {
          
            try
            {

                string fdate = System.DateTime.Today.Date.ToString();
                string tdate = "";

                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                TransactionModel obj = new TransactionModel();
                obj.transanctionlist = TransactionBAL.Transaction_Listdata(fdate, tdate);
                //fndlist.fromdate = System.DateTime.Now;
                return View(obj);
              
            }
            catch (Exception ex)
            {
                return RedirectToAction("../Admin/index");
               
            }
            
        }
        [HttpPost]
        public ActionResult TransactionReport(TransactionModel reg)
        {

            try
            {

                //var fdt = string.Format("{0:MM/dd/yyyy}", reg.fromdate);
                //var tdt = string.Format("{0:MM/dd/yyyy}", reg.todate);
                 PaymezonDBEntities1 db = new PaymezonDBEntities1();
                TransactionModel obj = new TransactionModel();
                if (ModelState.IsValid)
                {
                   

                    obj.transanctionlist = TransactionBAL.Transaction_Listdata(reg.fromdate, reg.todate);
                    // fndlist.fromdate = System.DateTime.Now;
                    return View(obj);
                }
                else
                {
                    reg.fromdate = System.DateTime.Now.Date.ToString();
                    reg.todate = "";
                    obj.transanctionlist = TransactionBAL.Transaction_Listdata(reg.fromdate, reg.todate);
                    obj.message = "Please Select Fromdate or Todate";
                    return View(obj);
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("../Admin/index");
            }
        }
        public ActionResult WalletHistory()
        {

            try
            {
                string fdate = System.DateTime.Now.Date.ToString();
                string tdate = "";
                UserWalletViewModel WalletLIST = new UserWalletViewModel();
                WalletLIST.List_UserWallet = UserWalletBAL.ListWallet(fdate, tdate);
                return View(WalletLIST);
            }
            catch (Exception ex)
            {
                return View();

            }

        }

        [HttpPost]
        public ActionResult WalletHistory(UserWalletViewModel reg)
        {

            try
            {
                UserWalletViewModel WalletLIST = new UserWalletViewModel();

                if (ModelState.IsValid)
                {
                    WalletLIST.List_UserWallet = UserWalletBAL.ListWallet(reg.fromdate, reg.todate);
                    return View(WalletLIST);
                }
                else
                {
                    reg.fromdate = System.DateTime.Now.ToString();
                    reg.todate = "";
                    WalletLIST.List_UserWallet = UserWalletBAL.ListWallet(reg.fromdate, reg.todate);
                    reg.message = "Please Select From or To Date..!";
                    return View(WalletLIST);
                }

                  
                
            }
            catch (Exception ex)
            {
                return View();

            }

        }


    }
}