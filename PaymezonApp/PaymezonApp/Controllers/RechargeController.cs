﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PaymezonBAL;
using PaymezonApp.Models;
using PaymezonDAL;

namespace PaymezonApp.Controllers
{
    public class RechargeController : Controller
    {
        //
        // GET: /Recharge/
        public ActionResult Index()
        {
            if (Session["U_ID"] != null)
            {
                string fromdate = System.DateTime.Now.ToString();
                string todate = "";
                long ID = Convert.ToInt64(Session["U_ID"]);
                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                RechargeViewModels Rcglist = new RechargeViewModels();
                Rcglist.RCG_List = RechargeBAL.RCG_List_byId(ID,fromdate,todate);
                return View(Rcglist);
            }
            else
            {
                return RedirectToAction("../Register/Login");
            }
        }
        [HttpPost]
        public ActionResult Index(RechargeViewModels  reg)
        {
            if (Session["U_ID"] != null)
            {
                long ID = Convert.ToInt64(Session["U_ID"]);
                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                RechargeViewModels Rcglist = new RechargeViewModels();
                if (ModelState.IsValid)
                {
                   
                    Rcglist.RCG_List = RechargeBAL.RCG_List_byId(ID, reg.fromdate, reg.todate);
                    return View(Rcglist);
                }
                else
                {
                    reg.fromdate = System.DateTime.Now.ToString();
                    reg.todate = "";
                    Rcglist.RCG_List = RechargeBAL.RCG_List_byId(ID, reg.fromdate, reg.todate);
                    Rcglist.message = "Please Select Valid From Date And From Date";
                    return View(Rcglist);
                }
            }
            else
            {
                return RedirectToAction("../Register/Login");
            }
        }

   

        public PartialViewResult AddNewRecharge()
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            RechargeViewModels obj = new RechargeViewModels();
            obj.List_Company = CompanyBAL.CompanyList();
            return PartialView(obj);
        }
        public ActionResult AddRecharge(Models.RechargeViewModels reg)
        {
            try
            {
                CommonApi api = new CommonApi();
                string txnId = "TN" + DateTime.Now.Year + DateTime.Now.Ticks.ToString("D" + 3);
                string refNo = "RF" + DateTime.Now.Ticks.ToString("D" + 5);
                string apiName = "";
                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                Recharge_Detail usr = new Recharge_Detail();

                long ID = Convert.ToInt64(Session["U_ID"]);
                string labelID = Convert.ToString(this.Session["Label_ID"]);
                string usertype = Convert.ToString(this.Session["User_Type"]);
                UserRegistration usrs = RegistrationDAL.CheckUserbyId(ID);
                string strmsg = Utility.CheckRechargeDetail(reg.Amount, usrs, Convert.ToInt64(reg.Mobile_no) );
                if (strmsg == "Succ")
                {

                    usr.Action_On_Amount = "Dr";
                    usr.Circle_Code = "8";
                    usr.Operator_Code = reg.Operator_Code;
                    usr.Mobile_no = reg.Mobile_no;
                    usr.Amount = reg.Amount;
                    usr.Transaction_Id = "TN" + DateTime.Now.Year + DateTime.Now.Ticks.ToString("D" + 3);  //new Random().Next();
                    usr.Ref_number = "RF" + DateTime.Now.Ticks.ToString("D" + 5);  // new Random().Next();
                    usr.Agent_Id = labelID;
                    usr.User_Type = usertype;
                    usr.Recharge_Date = System.DateTime.Now;
                    usr.Recharge_By = ID;
                    usr.Fk_RegisterId = ID;
                    usr.Status = "Pending";
                    usr.ipAdd = Utility.GetMacAddress();
                    //db.Recharge_Detail.Add(usr);

                    //db.SaveChanges();
                    string strMsgAmt = PaymezonBAL.RechargeBAL.InsertRegBAL(usr);
                    Session["Amt"] = strMsgAmt;
                    Utility.balance = Convert.ToDecimal(strMsgAmt);
                   
                }
                return RedirectToAction("../Recharge/Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}