﻿using Newtonsoft.Json;
using PaymezonApp.Models;
using PaymezonBAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace PaymezonApp.Controllers
{
    public class ALLReportController : Controller
    {
        public ActionResult FundReport()
        {
            try
            {
                if (Session["U_ID"] != null)
                {
                    string  fromdate = System.DateTime.Today.ToString();
                    string todate = "";
                    long ID = Convert.ToInt64(Session["U_ID"]);
                    UserWalletViewModel WalletLIST = new UserWalletViewModel();
                    WalletLIST.List_UserWallet_BYId = ALLReportBAL.ListWallet_By_Id(ID,fromdate,todate);
                    return View(WalletLIST);
                }
                else
                {
                    return RedirectToAction("../Register/Login");
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult FundReport(UserWalletViewModel reg)
        {
            try
            {
                if (Session["U_ID"] != null)
                {
                    long ID = Convert.ToInt64(Session["U_ID"]);
                    UserWalletViewModel WalletLIST = new UserWalletViewModel();
                    if (ModelState.IsValid)
                    {
                        WalletLIST.List_UserWallet_BYId = ALLReportBAL.ListWallet_By_Id(ID, reg.fromdate, reg.todate);
                        return View(WalletLIST);
                    }
                    else
                    {
                        WalletLIST.message = "Please Enter Valid From Date Or To Date ";
                        reg.fromdate = System.DateTime.Now.Date.ToString();
                        reg.todate = "";
                        WalletLIST.List_UserWallet_BYId = ALLReportBAL.ListWallet_By_Id(ID, reg.fromdate, reg.todate);
                        return View(WalletLIST);
                    }
                }
                else
                {
                    return RedirectToAction("../Register/Login");
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        public ActionResult OperatorWiseReport()
        {
            if (Session["U_ID"] != null)
            {
                long ID = Convert.ToInt64(Session["U_ID"]);

                TransactionModel opwiselist = new TransactionModel();
                opwiselist.transanctionlist = ALLReportBAL.opwise_by_id(ID);
                return View(opwiselist);
            }
            else
            {
                return RedirectToAction("../Register/Login");
            }


            return View();
        }
        public ActionResult DailyReport()
        {

            if (Session["U_ID"] != null)
            {
                long ID = Convert.ToInt64(Session["U_ID"]);
                string todaydate = "";
                TransactionStatus opwiselist = new TransactionStatus();
                // opwiselist.transtts = Utility.CountTransaction(ID, todaydate);
                opwiselist.listtransaction = Utility.CountTransactionforuser(ID, todaydate);
                return View(opwiselist);
            }
            else
            {
                return RedirectToAction("../Register/Login");
            }
        }
        [HttpPost]
        public ActionResult DailyReport(TransactionStatus reg)
        {
            long ID = Convert.ToInt64(Session["U_ID"]);
            TransactionStatus opwiselist = new TransactionStatus();
            if (Session["U_ID"] != null)
            {
                if (ModelState.IsValid)
                {
                    opwiselist.listtransaction = Utility.CountTransactionforuser(ID, reg.dailydate);
                    return View(opwiselist);
                }
                else
                {
                    reg.dailydate = "";
                    opwiselist.listtransaction = Utility.CountTransactionforuser(ID, reg.dailydate);
                    opwiselist.message = "Please Enter Date .!";
                    return View(opwiselist);
                }
            }
            else
            {
                return RedirectToAction("../Register/Login");
            }
        }

        public ActionResult AccountReport()
        {
            if (Session["U_ID"] != null)
            {
                long ID = Convert.ToInt64(Session["U_ID"]);

                TransactionModel opwiselist = new TransactionModel();
                opwiselist.transanctionlist = ALLReportBAL.account_report_byID(ID);
                return View(opwiselist);
            }
            else
            {
                return RedirectToAction("../Register/Login");
            }


            //return View();
        }

        public ActionResult Jioreport() 
        {
            try
            {
                List<Vendor_InptOutPut_ReqDetailsViewModels> listvendormodel = new List<Vendor_InptOutPut_ReqDetailsViewModels>();
              //  DateTime? fdate, DateTime? tdate
                string fdate = System.DateTime.Today.Date.ToString();
                string tdate = ""; 
              
                    long ID = Convert.ToInt64(Session["U_ID"]);
                    TransactionModel jioselist = new TransactionModel();
                    jioselist.listvendr = ALLReportBAL.jioresponsereport(fdate, tdate);
                    foreach (var item in jioselist.listvendr)
                    {
                        Vendor_InptOutPut_ReqDetailsViewModels model = new Vendor_InptOutPut_ReqDetailsViewModels();
                        model.S_No = item.S_No;
                        model.REFERENCE_NUMBER = item.REFERENCE_NUMBER;
                        model.ReqDate = item.ReqDate;
                        model.ResDate = item.ResDate;
                        model.Output_Responce = item.Output_Responce;
                        string[] msg = model.Output_Responce.Split(',');
                        model.Output_Responce = msg[7].ToString();
                        model.Service_name = item.Service_name;
                        listvendormodel.Add(model);
                    }
                    jioselist.listvendrreqmdl = listvendormodel;
                    return View(jioselist);
             
                
            }
            catch (Exception)
            {

                return View();
            }
            
        }
        [HttpPost]
        public ActionResult Jioreport(TransactionModel reg)
        {
            long ID = Convert.ToInt64(Session["U_ID"]);
            TransactionModel jioselist = new TransactionModel();
            try
            {
                List<Vendor_InptOutPut_ReqDetailsViewModels> listvendormodel = new List<Vendor_InptOutPut_ReqDetailsViewModels>();
                //  DateTime? fdate, DateTime? tdate
           
                              
                if (ModelState.IsValid)
                {
                    jioselist.listvendr = ALLReportBAL.jioresponsereport(reg.fromdate, reg.todate);
                    foreach (var item in jioselist.listvendr)
                    {
                        Vendor_InptOutPut_ReqDetailsViewModels model = new Vendor_InptOutPut_ReqDetailsViewModels();
                        model.S_No = item.S_No;
                        model.REFERENCE_NUMBER = item.REFERENCE_NUMBER;
                        model.ReqDate = item.ReqDate;
                        model.ResDate = item.ResDate;
                        model.Output_Responce = item.Output_Responce;
                        try
                        {
                            string[] msg = model.Output_Responce.Split(',');
                            model.Output_Responce = msg[7].ToString();
                        }
                        catch (Exception ex)
                        {
                            model.Output_Responce = item.Output_Responce;
                        }                       
                        model.Service_name = item.Service_name;
                        listvendormodel.Add(model);
                    }
                    jioselist.listvendrreqmdl = listvendormodel;
                    return View(jioselist);
                }
                else
                {
                   string fdate = System.DateTime.Today.Date.ToString();
                    string tdate = "";
                    jioselist.listvendr = ALLReportBAL.jioresponsereport(fdate, tdate);
                    foreach (var item in jioselist.listvendr)
                    {
                        Vendor_InptOutPut_ReqDetailsViewModels model = new Vendor_InptOutPut_ReqDetailsViewModels();
                        model.S_No = item.S_No;
                        model.REFERENCE_NUMBER = item.REFERENCE_NUMBER;
                        model.ReqDate = item.ReqDate;
                        model.ResDate = item.ResDate;
                        model.Output_Responce = item.Output_Responce;
                        string[] msg = model.Output_Responce.Split(',');
                        model.Output_Responce = msg[7].ToString();
                        model.Service_name = item.Service_name;
                        listvendormodel.Add(model);
                    }
                    jioselist.listvendrreqmdl = listvendormodel;
                    jioselist.message = "Please Select or Enter Valid FromDate And Todate..!";
                    return View(jioselist);
                }
              


            }
            catch (Exception)
            {
                return View(jioselist);
            }

        }

    }
}
