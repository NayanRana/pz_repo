﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PaymezonBAL;
using PaymezonApp.Models;
using PaymezonDAL;

namespace PaymezonApp.Controllers
{
    public class FundRequestController : Controller
    {
       
        public ActionResult Index()
        {
            if (Session["U_ID"] != null)
            {
                string  fromdate = System.DateTime.Now.Date.ToString();
                string todate = "";
                long ID = Convert.ToInt64(Session["U_ID"]);
                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                FundRequestViewModels fndlist = new FundRequestViewModels();
                fndlist.LstFund_Req = FundRequestBAL.Fund_Request_List_byId(ID,fromdate,todate);
                return View(fndlist);
            }
            else
            {
                return RedirectToAction("../Register/Login");
            }
        }
        [HttpPost]
        public ActionResult Index(FundRequestViewModels reg)
        {
            if (Session["U_ID"] != null)
            {
                long ID = Convert.ToInt64(Session["U_ID"]);
                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                FundRequestViewModels fndlist = new FundRequestViewModels();
                if (ModelState.IsValid)
                {
                    
                    fndlist.LstFund_Req = FundRequestBAL.Fund_Request_List_byId(ID, reg.fromdate, reg.todate);
                    return View(fndlist);
                }
                else
                {
                    reg.fromdate = System.DateTime.Now.ToString();
                    reg.todate = "";

                    fndlist.message = "Please Enter Valid From Date OR To Date.!";
                    fndlist.LstFund_Req = FundRequestBAL.Fund_Request_List_byId(ID, reg.fromdate, reg.todate);
                    return View(fndlist);
                }

            }
            else
            {
                return RedirectToAction("../Register/Login");
            }
        }

      
        public PartialViewResult AddFundRequest()
        {
            long ID = Convert.ToInt64(Session["U_ID"]);
            
            FundRequestViewModels obj = new FundRequestViewModels();
            obj.dalfund = FundRequestBAL.usr_detail_byId(ID);

            return PartialView(obj);
        }

        [HttpPost]
        public ActionResult CreateFund(Models.FundRequestViewModels reg)
        {
            try
            {

                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                Fund_Request usr = new Fund_Request();

                long ID = Convert.ToInt64(Session["U_ID"]);
                string labelID = Convert.ToString(this.Session["Label_ID"]);

                usr.Request_date_time = reg.Request_date_time;
                usr.User_Type = reg.dalfund.User_Type;
                usr.Fk_User_Id = ID;
                usr.User_Id = reg.dalfund.Label_Id;
                usr.Name = reg.dalfund.Portal_FName;
                usr.Pay_Made = reg.Pay_Made;
                usr.Amount = reg.Amount;
                usr.Bank_Name = reg.Bank_Name;
                usr.Bank_Txn_Id = reg.Bank_Txn_Id;
                usr.Transaction_Id = reg.Transaction_Id;
                usr.Status = "Pending";
                usr.Request_Status = "Pending";
                usr.Created_By = ID;
                usr.Created_Date = System.DateTime.Now;
                usr.Is_Deleted = false;

              

              
                db.Fund_Request.Add(usr);
                db.SaveChanges();
                return RedirectToAction("../FundRequest/index");
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult SuceessFundDetail()
        {

            string fdate = System.DateTime.Today.ToString();
            string tdate = "";

            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            FundRequestViewModels fndlist = new FundRequestViewModels();
            fndlist.LstFund_Req = FundRequestBAL.Success_Fund_List(fdate, tdate);
           // fndlist.fromdate = System.DateTime.Now;
            return View(fndlist);

        }

        [HttpPost]
        public ActionResult SuceessFundDetail(FundRequestViewModels reg)
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            FundRequestViewModels fndlist = new FundRequestViewModels();
            if (ModelState.IsValid)
            {
               
                fndlist.LstFund_Req = FundRequestBAL.Success_Fund_List(reg.fromdate, reg.todate);
                return View(fndlist);
            }
            else
            {
                string fdate = System.DateTime.Today.ToString();
                string tdate = "";
                fndlist.LstFund_Req = FundRequestBAL.Success_Fund_List(fdate, tdate);
                fndlist.message = "Please Select Fromdate or Todate";
                return View(fndlist);

            }
        }
	}
}