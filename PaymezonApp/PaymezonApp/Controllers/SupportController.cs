﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PaymezonBAL;
using PaymezonApp.Models;
using PaymezonDAL;


namespace PaymezonApp.Controllers
{
    public class SupportController : Controller
    {

        public ActionResult Index()
        {
            if (Session["U_ID"] != null)
            {
                string fromdate = System.DateTime.Now.Date.ToString();
                string todate = "";
                long ID = Convert.ToInt64(Session["U_ID"]);
                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                SupportViewModels msglist = new SupportViewModels();
                msglist.List_SupportDetail = SupportBAL.MSG_List_byId(ID, fromdate,todate);
                return View(msglist);
            }
            else
            {
                return RedirectToAction("../Register/Login");
            }
        }
        [HttpPost]
        public ActionResult Index(SupportViewModels reg)
        {
            if (Session["U_ID"] != null)
            {

                long ID = Convert.ToInt64(Session["U_ID"]);
                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                SupportViewModels msglist = new SupportViewModels();
                if (ModelState.IsValid)
                {
                    msglist.List_SupportDetail = SupportBAL.MSG_List_byId(ID, reg.fromdate, reg.todate);
                    return View(msglist);
                }
                else
                {
                    msglist.message = "Please Select From Date or To Date.!";
                    msglist.List_SupportDetail = SupportBAL.MSG_List_byId(ID, reg.fromdate, reg.todate);
                    return View(msglist);
                }
            }
            else
            {
                return RedirectToAction("../Register/Login");
            }
        }
        public PartialViewResult AddNewSupport(string amt, string moblie, string OperatorCode)
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            SupportViewModels obj = new SupportViewModels();
            //obj.Date = dt;
            obj.Company_name = OperatorCode;
            obj.Amount = amt;
            obj.Mobile_Number = moblie;
            //obj.Company = 1;
            obj.List_Company = CompanyBAL.CompanyList();
            return PartialView(obj);
        }

        public PartialViewResult ReplySupport(string TokenId)
        {
            SupportViewModels obj = new SupportViewModels();
            obj.Token_no = TokenId;
            obj.SupportDetail_byTokenID = SupportBAL.GetsinglesupportByid(TokenId);
            return PartialView(obj);

        }
        [HttpPost]

        public ActionResult ReplyToAdmin(Models.SupportViewModels reg)
        {
            try
            {
                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                Support_Detail usr = new Support_Detail();

                long ID = Convert.ToInt64(Session["U_ID"]);

                usr.Token_no = reg.Token_no;
                usr.Content_Detail = reg.Content_Detail;
                usr.Created_By = Convert.ToInt32(ID);
                usr.Created_Date = System.DateTime.Now;
                usr.Is_Deleted = false;
                db.Support_Detail.Add(usr);
                db.SaveChanges();

                return RedirectToAction("../Support/index");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public ActionResult AddSupport(Models.SupportViewModels reg)
        {
            try
            {

                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                Support_Detail usr = new Support_Detail();

                long ID = Convert.ToInt64(Session["U_ID"]);
                string labelID = Convert.ToString(this.Session["Label_ID"]);
                String usertype = Convert.ToString(this.Session["User_Type"]);
                usr.Token_no = "TK" + new Random().Next();
                usr.User_type = usertype;
                usr.Company = reg.Company;
                usr.Company_name = reg.Company_name;
                usr.Date = reg.Date;
                usr.Amount = reg.Amount;
                usr.Mobile_Number = reg.Mobile_Number;
                usr.Query_Type = reg.Query_Type;
                usr.Content_Detail = reg.Content_Detail;
                usr.Created_By = Convert.ToInt32(ID);
                usr.Created_Date = System.DateTime.Now;
                usr.Is_Deleted = false;
                db.Support_Detail.Add(usr);
                db.SaveChanges();
                return RedirectToAction("../Support/Index");

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}