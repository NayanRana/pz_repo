﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PaymezonBAL;
using PaymezonApp.Models;
using PaymezonDAL;

namespace PaymezonApp.Controllers
{
    public class AssignApiController : Controller
    {
        public ActionResult Index()
        {
            AssignApiViewModels obj = new AssignApiViewModels();
            obj.list_assign_api = AssignApiBAL.ListAssignApi();
            //  obj.listvendor = AssignApiBAL.VendorList();
            obj.listcompany = CompanyBAL.CompanyListorderbyname();
            obj.listApi = AssignApiBAL.ListApi();
            return View(obj);
        }
        // first called AddAssignApi function after than change the function due to some changes by DJ
        [HttpPost]
        public ActionResult Index(Models.AssignApiViewModels reg)
        {
            try
            {
                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                reg.listcompany = CompanyBAL.CompanyListorderbyname();
                reg.listApi = AssignApiBAL.ListApi();
                reg.list_assign_api = AssignApiBAL.ListAssignApi();
                Assign_Api usr = new Assign_Api();
                int? opid = reg.Operator_Id;

                if (ModelState.IsValid)
                {
                    usr.Pk_Assign_Api_Id = reg.Pk_Assign_Api_Id;
                    //usr.Operator_Id = id;
                    usr.Operator_Id = reg.Operator_Id;
                    usr.Grater_than_Amount = reg.Grater_than_Amount;
                    usr.Api_Id_by_Amount = reg.Api_Id_by_Amount;
                    usr.Normal_Api_Id = reg.Normal_Api_Id;
                    usr.Emergency_Api_Id = reg.Emergency_Api_Id;
                    usr.Is_Deleted = false;
                    usr.Created_By = 0;
                    usr.Created_Date = System.DateTime.Now;
                    usr.Modified_By = 0;
                    usr.Modified_Date = System.DateTime.Now;
                    if (usr.Pk_Assign_Api_Id == 0)
                    {
                        bool checkop = AssignApiBAL.checkoperatorforalreadyassign(opid);
                        if (checkop == true)
                        {
                            AssignApiBAL.AddAssignApi(usr);
                            reg.message = "Successfully Inserted Assigned Operator.";
                            reg.list_assign_api = AssignApiBAL.ListAssignApi();
                            return View(reg);
                        }
                        else
                        {
                            reg.message = "Sorry..This Operator Already Assigned";
                            return View(reg);
                        }
                    }
                    else
                    {
                        AssignApiBAL.UpdateAssignApi(usr);
                        reg.message = "Successfully Updated Assigned Operator.";
                        reg.list_assign_api = AssignApiBAL.ListAssignApi();
                        return View(reg);
                    }
                }
                else
                {
                    reg.message = "Please Select or Entered All values.";
                    return View(reg);
                }


                //   return RedirectToAction("../AssignApi/Index");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public PartialViewResult CreateAssignApi()
        {
            AssignApiViewModels obj = new AssignApiViewModels();
            obj.listvendor = AssignApiBAL.VendorList();
            obj.listApi = AssignApiBAL.ListApi();
            return PartialView(obj);
        }

        public PartialViewResult EditAssignApi(Int32 id)
        {
            AssignApiViewModels obj = new AssignApiViewModels();
            obj.listvendor = AssignApiBAL.VendorList();
            obj.listApi = AssignApiBAL.ListApi();
            obj.dalassignapi = AssignApiBAL.GetAssignApiByid(id);
            return PartialView(obj);

        }
        [HttpPost]
        public ActionResult AddAssignApi(Models.AssignApiViewModels reg)
        {
            try
            {


                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                Assign_Api assapi = new Assign_Api();
                int id = AssignApiBAL.getvendoridbycmpnyid(reg.Operator_Id);
                Assign_Api usr = new Assign_Api();
                if (id != 0)
                {
                    bool checkuser = AssignApiBAL.checkoperatorforassign(id);

                    if (checkuser == true)
                    {

                        usr.Pk_Assign_Api_Id = reg.Pk_Assign_Api_Id;
                        usr.Operator_Id = id;
                        //  usr.Operator_Id = reg.Operator_Id;
                        usr.Grater_than_Amount = reg.Grater_than_Amount;
                        usr.Api_Id_by_Amount = reg.Api_Id_by_Amount;
                        usr.Normal_Api_Id = reg.Normal_Api_Id;
                        usr.Emergency_Api_Id = reg.Emergency_Api_Id;
                        usr.Is_Deleted = false;
                        usr.Created_By = 0;
                        usr.Created_Date = System.DateTime.Now;
                        usr.Modified_By = 0;
                        usr.Modified_Date = System.DateTime.Now;
                    }
                    else
                    {

                        usr.Pk_Assign_Api_Id = reg.Pk_Assign_Api_Id;
                        usr.Operator_Id = id;
                        //usr.Operator_Id = reg.Operator_Id;
                        usr.Grater_than_Amount = reg.Grater_than_Amount;
                        usr.Api_Id_by_Amount = reg.Api_Id_by_Amount;
                        usr.Normal_Api_Id = reg.Normal_Api_Id;
                        usr.Emergency_Api_Id = reg.Emergency_Api_Id;
                        usr.Is_Deleted = false;
                        usr.Created_By = 0;
                        usr.Created_Date = System.DateTime.Now;
                        usr.Modified_By = 0;
                        usr.Modified_Date = System.DateTime.Now;
                    }
                    //var opName = db.VendorDiversions.AsEnumerable().FirstOrDefault(e => e.OperatorID == reg.Operator_Id).Operator.ToString();
                    //assapi = db.Assign_Api.Where(d => d.Operator_Id == reg.Operator_Id && d.Created_By == reg.Created_By).FirstOrDefault();
                    if (usr.Pk_Assign_Api_Id == 0)
                    {
                        AssignApiBAL.AddAssignApi(usr);
                    }
                    else
                    {
                        AssignApiBAL.UpdateAssignApi(usr);
                    }
                }
                return RedirectToAction("../AssignApi/Index");


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public ActionResult DeleteAssignApi(long id)
        {

            AssignApiBAL.DeleteAssignApi(id);
            return RedirectToAction("../AssignApi/Index");

        }
        [HttpPost]
        public ActionResult UpdateAssignApi(Models.AssignApiViewModels reg)
        {
            try
            {

                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                Assign_Api usr = new Assign_Api();

                usr.Pk_Assign_Api_Id = reg.dalassignapi.Pk_Assign_Api_Id;
                usr.Operator_Id = reg.dalassignapi.Operator_Id;
                usr.Grater_than_Amount = reg.dalassignapi.Grater_than_Amount;
                usr.Api_Id_by_Amount = reg.dalassignapi.Api_Id_by_Amount;
                usr.Normal_Api_Id = reg.dalassignapi.Normal_Api_Id;
                usr.Emergency_Api_Id = reg.dalassignapi.Emergency_Api_Id;
                usr.Modified_By = 0;
                usr.Modified_Date = System.DateTime.Now;

                AssignApiBAL.UpdateAssignApi(usr);

                return RedirectToAction("../AssignApi/Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}