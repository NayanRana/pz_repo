﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PaymezonBAL;
using PaymezonApp.Models;
using PaymezonDAL;

namespace PaymezonApp.Controllers
{
    public class UserWalletController : Controller
    {
        //
        // GET: /UserWallet/
        public ActionResult Index()
        {
            if (Session["U_ID"] != null)
            {
                long ID = Convert.ToInt64(Session["U_ID"]);
                string fromdate = System.DateTime.Now.Date.ToString();
                string todate = "";
                UserWalletViewModel WalletLIST = new UserWalletViewModel();
                WalletLIST.List_UserWallet_BYId = UserWalletBAL.ListWallet_By_Id(ID, fromdate, todate);
                return View(WalletLIST);
            }
            else
            {
                return RedirectToAction("../Register/Login");
            }
        }

        [HttpPost]
        public ActionResult Index(UserWalletViewModel reg)
        {
            if (Session["U_ID"] != null)
            {
                long ID = Convert.ToInt64(Session["U_ID"]);
                
                UserWalletViewModel WalletLIST = new UserWalletViewModel();
                if (ModelState.IsValid)
                {
                    WalletLIST.List_UserWallet_BYId = UserWalletBAL.ListWallet_By_Id(ID, reg.fromdate, reg.todate);
                    return View(WalletLIST);
                }
                else
                {
                    string fromdate = System.DateTime.Now.Date.ToString();
                    string todate = "";
                    WalletLIST.List_UserWallet_BYId = UserWalletBAL.ListWallet_By_Id(ID, fromdate, todate);
                    WalletLIST.message = "Please Select From Or To Date..!";
                    return View(WalletLIST);
                }
            }
            else
            {
                return RedirectToAction("../Register/Login");
            }
        }

        public ActionResult TransactionReport()
        {
            try
            {
                if (Session["U_ID"] != null)
                {
                    long ID = Convert.ToInt64(Session["U_ID"]);
                    string fdate = System.DateTime.Now.Date.ToString();
                    string tdate = "";

                    PaymezonDBEntities1 db = new PaymezonDBEntities1();
                    TransactionModel obj = new TransactionModel();
                    obj.transanctionlist = TransactionBAL.Transaction_List(ID,fdate,tdate);
                    return View(obj);
                }
                else
                {
                    return RedirectToAction("../Register/Login");
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult TransactionReport(TransactionModel reg)
        {
            try
            {
                if (Session["U_ID"] != null)
                {
                    long ID = Convert.ToInt64(Session["U_ID"]);
                   

                    PaymezonDBEntities1 db = new PaymezonDBEntities1();
                    TransactionModel obj = new TransactionModel();

                    if (ModelState.IsValid)
                    {
                        obj.transanctionlist = TransactionBAL.Transaction_List(ID, reg.fromdate, reg.todate);
                        return View(obj);
                    }
                    else
                    {
                        reg.fromdate = System.DateTime.Now.Date.ToString();
                        reg.todate = "";
                        obj.message = "Please Select From Or To Date..!";
                        obj.transanctionlist = TransactionBAL.Transaction_List(ID, reg.fromdate, reg.todate);
                        return View(obj);
                    }
                }
                else
                {
                    return RedirectToAction("../Register/Login");
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }
    }
}