﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PaymezonDAL;
using PaymezonBAL;
using PaymezonApp;
using PaymezonApp.Models;
using System.Data.Entity;
using System.Web.Routing;

namespace PaymezonApp.Controllers
{
    public class VendorDiversionController : Controller
    {
        public ActionResult Index()
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            VendorDiversionViewModel obj = new VendorDiversionViewModel();
            obj.listApi = AssignApiBAL.ListApi();
            obj.listcompany = CompanyBAL.CompanyList();
            //vendordivlist.List_Commision = CommisionBAL.List_Commision();
            //vendordivlist.List_Company = CompanyBAL.CompanyList();
            return View(obj);
        }

        public ActionResult AddVendorDiversion(List<VendorDiversion> lstData)
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            VendorDiversion vendordiversion = new VendorDiversion();

            VendorDiversionBAL.addoreditvendorlist(lstData);

            return RedirectToAction("../VendorDiversion/Index");
        }

        [HttpGet]
        public object selectVendordiversionlist(int? start, int length, string apiname)
        {

            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            
            var lst = VendorDiversionBAL.listvendorbyapiname(apiname);
            DatatablePaggeing d = new DatatablePaggeing();
          
                d.aaData = lst;
                d.iTotalDisplayRecords = 25;
                d.iTotalRecords = 25;
                d.sEcho = "";
                return Json(d, JsonRequestBehavior.AllowGet);
           
           
        }
    }
}
