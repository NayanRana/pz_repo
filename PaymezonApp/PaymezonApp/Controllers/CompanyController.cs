﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PaymezonBAL;
using PaymezonApp.Models;
using PaymezonDAL;


namespace PaymezonApp.Controllers
{
    public class CompanyController : Controller
    {
        //
        // GET: /Company/
        public ActionResult Index()
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            CompanyViewModels cmnylist = new CompanyViewModels();
            cmnylist.List_Company = CompanyBAL.CompanyListorderbyname();
            return View(cmnylist);
        }

        public PartialViewResult CreateCompany()
        {
            return PartialView();
        }

        public PartialViewResult EditCompany (Int32 id)
        {

            CompanyViewModels obj = new CompanyViewModels();
            obj.dalcompany = CompanyBAL.GetCompanyByid(id);
            return PartialView(obj);
        }
        [HttpPost]
        public ActionResult AddCompany(Models.CompanyViewModels reg)
        {
            try
            {

                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                Company_Detail usr = new Company_Detail();
                usr.Company_Name = reg.Company_Name;
                usr.Company_Code = reg.Company_Code;
                usr.Company_Detail1 = reg.Company_Detail1;          
                usr.Is_deleted = false;
                db.Company_Detail.Add(usr);
                db.SaveChanges();
                return RedirectToAction("../Company/Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public ActionResult DeleteCompany(long id)
        {

            CompanyBAL.DeleteCompany(id);
            return RedirectToAction("../Company/Index");

        }
        [HttpPost]
        public ActionResult UpdateCompany(Models.CompanyViewModels reg)
        {
            try
            {

                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                Company_Detail usr = new Company_Detail();

                usr.Pk_Company_Id = reg.dalcompany.Pk_Company_Id;
                usr.Company_Name = reg.dalcompany.Company_Name;
                usr.Company_Code = reg.dalcompany.Company_Code;
                usr.Company_Detail1 = reg.dalcompany.Company_Detail1;

                CompanyBAL.UpdateCompany(usr);

                return RedirectToAction("../Company/Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult Blockoperatorbyuser() 
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            CompanyViewModels cmnylist = new CompanyViewModels();
            cmnylist.List_Company = CompanyBAL.CompanyList();
            return View(cmnylist);
        }

	}
}