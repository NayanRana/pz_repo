﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PaymezonDAL;
using System.Net.NetworkInformation;
using PaymezonBAL;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace PaymezonApp.Controllers
{
    public class APIRechargerController : Controller
    {
        [HttpGet]
        //
        // GET: /APIRecharger/
        public ActionResult Index(string mobile, string amt, string ctxnid, string oprator, string cid, string userName, string pass)
        {
            string dataString = "";

            string str = "http://103.231.32.126:85/?mobile=" + mobile + "&amt=" + amt + "&ctxnid=" + ctxnid + "&operator=" + oprator + "&type=Recharge&rt=4&cid=" + cid + "";

            string ipAdd = Utility.GetMacAddress();
            TempData["notice"] = ipAdd;

            //string hostName = Dns.GetHostName(); // Retrive the Name of HOST         
            //string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();
            // bool lst = PaymezonBAL.IPBAL.CheckIP(userName, pass);
            //var ipDetl = lst.Where(c => c.Ip_One == ipAdd || c.Ip_Two == ipAdd || c.Ip_Three == ipAdd || c.Ip_Four == ipAdd || c.Ip_Five == ipAdd);

            UserRegistration usr = new RegistrationBAL().GetUserByIP(userName, pass, ipAdd);
            if (usr != null && usr.Pk_Register_ID > 0)
            {

                string strmsg = Utility.CheckRechargeDetail(Convert.ToDecimal(amt), usr,Convert.ToInt64(mobile));

                if (strmsg == "Succ")
                {
                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(str);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream s = (Stream)response.GetResponseStream();
                    StreamReader readStream = new StreamReader(s);
                    dataString = readStream.ReadToEnd();
                    response.Close();

                    if (dataString.Trim().Split('|')[1].ToString() == "Transaction Successful ")
                    {
                        using (var db = new PaymezonDBEntities1())
                        {
                            string labelID = usr.Label_Id;
                            string usertype = usr.User_Type;
                            Recharge_Detail rech = new Recharge_Detail();
                            rech.Action_On_Amount = "Dr";
                            rech.Agent_Id = labelID;
                            rech.Amount = Convert.ToDecimal(amt);
                            rech.Circle_Code = "8";
                            rech.Fk_RegisterId = usr.Pk_Register_ID;
                            rech.Mobile_no = Convert.ToDecimal(mobile);
                            rech.Operator_Code = oprator;
                            rech.Recharge_By = usr.Pk_Register_ID;
                            rech.Recharge_Date = System.DateTime.Now;
                            rech.Transaction_Id = "TN" + DateTime.Now.Year + rech.Pk_Recharge_Id.ToString("D" + 5);  //new Random().Next();
                            rech.Ref_number = "RF" + DateTime.Now.Year + rech.Pk_Recharge_Id.ToString("D" + 5);  // new Random().Next();
                            rech.Status = "Pending";
                            rech.User_Type = usertype;
                            rech.ipAdd = ipAdd;
                            rech.C_Tranx_ID = ctxnid;
                            string strMsgAmt = PaymezonBAL.RechargeBAL.InsertRegBAL(rech);
                            Utility.balance = Convert.ToDecimal(strMsgAmt);
                            TempData["notice"] = dataString.Trim().Split('|')[1].ToString();
                        }
                    }
                    else
                    {
                        TempData["notice"] = dataString.Trim().Split('|')[1].ToString();
                    }
                }
                else
                {
                    TempData["notice"] = strmsg;
                }
            }

            else
            {
                TempData["notice"] = ipAdd + "User does not exist or Ip Address not registered.";
            }
            return View();

        }

        [HttpGet]
        public ActionResult APIresponse(string tid, string status, string opid)
        
{
            try
            {              

                if (tid != null)
                {   
                    string str = TransactionBAL.UpdateStatus(status, tid, opid);

                    if (str != "")
                    {
                        
                        string[] amt = str.Split('|');
                        if (status == "0")
                        {
                            Utility.balance = Utility.balance + Convert.ToDecimal(amt[1]);
                        }
                       string msg = " tid=" + tid + " and status=" + status + " and operatorid= " + opid;
                      // LogDetailBAL.AddLog(msg, "Api Response");

                       DataTable dt = new DataTable();
                       dt.Clear();
                       dt.Columns.Add("Date");
                       dt.Columns.Add("Reponse");
                       DataRow dr = dt.NewRow();
                       dr["Date"] = System.DateTime.Now;
                       dr["Reponse"] = msg;
                       dt.Rows.Add(dr);
                       
                       UploadDataTableToExcel(dt);

                   
                       return Redirect(str);
                    }
                    else
                    {
                        TempData["notice"] = "Something wrong.";
                    }
                }
               

                //string strUrl = "http://paymezone.softnsource.com/APIRecharger/APIresponse?tid=" + tid + "&status=" + status + "&opid=" + opid;                    
                //WebRequest request = HttpWebRequest.Create(strUrl);
                //request.Method = "GET";
                //HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                //Stream s = (Stream)response.GetResponseStream();
                //StreamReader readStream = new StreamReader(s);
                //string dataString = readStream.ReadToEnd();

            }
            catch (Exception ex)
            {
                LogDetailBAL.AddLog(ex.Message.ToString(), "Api Response");
            }
            return Json("Some issue", JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult APIresponseOFAnsh(string Mobno, string status)
        {
            try
            {
               

                if (Mobno != null)
                {
                    string str = TransactionBAL.UpdateStatusOfAnsh(status, Mobno);

                    if (str != "")
                    {

                        //string[] amt = str.Split('|');
                        //if (status == "0")
                        //{
                        //    Utility.balance = Utility.balance + Convert.ToDecimal(amt[1]);
                        //}
                        //string msg = " tid=" + Mobno + " and status=" + status ;
                        //// LogDetailBAL.AddLog(msg, "Api Response");

                        //DataTable dt = new DataTable();
                        //dt.Clear();
                        //dt.Columns.Add("Date");
                        //dt.Columns.Add("Reponse");
                        //DataRow dr = dt.NewRow();
                        //dr["Date"] = System.DateTime.Now;
                        //dr["Reponse"] = msg;
                        //dt.Rows.Add(dr);

                        //UploadDataTableToExcel(dt);


                        return Redirect(str);
                    }
                    else
                    {
                        TempData["notice"] = "Something wrong.";
                    }
                }


                //string strUrl = "http://paymezone.softnsource.com/APIRecharger/APIresponse?tid=" + tid + "&status=" + status + "&opid=" + opid;                    
                //WebRequest request = HttpWebRequest.Create(strUrl);
                //request.Method = "GET";
                //HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                //Stream s = (Stream)response.GetResponseStream();
                //StreamReader readStream = new StreamReader(s);
                //string dataString = readStream.ReadToEnd();

            }
            catch (Exception ex)
            {
                LogDetailBAL.AddLog(ex.Message.ToString(), "Api Response");
            }
            return Json("Some issue", JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult Pendingresponse(string tid, string status, string opid)
        {
            
            try
            {
               
                List<Recharge_Detail> lstpending = new List<Recharge_Detail>();
                lstpending = PaymezonBAL.RechargeBAL.pendingBAL();
                foreach (var item in lstpending)
                {
                    string strUrl = "http://103.231.32.126:85/status.php?cid=5&ctxnid=" + item.Transaction_Id;
                    WebRequest request = HttpWebRequest.Create(strUrl);
                    request.Method = "GET";
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream s = (Stream)response.GetResponseStream();
                    StreamReader readStream = new StreamReader(s);
                    string dataString = readStream.ReadToEnd();
                   
                    var checkstatus = PaymezonBAL.RechargeBAL.changestatus(item.Transaction_Id, dataString.Split('|')[1]);
                   

                }
               

                
            }
            catch (Exception ex)
            {
                LogDetailBAL.AddLog(ex.Message.ToString(), "Api Response");

            }
          
            return Json("succ", JsonRequestBehavior.AllowGet);
        }


        protected void UploadDataTableToExcel(DataTable dtRecords)
        {//@"C:\Users\hadi\Desktop\test.xlsx"


          
            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);

            // Render grid view control.
            GridView GridView1 = new GridView();
            GridView1.DataSource = dtRecords;
            GridView1.DataBind();
            GridView1.RenderControl(htw);

            // Write the rendered content to a file.
            string renderedGridView = sw.ToString();
            renderedGridView = renderedGridView.Replace("\"", "'");
            //renderedGridView = renderedGridView.Replace("<tr><th scope='col'>Date</th><th scope='col'>Reponse</th></tr>", " ");

           // renderedGridView = renderedGridView.Replace("<tr>", " " );
            renderedGridView = renderedGridView.Replace("<th scope='col'>Date</th><th scope='col'>Reponse</th>", " ");
            renderedGridView = renderedGridView.Replace("</tr><tr>", " ");
            //renderedGridView = renderedGridView.Replace("col'", "");
            //renderedGridView = renderedGridView.Replace(">Reponse</th></tr><tr>", "");
            
            System.IO.File.AppendAllText(@"C:\Users\hadi\Desktop\ResponseLog.xls", renderedGridView);


            
            //string XlsPath = Server.MapPath(@"~/img/test.xls");
            //string attachment = string.Empty;
            //if (XlsPath.IndexOf("\\") != -1)
            //{
            //    string[] strFileName = XlsPath.Split(new char[] { '\\' });
            //    attachment = "attachment; filename=" + strFileName[strFileName.Length - 1];
            //}
            //else
            //    attachment = "attachment; filename=" + XlsPath;
            try
            {
            //   /// Response.ClearContent();
            //    Response.AddHeader("content-disposition", attachment);
            //    Response.ContentType = "application/vnd.ms-excel";
            //    string tab = string.Empty;

            //    foreach (DataColumn datacol in dtRecords.Columns)
            //    {
            //        Response.Write(tab + datacol.ColumnName);
            //        tab = "\t";
            //    }
            //    Response.Write("\n");

            //    foreach (DataRow dr in dtRecords.Rows)
            //    {
            //        tab = "";
            //        for (int j = 0; j < dtRecords.Columns.Count; j++)
            //        {
            //            Response.Write(tab + Convert.ToString(dr[j]));
            //            tab = "\t";
            //        }

            //        Response.Write("\n");
            //    }
            //    Response.End();
            }
            catch (Exception ex)
            {
                //Response.Write(ex.Message);
            }
        }
    }


}