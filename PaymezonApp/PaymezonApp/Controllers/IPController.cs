﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PaymezonBAL;
using PaymezonApp.Models;
using PaymezonDAL;

namespace PaymezonApp.Controllers
{
    public class IPController : Controller
    {
        //
        // GET: /IP/
        public ActionResult Index()
        {
            if (Session["U_ID"] != null)
            {
                long ID = Convert.ToInt64(Session["U_ID"]);
                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                IPViewModels IPLIST = new IPViewModels();
                IPLIST.List_IP_By_Id = IPBAL.ListIP_By_Id(ID);
                return View(IPLIST);
            }
            else
            {
                return RedirectToAction("../Register/Login");
            }
        }
        public PartialViewResult AddIP()
        {
            return PartialView();

        }

        public PartialViewResult EditIP(Int32 id)
        {
            IPViewModels obj = new IPViewModels();
            obj.dalIP = IPBAL.GetIpByid(id);
            return PartialView(obj);

        }

        public ActionResult UpdateIP(Models.IPViewModels reg)
        {
            try
            {
                long ID = Convert.ToInt64(Session["U_ID"]);
                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                Ip_Address_Detail usr = new Ip_Address_Detail();

                usr.Pk_Ip_Id = reg.dalIP.Pk_Ip_Id;
                usr.Ip_One = reg.dalIP.Ip_One;
                usr.Ip_Two = reg.dalIP.Ip_Two;
                usr.Ip_Three = reg.dalIP.Ip_Three;
                usr.Ip_Four = reg.dalIP.Ip_Four;
                usr.Ip_Five = reg.dalIP.Ip_Five;
                usr.Modify_By = ID;
                usr.ModifyDate = System.DateTime.Now;

                IPBAL.UpdateIP(usr);

                return RedirectToAction("../IP/Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult CreateIP(Models.IPViewModels reg)
        {
            try
            {

                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                Ip_Address_Detail usr = new Ip_Address_Detail();

                long ID = Convert.ToInt64(Session["U_ID"]);
                string labelID = Convert.ToString(this.Session["Label_ID"]);

                usr.Ip_One = reg.Ip_One;
                usr.Ip_Two = reg.Ip_Two;
                usr.Ip_Three = reg.Ip_Three;
                usr.Ip_Four = reg.Ip_Four;
                usr.Ip_Five = reg.Ip_Five;
                usr.fk_user_Id = ID;
                usr.Created_By = ID;
                usr.Created_Date = System.DateTime.Now;
                usr.Is_Deleted = false;

                db.Ip_Address_Detail.Add(usr);
                db.SaveChanges();
                return RedirectToAction("../IP/index");

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
	}
}