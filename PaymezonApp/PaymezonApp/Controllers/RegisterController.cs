﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PaymezonBAL;
using PaymezonApp.Models;
using PaymezonDAL;
using System.Text;

namespace PaymezonApp.Controllers
{
    public class RegisterController : Controller
    {
        //
        // GET: /Register/
        public ActionResult Index()
        {
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            //List<Models.RegisterViewModel> lst = new List<Models.RegisterViewModel>();
            // List<UserRegistration> lstusr = db.UserRegistrations.ToList();
            // foreach (UserRegistration item in lstusr)
            // {
            //     Models.RegisterViewModels modl = new RegisterViewModels();
            //     modl.Author_Email = item.Author_Email;
            //     modl.Author_MobNo = item.Author_MobNo;
            //     modl.Author_Name = item.Author_Name;
            //     modl.Pk_Register_ID = item.Pk_Register_ID;
            //     lst.Add(modl);
            // }
            Models.RegisterViewModel md = new RegisterViewModel();
            RegistrationBAL UserDetail = new RegistrationBAL();
            Models.RegisterViewModel mdl = new RegisterViewModel();

            md.lstUser = UserDetail.UserDetail(); //db.UserRegistrations.ToList();
            return View(md);
        }

        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SubmitLogin(Models.LoginViewModel log)
        
        {
            this.Session["U_ID"] = "";
            this.Session["Label_ID"] = "";
            this.Session["A_Email"] = "";
            this.Session["User_Type"] = "";
            this.Session["User_Name"] = "";

            //if (log.UserName == "admin" && log.Password == "admin")
            //{
            //    return RedirectToAction("/Index");
            //}

            if (log.UserName != "" && log.Password != "")
            {
                RegistrationBAL balReg = new RegistrationBAL();
                //  PaymezonDBEntities1 db = new PaymezonDBEntities1();
                // db.UserRegistrations.Any(c => c.Author_Email == log.UserName && c.Password == log.Password);
                UserRegistration usrdetail = balReg.CheckUser(log.UserName, log.Password);

                if (usrdetail != null)
                {
                    this.Session["U_ID"] = usrdetail.Pk_Register_ID;
                    this.Session["Label_ID"] = usrdetail.Label_Id;
                    this.Session["A_Email"] = usrdetail.Author_Email;
                    this.Session["User_Type"] = usrdetail.User_Type;
                    this.Session["User_Name"] = usrdetail.Author_Name;
                    decimal? dv = usrdetail.User_Amount.Count > 0 ? usrdetail.User_Amount.FirstOrDefault().Actual_Amount : 0;
                    long id = usrdetail.Pk_Register_ID;
                     DateTime todaydate = System.DateTime.Now.Date;
                     using (var db = new PaymezonDBEntities1())
                     {
                         var CountSuccecssamount = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "SUCCESS" && (td.Transaction_Date_Time > todaydate) select td.Net_Amount).Sum();
                         var CountFailureamount = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "FAILED" && (td.Transaction_Date_Time > todaydate) select td.Net_Amount).Sum();
                         var CountPendingamount = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "PENDING" && (td.Transaction_Date_Time > todaydate) select td.Net_Amount).Sum();
                         var counttxnamount = (from td in db.Transaction_Table where td.Created_By == id && (td.Transaction_Date_Time > todaydate) select td.Net_Amount).Sum();
                         var CountSuccecss = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "SUCCESS" && (td.Transaction_Date_Time >= todaydate) select td.Status).Count();
                         var CountFailure = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "FAILED" && (td.Transaction_Date_Time > todaydate) select td.Status).Count();
                         var CountPending = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "PENDING" && (td.Transaction_Date_Time > todaydate) select td.Status).Count();
                         var counttxn = (from td in db.Transaction_Table where td.Created_By == id && (td.Transaction_Date_Time > todaydate) select td.Status).Count();
                  

                        Utility.CountSuccecssamount = CountSuccecssamount == null ? 0 : CountSuccecssamount;
                        Utility.CountPendingamount = CountPendingamount == null ? 0 : CountPendingamount;
                        Utility.CountFailureamount = CountFailureamount == null ? 0 : CountFailureamount;
                        Utility.counttxn = counttxn == null ? 0 : counttxn;
                        Utility.counttxnamount = counttxnamount == null ? 0 : counttxnamount;
                        Utility.SuccessTxn = CountSuccecss == null ? 0 : CountSuccecss;
                        Utility.CountFailure = CountFailure == null ? 0 : CountFailure;
                        Utility.CountPending = CountPending == null ? 0 : CountPending;
                     }
                     this.Session["Amt"] = dv.ToString();
                    Utility.balance = dv;
                    Utility.userId = usrdetail.Pk_Register_ID;
                    return RedirectToAction("Dashboard", "Home");
                }
                else
                {
                    return RedirectToAction("/Login");
                }
            }
            else
            {
                return RedirectToAction("/Login");
            }
            /// return View();
        }

        public PartialViewResult AddUser()
        {
          
            RegisterViewModels modl = new RegisterViewModels();
           
            return PartialView(modl);
        }

        [HttpPost]
        public ActionResult AddUser(Models.RegisterViewModels reg)
        {
            try
            {
               
                    PaymezonDBEntities1 db = new PaymezonDBEntities1();
                    UserRegistration usr = new UserRegistration();
                    usr.fk_Mas_Distributor_Id = reg.MasterDistributor;
                    usr.fk_Distributor_Id = reg.Distributor;
                    usr.Author_Email = reg.Author_Email;
                    usr.Author_MobNo = reg.Author_MobNo;
                    usr.Author_Name = reg.Author_Name;
                    usr.City = reg.City;
                    usr.Company_Name = reg.Company_Name;
                    usr.Copyright = reg.Copyright;
                    usr.Domain_Name = reg.Domain_Name;
                    usr.Fk_Country_Id = reg.Fk_Country_Id;
                    usr.Fk_State_Id = reg.Fk_State_Id;
                    usr.Helpdesk_Email = reg.Helpdesk_Email;
                    usr.Logo = reg.Logo;
                    usr.Mobile_Number = reg.Mobile_Number;
                    usr.Office_Address = reg.Office_Address;
                    usr.PAN_Num = reg.PAN_Num;
                    usr.Panel_Title = reg.Panel_Title;
                    usr.Pincode = reg.Pincode;
                    usr.Portal_FName = reg.Portal_FName;
                    usr.Portal_LName = reg.Portal_LName;
                    usr.Portal_MName = reg.Portal_MName;
                    usr.Reg_Date = System.DateTime.Now;
                    usr.Password = new Random().Next().ToString();
                    usr.Return_URL = reg.Return_URL;
                    usr.Is_Deleted = false;
                    usr.Is_Active = true;
                    if (reg.User_Type == "1")
                    {
                        usr.Label_Id = "WL" + new Random().Next(5).ToString() + DateTime.Now.Year;
                    }
                    else if (reg.User_Type == "2")
                    {
                        usr.Label_Id = "MD" + new Random().Next(5).ToString() + DateTime.Now.Year;

                    }
                    else if (reg.User_Type == "3")
                    {
                        usr.Label_Id = "DS" + new Random().Next(5).ToString() + DateTime.Now.Year;

                    }
                    else if (reg.User_Type == "4")
                    {
                        usr.Label_Id = "AG" + new Random().Next(5).ToString() + DateTime.Now.Year;

                    }
                    else if (reg.User_Type == "5")
                    {
                        usr.Label_Id = "AU" + new Random().Next(5).ToString() + DateTime.Now.Year;
                    }
                    usr.User_Type = reg.User_Type;
                    db.UserRegistrations.Add(usr);
                    db.SaveChanges();

                    string EmailID = reg.Author_Email;


                    string Subject = "Welcome to Paymezon Api MR./Miss." + reg.Author_Name;

                    StringBuilder str = new StringBuilder();


                    str.Append("<html><head><title></title><style>.mytext {padding: 20px 30px 0;color: #61696d;font-family: 'Lato',Helvetica,Arial,sans-serif;font-size: 18px;font-weight: 400;line-height: 25px;}.btn {font-size: 16px;font-family: 'Lato',Helvetica,Arial,sans-serif;color: #ffffff;text-decoration: none;color: #ffffff;text-decoration: none;border-radius: 3px;border-top: 10px solid #48a7fe;border-bottom: 10px solid #48a7fe;border-left: 15px solid #48a7fe;border-right: 15px solid #48a7fe;display: inline-block;background-color: #48a7fe;}.headtext{padding: 30px 30px 0;color: #3a4449;font-family: 'Lato',Helvetica,Arial,sans-serif;font-size: 25px;line-height: 23px;}</style></head><body>");
                    str.Append("<div style='background-color:#f4f4f4;margin:0!important;padding:0!important'>");
                    str.Append("<div style='display:none;font-size:1px;color:#fefefe;line-height:1px;font-family:'Lato',Helvetica,Arial,sans-serif;max-height:0px;max-width:0px;opacity:0;overflow:hidden'></div>");
                    str.Append("<table border='0' cellpadding='0' cellspacing='0' width='100%'><tbody><tr>");
                    str.Append("<td bgcolor='#f4f4f4' align='center'>");
                    str.Append("<table border='0' cellpadding='0' cellspacing='0' width='600'>");
                    str.Append("<tbody><tr> ");
                    str.Append("<td align='center' valign='top' style='padding:20px 10px'>");
                    str.Append("<a href='http://74.208.129.24/' target='_blank' data-saferedirecturl='http://74.208.129.24/'>");
                    str.Append("<img alt='Logo' src='http://b2b.paymezon.com/images/paymezon.png' width='255' height='63' style='display:block;width:255px;max-width:255px;min-width:255px;font-family:'Lato',Helvetica,Arial,sans-serif;color:#ffffff;font-size:18px' border='0' class='CToWUd'>");
                    str.Append("</a></td></tr></tbody></table></td></tr>");
                    str.Append("<tr><td bgcolor='#f4f4f4' align='center' style='padding:0px 10px 0px 10px'>");
                    str.Append("<table border='0' cellpadding='0' cellspacing='0' width='600' > ");
                    str.Append("<tbody><tr bgcolor='#ffffff'>");
                    str.Append("<td bgcolor='#ffffff' align='left' valign='top' class='headtext'>");
                    str.Append("<h1 style='font-size:25px;font-weight:400;margin:0'>Dear Mr./Miss." + reg.Author_Name + ",</h1></td></tr>");
                    str.Append("<tr><td bgcolor='#ffffff' align='left'  class='mytext'>");
                    str.Append("<p style='margin:0'>City :- " + reg.City + " </p>");
                    str.Append("<p style='margin:0'>Mobile No :- " + reg.Author_MobNo + " </p></td> </tr>");
                    str.Append("<tr><td bgcolor='#ffffff' align='left' class='mytext'><p style='margin:0'>");
                    str.Append("welcome to Paymezon Api's Family.Your API account has been created successfully.</p></td></tr> ");
                    str.Append("<tr><td bgcolor='#ffffff' align='left' class='mytext'>");
                    str.Append("<p style='margin:0'>Just to avoid Un-authorized access please change your Password immediately,</p>");
                    str.Append("<p style='margin:0'>User Name :- " + usr.Author_Email + "</p>");
                    str.Append("<p style='margin:0'>Password :- " + usr.Password + "</p></td> </tr>");
                    str.Append("<tr><td bgcolor='#ffffff' align='left' class='mytext'>");
                    str.Append("<p style='margin:0'>Your account will be active as soon as you request for wallet transfer.</p>");
                    str.Append("<p style='margin:0'>Bank details as follow.. </p>");
                    str.Append("<p style='margin:0'>Company Name:- Paymezon E-comm Pvt Ltd.</p>");
                    str.Append("<p style='margin:0'>Account Number:- 50200010928053</p>");
                    str.Append("<p style='margin:0'>IFSC:- HDFC0000048</p>");
                    str.Append("<p style='margin:0'> Branch:- VEJALPUR ,AHMEDABAD.</p>");
                    str.Append("</td></tr><tr>");
                    str.Append("<td bgcolor='#ffffff' align='left' class='mytext'>");
                    str.Append("<a href='http://www.pzapi.com/'  class='btn' target='_blank' data-saferedirecturl='http://www.pzapi.com/'>Change Your Password</a></br></br>");
                    //str.Append("<p style='margin:0'>Sincerely</p>");        
                    //str.Append("<p style='margin:0'>PZ API</p></td> </tr>");
                    //str.Append("<p style='margin:0'>+91 990992332</p></td> </tr>");
                    str.Append("</td></tr>");
                    str.Append("<tr><td bgcolor='#ffffff' align='left' class='mytext'>");
                    str.Append("<p style='margin:0'>Sincerely,</p>");
                    str.Append("<p style='margin:0'>PZ API</p>");
                    str.Append("<p style='margin:0'>+91 990992332</p></td> </tr></tbody></table></td></tr><tr>");
                    str.Append("<td bgcolor='#f4f4f4' align='center'><table border='0' cellpadding='0' cellspacing='0' width='600' height='20' ><tbody><tr><td bgcolor='#f4f4f4' align='center'></td></tr></tbody></table></td></tr></tbody>");
                    str.Append("</table></div></body></html>");

                    Utility.SendMailmsg(str.ToString(), EmailID, "", "", Subject);
                    Response.Write("Mail send");

                    if (reg.reg_Type == "admin")
                    {
                        User_Amount usramt = new User_Amount();
                        usramt.Fk_User_ID = usr.Pk_Register_ID;
                        usramt.Total_Credit = 0;
                        usramt.Total_Debit = 0;
                        usramt.Actual_Amount = 0;
                        usramt.Updated_Date = System.DateTime.Now;
                        db.User_Amount.Add(usramt);
                        db.SaveChanges();
                        int userid = Convert.ToInt16(usr.Pk_Register_ID);
                        CommisionBAL.addnewcommisionfornewuser(userid);
                    }
                    if (reg.reg_Type == "admin")
                    {
                        return RedirectToAction("../Admin/Adminuser");
                    }
                    else
                    {
                        return RedirectToAction("/Index");
                    }
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}