﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PaymezonBAL;
using PaymezonApp.Models;
using PaymezonDAL;


namespace PaymezonApp.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            if (Session["U_ID"] != null)
             
            {

                long ID = Convert.ToInt64(Session["U_ID"]);
                DateTime todaydate = System.DateTime.Now.Date;
                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                RechargeViewModels obj = new RechargeViewModels();
                obj.List_Company = CompanyBAL.CompanyList();
                obj.transaction = Utility.CountTransaction(ID, todaydate);
                obj.list_transaction = Utility.liststatusbyapiname();
                obj.RCG_List = Utility.counttransactionbyserviceprovider(ID);
                getcountBYUSER();
                return View(obj);
            }
            else
            {
                return RedirectToAction("../Register/Login");
            }
        }
        public object getcountBYUSER()
        {
            DateTime todaydate = System.DateTime.Now.Date;
           long ID = Convert.ToInt64(Session["U_ID"]);
           return Utility.CountTransaction(ID, todaydate);

        }
	}
}