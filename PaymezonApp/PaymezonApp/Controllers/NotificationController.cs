﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PaymezonBAL;
using PaymezonApp.Models;
using PaymezonDAL;
using System.Text;

namespace PaymezonApp.Controllers
{
    public class NotificationController : Controller
    {
        //
        // GET: /Notification/
        
        public PartialViewResult Index()        
        {
            string date = "";
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            SupportViewModels msglist = new SupportViewModels();

            msglist.List_SupportDetail = SupportBAL.MSG_LIST(date);
            var lstNote = SupportBAL.MSG_LIST_Noti(0,5);
            StringBuilder str = new StringBuilder();
             foreach (var item in lstNote)
            {
                str.Append("<a href='#' >");
                str.Append("<div class='icon-circle bg-blue-grey'><i class='material-icons'>comment</i></div>");
                str.Append("<div class='menu-info'><h4><b>" + item.Company + "</b>"+ item.Query_Type + "--"+item.Token_no + "</h4>");
                str.Append( "<p> <i class='material-icons'>access_time</i>" + item.Created_Date + " </p></div> </a>");
            }
            IHtmlString strHtml = new HtmlString(str.ToString());
            Session["Note"] = strHtml;
            return PartialView(msglist);
        }


	}
}