﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PaymezonBAL;
using PaymezonApp.Models;
using PaymezonDAL;


namespace PaymezonApp.Controllers
{
    public class ProfileController : Controller
    {
        //
        // GET: /Profile/
        public ActionResult Index()
        {    
            long ID = Convert.ToInt64(Session["U_ID"]);
            PaymezonDBEntities1 db = new PaymezonDBEntities1();
            RegistrationBAL regBal = new RegistrationBAL();
            Models.RegisterViewModels md = new RegisterViewModels();
            md.dalusrreg = ProfileBAL.Editprofile(ID);
            md.lstComm = CommisionBAL.List_CommisionByPlanname();
            md.lstAgnt = regBal.DDLUser("4").ToList();
            md.lstDis = regBal.DDLUser("3").ToList();
            md.lstMDis = regBal.DDLUser("2").ToList();
            md.lstWL = regBal.DDLUser("1").ToList();
            return View(md);
        }
        public ActionResult Edituser(Models.RegisterViewModels reg)
        {
            try
            {

                PaymezonDBEntities1 db = new PaymezonDBEntities1();
                UserRegistration usr = new UserRegistration();
                usr.Pk_Register_ID = reg.dalusrreg.Pk_Register_ID;
                usr.Author_Email = reg.dalusrreg.Author_Email;
                usr.Author_MobNo = reg.dalusrreg.Author_MobNo;
                usr.Author_Name = reg.dalusrreg.Author_Name;
                usr.City = reg.dalusrreg.City;
                usr.Company_Name = reg.dalusrreg.Company_Name;
                usr.Copyright = reg.dalusrreg.Copyright;
                usr.Domain_Name = reg.dalusrreg.Domain_Name;
                usr.Label_Id = reg.dalusrreg.Label_Id;


              //  usr.Fk_Country_Id = reg.dalusrreg.Fk_Country_Id;
               // usr.Fk_State_Id = reg.dalusrreg.Fk_State_Id;
                usr.Helpdesk_Email = reg.dalusrreg.Helpdesk_Email;
              //  usr.Logo = reg.dalusrreg.Logo;
                usr.Mobile_Number = reg.dalusrreg.Mobile_Number;
                usr.Office_Address = reg.dalusrreg.Office_Address;
                usr.PAN_Num = reg.dalusrreg.PAN_Num;
                usr.Panel_Title = reg.dalusrreg.Panel_Title;
                usr.Pincode = reg.dalusrreg.Pincode;
                usr.Portal_FName = reg.dalusrreg.Portal_FName;
                usr.Portal_LName = reg.dalusrreg.Portal_LName;
                usr.Portal_MName = reg.dalusrreg.Portal_MName;
                usr.Modified_Date =System.DateTime.Now;
                usr.Modified_By = Convert.ToInt64(Session["U_ID"]);
                usr.Return_URL = reg.dalusrreg.Return_URL;
                ProfileBAL.Updateprofile(usr);
             //   AdminBAL.UpdateAPI(usr);
                //return SuccessResponse("Event Updated Successfully");
                //db.Api_Detail.Add(usr);
                //db.SaveChanges();
                return RedirectToAction("../Profile/Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PartialViewResult ChangePassword()
        {

            return PartialView();
        }

        public ActionResult EditPassword(Models.RegisterViewModels reg)
        {
            try
            {
                if (reg.newpassword == reg.newpassword)
                {

                    PaymezonDBEntities1 db = new PaymezonDBEntities1();
                    UserRegistration usr = new UserRegistration();
                    long ID = Convert.ToInt64(Session["U_ID"]);
                    try
                    {
                        var datamodify = db.UserRegistrations.Where(d => d.Pk_Register_ID == ID && d.Password == reg.Password).FirstOrDefault();
                        if (datamodify != null)
                        {
                            datamodify.Password = reg.newpassword;
                            db.SaveChanges();
                        }
                       

                        return RedirectToAction("../Profile/Index");
                    }
                    catch (Exception ex)
                    {
                        
                        throw ex;
                    }
                  
                }
                else
                {
                    return RedirectToAction("../Profile/Index");
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
	}
}