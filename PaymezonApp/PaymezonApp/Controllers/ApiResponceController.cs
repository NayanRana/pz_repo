﻿using PaymezonBAL;
using PaymezonDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Net.Http;
using System.Runtime.Remoting.Contexts;
using PaymezonApp.Models;
using System.Net.NetworkInformation;
using System.Net;
using System.Net.Mail;
using System.Web.Script.Serialization;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace PaymezonApp.Controllers
{
    public class ApiResponceController : Controller
    {

        public object leoRechargeAPIResponse(string accountId, string txid, string Transtype, string opr_id = "2")
        {

            try
            {
               
                if (Transtype != "")
                {
                    string url = txid + "@" + accountId + "@" + Transtype;
                    using (var db = new PaymezonDBEntities1())
                    {
                        Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                        input.Service_name = "leoRechargeAPI";
                        input.REFERENCE_NUMBER = accountId;
                        input.COURENTLEVEL = null;
                        input.Input_Request = "789";
                        input.Output_Responce = url;
                        input.ReqDate = System.DateTime.Now;
                        input.ResDate = System.DateTime.Now;
                        db.Vendor_InptOutPut_ReqDetails.Add(input);
                        db.SaveChanges();

                    }
                }
                string apitxID = accountId;

               // string txnid = TransactionBAL.gettxnbymobno(txid);

                string txnid = TransactionBAL.Gettxnidbyapitxnid(apitxID);
                if (Transtype == "s")
                {
                    Transtype = "SUCCESS";
                    string updateapiid = TransactionBAL.updateapiidbytxnid(txnid, txid);
                }
                else 
                {
                    Transtype = "FAILED";
                    string updateapiid = TransactionBAL.updateapiidbytxnid(txnid, "NA");
                }

                string requsturl = TransactionBAL.Getrequrl(txnid);

                string clienttxid = TransactionBAL.Getctxid(txnid);

                string retUrl = requsturl + "?ctxnid=" + clienttxid + "&status=" + Transtype + "&apitransactionID=" + txid; 
                string str = TransactionBAL.UpdateStatus(Transtype, txnid, opr_id);
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
                string dataString = "";
                string respons;
                request.Method = "GET";
                request.Timeout = 180000;
                request.ReadWriteTimeout = 180000;
                request.KeepAlive = false;
                request.Proxy = null;
                request.ServicePoint.ConnectionLeaseTimeout = 60000;
                request.ServicePoint.MaxIdleTime = 60000;
                HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
                Stream s = (Stream)responsee.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                dataString = readStream.ReadToEnd();
                respons = dataString;
                responsee.Close();
                s.Close();
                readStream.Close();
               

                return "Succ";
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public object MyRioAPIResponse(string ipay_id, string cust_no, string amt, string opr_id, string status, string bal, string agent_id)
        {

            try
            {
                agent_id = Request.QueryString["rid"].ToString();
                ipay_id = Request.QueryString["txid"].ToString();
                opr_id = Request.QueryString["prvdr"].ToString();
                cust_no = Request.QueryString["rchNo"].ToString();
                status = Request.QueryString["transtype"].ToString();
                amt = Request.QueryString["amt"].ToString();
                bal = Request.QueryString["bal"].ToString();

                string rtnturl = TransactionBAL.Getrequrl(ipay_id);

               

                if (!string.IsNullOrEmpty(Request.QueryString["transtype"]))
                {
                    status = Request.QueryString["transtype"];
                }
                else if (!string.IsNullOrEmpty(Request["transtype"]))
                {
                    status = Request["transtype"];
                }
                else if (!string.IsNullOrEmpty(Request.Form["transtype"]))
                {
                    status = Request.Form["transtype"];
                }
                if (status != "")
                {
                    string url = ipay_id + "@" + agent_id + "@" + opr_id + "@" + status + "@" + cust_no;
                    using (var db = new PaymezonDBEntities1())
                    {
                        Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                        input.Service_name = "MyRioAPI";
                        input.REFERENCE_NUMBER = ipay_id;
                        input.COURENTLEVEL = null;
                        input.Input_Request = "789";
                        input.Output_Responce = url;
                        input.ReqDate = System.DateTime.Now;
                        input.ResDate = System.DateTime.Now;
                        db.Vendor_InptOutPut_ReqDetails.Add(input);
                        db.SaveChanges();

                    }
                }
                string clienttxid = TransactionBAL.Getctxid(ipay_id);

                string retUrl = rtnturl + "?ctxnid=" + clienttxid + "&status=" + status;

                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
                string str = TransactionBAL.UpdateStatus(status, ipay_id, opr_id);
                string dataString = "";
                string respons;
                request.Method = "GET";
                request.Timeout = 180000;
                request.ReadWriteTimeout = 180000;
                request.KeepAlive = false;
                request.Proxy = null;
                request.ServicePoint.ConnectionLeaseTimeout = 60000;
                request.ServicePoint.MaxIdleTime = 60000;
                HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
                Stream s = (Stream)responsee.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                dataString = readStream.ReadToEnd();
                respons = dataString;
                responsee.Close();
                s.Close();
                readStream.Close();
                return "Succ";
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public object NilkanthAPIResponse(string status, string uniqueid, string operator_id, string transaction_id)
        {
            try
            {

                status = Request.QueryString["status"];
                uniqueid = Request.QueryString["uniqueid"];
                operator_id = Request.QueryString["operator_id"];
                transaction_id = Request.QueryString["transaction_id"];

                string requsturl = TransactionBAL.Getrequrl(transaction_id);

                string clienttxid = TransactionBAL.Getctxid(transaction_id);

                string retUrl = requsturl + "?ctxnid=" + clienttxid + "&status=" + status;
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);


                if (!string.IsNullOrEmpty(Request.QueryString["transtype"]))
                {
                    status = Request.QueryString["transtype"];
                }
                else if (!string.IsNullOrEmpty(Request["transtype"]))
                {
                    status = Request["transtype"];
                }
                else if (!string.IsNullOrEmpty(Request.Form["transtype"]))
                {
                    status = Request.Form["transtype"];
                }
                if (status != "")
                {
                    string url = uniqueid + "@" + operator_id + "@" + status + "@" + transaction_id;
                    using (var db = new PaymezonDBEntities1())
                    {
                        Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                        input.Service_name = "NilkanthAPI";
                        input.REFERENCE_NUMBER = transaction_id;
                        input.COURENTLEVEL = null;
                        input.Input_Request = "789";
                        input.Output_Responce = url;
                        input.ReqDate = System.DateTime.Now;
                        input.ResDate = System.DateTime.Now;
                        db.Vendor_InptOutPut_ReqDetails.Add(input);
                        db.SaveChanges();

                    }
                }
                string str = TransactionBAL.UpdateStatus(status, transaction_id, operator_id);
                string dataString = "";
                string respons;
                request.Method = "GET";
                request.Timeout = 180000;
                request.ReadWriteTimeout = 180000;
                request.KeepAlive = false;
                request.Proxy = null;
                request.ServicePoint.ConnectionLeaseTimeout = 60000;
                request.ServicePoint.MaxIdleTime = 60000;
                HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
                Stream s = (Stream)responsee.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                dataString = readStream.ReadToEnd();
                respons = dataString;
                responsee.Close();
                s.Close();
                readStream.Close();
                return "succ";
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public object MangalamAPIResponse(string status, string uniqueid, string operator_id, string transaction_id)
        {
            if (status != "")
            {
                string url = uniqueid + "@" + operator_id + "@" + status + "@" + transaction_id;
                using (var db = new PaymezonDBEntities1())
                {
                    Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                    input.Service_name = "MangalamAPI";
                    input.REFERENCE_NUMBER = uniqueid;
                    input.COURENTLEVEL = null;
                    input.Input_Request = "789";
                    input.Output_Responce = url;
                    input.ReqDate = System.DateTime.Now;
                    input.ResDate = System.DateTime.Now;
                    db.Vendor_InptOutPut_ReqDetails.Add(input);
                    db.SaveChanges();

                }
            }
            if (status == "Success")
            {
                status = "SUCCESS";
                string updateapiid = TransactionBAL.updateapiidbytxnid(uniqueid, operator_id);
            }
            else if (status == "Pending")
            {
                status = "PENDING";
            }
            else 
            {
                status = "FAILED";
                string updateapiid = TransactionBAL.updateapiidbytxnid(uniqueid, "NA");
            }

            string requsturl = TransactionBAL.Getrequrl(uniqueid);
            string clienttxid = TransactionBAL.Getctxid(uniqueid);
            string retUrl = requsturl + "?ctxnid=" + clienttxid + "&status=" + status + "&apitransactionID=" + operator_id;

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
            string dataString = "";
            string respons;
            request.Method = "GET";
            request.Timeout = 180000;
            request.ReadWriteTimeout = 180000;
            request.KeepAlive = false;
            request.Proxy = null;
            request.ServicePoint.ConnectionLeaseTimeout = 60000;
            request.ServicePoint.MaxIdleTime = 60000;
            HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
            Stream s = (Stream)responsee.GetResponseStream();
            StreamReader readStream = new StreamReader(s);
            dataString = readStream.ReadToEnd();
            respons = dataString;
            responsee.Close();
            s.Close();
            readStream.Close();
            string str = TransactionBAL.UpdateStatus(status, uniqueid, operator_id);

            return "succ";

        }
        public object oyepeAPIResponse(string transtype, string txid, string accountId, string OPID = "2")
        {
            if (transtype != "")
            {
                string url = accountId + "@" + transtype + "@" + txid;
                using (var db = new PaymezonDBEntities1())
                {
                    Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                    input.Service_name = "oyepe";
                    input.REFERENCE_NUMBER = txid;
                    input.COURENTLEVEL = null;
                    input.Input_Request = "789";
                    input.Output_Responce = url;
                    input.ReqDate = System.DateTime.Now;
                    input.ResDate = System.DateTime.Now;
                    db.Vendor_InptOutPut_ReqDetails.Add(input);
                    db.SaveChanges();
                }
            }
            if (transtype == "s")
            {
                transtype = "SUCCESS";
                string updateapiid = TransactionBAL.updateapiidbytxnid(accountId, txid);
            }
               
            else if (transtype == "f" || transtype == "you can't send same Recharge Request for 10 min.")
            {
                transtype = "FAILED";
                string updateapiid = TransactionBAL.updateapiidbytxnid(accountId, "NA");
            }
            string str = TransactionBAL.UpdateStatus(transtype, accountId, OPID);

            string requsturl = TransactionBAL.Getrequrl(accountId);
            string clienttxid = TransactionBAL.Getctxid(accountId);
            string retUrl = requsturl + "?ctxnid=" + clienttxid + "&status=" + transtype + "&apitransactionID=" + txid;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
            string dataString = "";
            string respons;
            request.Method = "GET";
            request.Timeout = 180000;
            request.ReadWriteTimeout = 180000;
            request.KeepAlive = false;
            request.Proxy = null;
            request.ServicePoint.ConnectionLeaseTimeout = 60000;
            request.ServicePoint.MaxIdleTime = 60000;
            HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
            Stream s = (Stream)responsee.GetResponseStream();
            StreamReader readStream = new StreamReader(s);
            dataString = readStream.ReadToEnd();
            respons = dataString;
            responsee.Close();
            s.Close();
            readStream.Close();
            return "succ";

        }
        public object AnikBansalResponse(string REF, string number, string amount, string status, string transid, string yourref)
        {
           // string responseurl = "http://74.208.129.24/API/PZAPIResponse/AnikBansalResponse?ref=RC12345678&number=9845311111&amount=100&status=SUCCESS&transid=98987878&yourref=12345678ABCD";
            if (status != "")
            {
                string url = REF + "@" + status + "@" + transid + "@" + number + "@" + amount + "@" + yourref;
                using (var db = new PaymezonDBEntities1())
                {
                    Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                    input.Service_name = "AnikBansal";
                    input.REFERENCE_NUMBER = yourref;
                    input.COURENTLEVEL = null;
                    input.Input_Request = "789";
                    input.Output_Responce = url;
                    input.ReqDate = System.DateTime.Now;
                    input.ResDate = System.DateTime.Now;
                    db.Vendor_InptOutPut_ReqDetails.Add(input);
                    db.SaveChanges();

                }
                
            }
            //string api = "TU2943297302@SUCCESS@HP0124120321000159@7622079097@10@TN2017636208560393035301";
            if (status == "SUCCESS")
            {
                status = "SUCCESS";
                   string updateapiid = TransactionBAL.updateapiidbytxnid(yourref, transid);
            }
            else if (status == "RESPWAIT")
            {
                status = "PENDING";
            }
            else
            {
                status = "FAILED";
                string updateapiid = TransactionBAL.updateapiidbytxnid(yourref, "NA");
            }

            string requsturl = TransactionBAL.Getrequrl(yourref);

            string clienttxid = TransactionBAL.Getctxid(yourref);


            string str = TransactionBAL.UpdateStatus(status, yourref, "2");

            string retUrl = requsturl + "?ctxnid=" + clienttxid + "&status=" + status + "&apitransactionID=" + transid;

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
            string dataString = "";
            string respons;
            request.Method = "GET";
            request.Timeout = 180000;
            request.ReadWriteTimeout = 180000;
            request.KeepAlive = false;
            request.Proxy = null;
            request.ServicePoint.ConnectionLeaseTimeout = 60000;
            request.ServicePoint.MaxIdleTime = 60000;
            HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
            Stream s = (Stream)responsee.GetResponseStream();
            StreamReader readStream = new StreamReader(s);
            dataString = readStream.ReadToEnd();
            respons = dataString;
            responsee.Close();
            s.Close();
            readStream.Close();

            
            
            return "succ";
        }
        public object MyRechargeResponse(string ReqS, string Code, string RecS, string YRef, string APIRef, string ORef, string N, string A, string R)
        {
            // string responseurl = "http://74.208.129.24/ApiResponce/MyRechargeResponse?ReqS=1&amp;Code=1&amp;RecS=1&amp;YRef=223593554354354311&amp;APIRef=0029893640&amp;ORef=1926348576.&amp;N=9793545898&amp;A=50&amp;R=R2";
            ///ReqS=0&Code=1&RecS=0&YRef=AR4555236&APIRef=R512612771&Oref=UN61263&N=9876543210&A=10&R=received,
            ///9727951851@10@-@MLAI0057237110@TN2017636209507832590875
            if (RecS != "")
            {
                string url = N + "@" + A + "@" + ORef + "@" + APIRef + "@" + YRef ;
                using (var db = new PaymezonDBEntities1())
                {
                    Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                    input.Service_name = "MyRecharge";
                    input.REFERENCE_NUMBER = YRef;
                    input.COURENTLEVEL = null;
                    input.Input_Request = "789";
                    input.Output_Responce = url;
                    input.ReqDate = System.DateTime.Now;
                    input.ResDate = System.DateTime.Now;
                    db.Vendor_InptOutPut_ReqDetails.Add(input);
                    db.SaveChanges();

                }

            }
            //string api = "TU2943297302@SUCCESS@HP0124120321000159@7622079097@10@TN2017636208560393035301";
            if (RecS == "1")
            {
                RecS = "SUCCESS";
                string updateapiid = TransactionBAL.updateapiidbytxnid(YRef, APIRef);
            }
            else if (RecS == "3" || RecS == "0")
            {
                RecS = "PENDING";
            }
            else if (RecS == "7")
            {
                RecS = "FAILED";
                string updateapiid = TransactionBAL.updateapiidbytxnid(YRef, "NA");
            }
            else
            {
                RecS = "PENDING";
            }
            string requsturl = TransactionBAL.Getrequrl(YRef);

            string clienttxid = TransactionBAL.Getctxid(YRef);


            string str = TransactionBAL.UpdateStatus(RecS, YRef, "2");

            string retUrl = requsturl + "?ctxnid=" + clienttxid + "&status=" + RecS + "&apitransactionID=" + APIRef;

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
            string dataString = "";
            string respons;
            request.Method = "GET";
            request.Timeout = 180000;
            request.ReadWriteTimeout = 180000;
            request.KeepAlive = false;
            request.Proxy = null;
            request.ServicePoint.ConnectionLeaseTimeout = 60000;
            request.ServicePoint.MaxIdleTime = 60000;
            HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
            Stream s = (Stream)responsee.GetResponseStream();
            StreamReader readStream = new StreamReader(s);
            dataString = readStream.ReadToEnd();
            respons = dataString;
            responsee.Close();
            s.Close();
            readStream.Close();



            return "succ";
        }
        public object RimApiresponse(string tid, string status, string opid)
        {
            try
            {

                if (status != "")
                {
                    string url = tid + "@" + opid + "@" + status;
                    using (var db = new PaymezonDBEntities1())
                    {
                        Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                        input.Service_name = "RimApiresponse";
                        input.REFERENCE_NUMBER = tid;
                        input.COURENTLEVEL = null;
                        input.Input_Request = "789";
                        input.Output_Responce = url;
                        input.ReqDate = System.DateTime.Now;
                        input.ResDate = System.DateTime.Now;
                        db.Vendor_InptOutPut_ReqDetails.Add(input);
                        db.SaveChanges();

                    }
                }
                string txnid = TransactionBAL.Gettxnidbyapitxnid(tid);

                if (status == "1")
                {
                    status = "SUCCESS";
                    string updateapiid = TransactionBAL.updateapiidbytxnid(txnid, opid);
                }
                else if (status == "0")
                {
                    status = "FAILED";
                    string updateapiid = TransactionBAL.updateapiidbytxnid(txnid, "NA");
                }
                else
                {
                    status = "PENDING";
                }

                string requsturl = TransactionBAL.Getrequrl(txnid);

                string clienttxid = TransactionBAL.Getctxid(txnid);

                string retUrl = requsturl + "?ctxnid=" + clienttxid + "&status=" + status + "&apitransactionID=" + opid;
                string str = TransactionBAL.UpdateStatus(status, txnid, opid);
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
                string dataString = "";
                string respons;
                request.Method = "GET";
                request.Timeout = 180000;
                request.ReadWriteTimeout = 180000;
                request.KeepAlive = false;
                request.Proxy = null;
                request.ServicePoint.ConnectionLeaseTimeout = 60000;
                request.ServicePoint.MaxIdleTime = 60000;
                HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
                Stream s = (Stream)responsee.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                dataString = readStream.ReadToEnd();
                respons = dataString;
                responsee.Close();
                s.Close();
                readStream.Close();


                return "Succ";
                
                

            }
            catch (Exception ex)
            {
                LogDetailBAL.AddLog(ex.Message.ToString(), "RimApiresponse");
            }
            return Json("Some issue", JsonRequestBehavior.AllowGet);
        }


        public object NeelKanthAPIResponse(string clientid, string status, string operatorid)
        {
            if (status != "")
            {
                string url = clientid + "@" + status + "@" + operatorid;
                using (var db = new PaymezonDBEntities1())
                {
                    Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                    input.Service_name = "NeelKanthAPI";
                    input.REFERENCE_NUMBER = clientid;
                    input.COURENTLEVEL = null;
                    input.Input_Request = "789";
                    input.Output_Responce = url;
                    input.ReqDate = System.DateTime.Now;
                    input.ResDate = System.DateTime.Now;
                    db.Vendor_InptOutPut_ReqDetails.Add(input);
                    db.SaveChanges();
                }
            }
            if (status.ToUpper() == "SUCCESS")
            {
                status = "SUCCESS";
                string updateapiid = TransactionBAL.updateapiidbytxnid(clientid,operatorid);
            }

            else if (status.ToUpper() == "FAILURE")
            {
                status = "FAILED";
                string updateapiid = TransactionBAL.updateapiidbytxnid(clientid, "NA");
            }
            string str = TransactionBAL.UpdateStatus(status, clientid, operatorid);

            string requsturl = TransactionBAL.Getrequrl(clientid);
            string clienttxid = TransactionBAL.Getctxid(clientid);
            string retUrl = requsturl + "?ctxnid=" + clienttxid + "&status=" + status + "&apitransactionID=" + operatorid;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
            string dataString = "";
            string respons;
            request.Method = "GET";
            request.Timeout = 180000;
            request.ReadWriteTimeout = 180000;
            request.KeepAlive = false;
            request.Proxy = null;
            request.ServicePoint.ConnectionLeaseTimeout = 60000;
            request.ServicePoint.MaxIdleTime = 60000;
            HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
            Stream s = (Stream)responsee.GetResponseStream();
            StreamReader readStream = new StreamReader(s);
            dataString = readStream.ReadToEnd();
            respons = dataString;
            responsee.Close();
            s.Close();
            readStream.Close();
            return "succ";

        }
       
        public object OOSwebAPI(string txid, string rchNo, string amt, string prvdr, string transtype, string bal, string rid)
        {
           // string response = "txid=Operator Id&rchNo=Recharge Number&amt=Amount&prvdr=Operator Code&transtype=SUCCESS&bal=Remain Balance&rid=yourrecharge ID";
            try
            {
                txid = Request.QueryString["txid"].ToString();
                rchNo = Request.QueryString["rchNo"].ToString();
                amt = Request.QueryString["amt"].ToString();
                prvdr = Request.QueryString["prvdr"].ToString();
                transtype = Request.QueryString["transtype"].ToString();
                bal = Request.QueryString["bal"].ToString();
                rid = Request.QueryString["rid"].ToString();
                if (transtype != "")
                {
                    string url = rid + "@" + txid + "@" + transtype;
                    using (var db = new PaymezonDBEntities1())
                    {
                        Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                        input.Service_name = "OOSwebAPI";
                        input.REFERENCE_NUMBER = rid;
                        input.COURENTLEVEL = null;
                        input.Input_Request = "789";
                        input.Output_Responce = url;
                        input.ReqDate = System.DateTime.Now;
                        input.ResDate = System.DateTime.Now;
                        db.Vendor_InptOutPut_ReqDetails.Add(input);
                        db.SaveChanges();
                    }
                }

                if (transtype.ToUpper() == "SUCCESS")
                {
                    transtype = "SUCCESS";
                    string updateapiid = TransactionBAL.updateapiidbytxnid(rid, txid);
                }
                else if (transtype.ToUpper() == "FAILED")
                {
                    transtype = "FAILED";
                    string updateapiid = TransactionBAL.updateapiidbytxnid(rid, "NA");
                }

                string requsturl = TransactionBAL.Getrequrl(rid);
                string clienttxid = TransactionBAL.Getctxid(rid);
                string str = TransactionBAL.UpdateStatus(transtype, rid, txid);

                string retUrl = requsturl + "?ctxnid=" + clienttxid + "&status=" + transtype + "&apitransactionID=" + txid;

                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
                string dataString = "";
                string respons;
                request.Method = "GET";
                request.Timeout = 180000;
                request.ReadWriteTimeout = 180000;
                request.KeepAlive = false;
                request.Proxy = null;
                request.ServicePoint.ConnectionLeaseTimeout = 60000;
                request.ServicePoint.MaxIdleTime = 60000;
                HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
                Stream s = (Stream)responsee.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                dataString = readStream.ReadToEnd();
                respons = dataString;
                responsee.Close();
                s.Close();
                readStream.Close();
             
                return "Succ";
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public object mystring()
        {
            string txn = "{'Response':{'Success':false,'Data':{'ErrorCode':100,'Error':'Please wait for 10 min.','Supress':false}}}";

            dynamic datares = JsonConvert.DeserializeObject(txn);

            if (datares.Response.Data.ErrorCode.ToString().ToUpper() == "100")
            {
                if (datares.Response.Data.status.ToString().ToUpper() == "SUCCESS")
                {
                    Response.Write("SUCCESS");
                }
                else if (datares.Response.Data.status.ToString().ToUpper() == "PENDING")
                {
                    Response.Write("PENDING");
                }
                else
                {
                    Response.Write("FAILED");
                }
            }
            else
            {
                Response.Write("FAILED");
            }
            //var status = datares.Response.Data.status.ToString().ToUpper();

            
            Response.Write("s");
            return "";
        }
	}
}