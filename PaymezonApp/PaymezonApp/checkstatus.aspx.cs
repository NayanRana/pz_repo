﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Web.Script.Serialization;
using PaymezonDAL;
using System.Net.NetworkInformation;
using PaymezonBAL;


namespace PaymezonApp
{
    public partial class checkstatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string strurl = "";
            string respons;
            string dataString = "";
            string status = "";
            var pendingtrans = TransactionBAL.checkpendingtransaction();
            foreach (var item in pendingtrans)
            {
                var getstatuscheckurl = AdminBAL.getapibyapiname(item.Service_Vendor);
                if (item.Service_Vendor == "oyepe")
                {
                    strurl = getstatuscheckurl.User_Transaction;
                    string txid = "http://oyepe.in/ReCharge/APIs.aspx?Mob=8154014405&amp;message=1001194953566&lt;%20&gt;TN416631301027469157&%20&4565&amp;Source=API";
                    strurl = strurl.Replace("TXNID",item.Ref_No);
                }
                else if (item.Service_Vendor == "leoRechargeAPI")
                {
                   //not available transaction  status url
                }
                else if (item.Service_Vendor == "MangalamApi")
                {
                    strurl = getstatuscheckurl.User_Transaction;
                    string txid = "http://www.mangalamservice.com/api_users/status?login_id=7049&transaction_password=60538722&CLIENTID=TXNID&response_type=CSV";
                    strurl = strurl.Replace("TXNID", item.Ref_No);
                }
                else if (item.Service_Vendor == "AnshApi")
                {
                    //not available transaction  status url
                    
                }
                else if (item.Service_Vendor == "RimApi")
                {
                    //not change request url
                   
                }
                else if (item.Service_Vendor == "AnikBansal")
                {

                    //not available transaction  status url
                }
                else if (item.Service_Vendor == "MYRECHARGE")
                {

                    //not available transaction  status url
                }
                else if (item.Service_Vendor == "VishnuAPI")
                {
                    //not available transaction  status url
                }
                else if (item.Service_Vendor == "JioApi")
                {

                    //not available transaction  status url
                }
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(strurl);
                request.Method = "GET";
                request.Timeout = 180000;
                request.ReadWriteTimeout = 180000;
                request.KeepAlive = false;
                request.Proxy = null;
                request.ServicePoint.ConnectionLeaseTimeout = 60000;
                request.ServicePoint.MaxIdleTime = 60000;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream s = (Stream)response.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                dataString = readStream.ReadToEnd();
                respons = dataString;
                response.Close();
                s.Close();
                readStream.Close();
                if (item.Service_Vendor == "oyepe")
                {
                    //string responsss = "Status:success, 7096829103*Idea*147.00 Rs *1001194953566";
                    string[] res = respons.Split(',');
                    if (res[0].ToUpper().Contains("SUCCESS"))
                    {
                        status = "SUCCESS";
                    }
                    else if (res[0].ToUpper().Contains("FAILED"))
                    {
                        status = "FAILED";  
                    }

                    string str = TransactionBAL.UpdateStatus(status, item.Ref_No, "2");

                }

            }
        }
    }
}