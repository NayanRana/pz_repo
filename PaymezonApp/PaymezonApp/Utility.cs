﻿using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using PaymezonBAL;
using PaymezonDAL;
using PaymezonApp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PaymezonApp.Models;
using System.Data.Entity;
using System.Globalization;

namespace PaymezonApp
{
    public class Utility
    {
        public static long userId = 0;
        public static decimal? balance = 0;
        public static int SuccessTxn = 0;
        public static int CountFailure = 0;
        public static int CountPending = 0;
        public static decimal? CountSuccecssamount = 0;
        public static decimal? CountFailureamount = 0;
        public static decimal? CountPendingamount = 0;
        public static decimal? counttxn = 0;
        public static decimal? counttxnamount = 0;
        public static decimal? debitamount = 0;
        public static decimal? creditamount = 0;
        public static decimal? fundamount = 0;
        public static decimal? totalcommision = 0;
        public static decimal? Rechargeamount = 0;
        public static decimal? totalcreditamount = 0;
        public static decimal? totaldebitamount = 0;
        public static decimal? openingbalance =  0;
        public static decimal? closingbalance = 0;

        public static string CheckRechargeDetail(decimal amt, UserRegistration usr, long mobile)
        {
            try
            {
                ///UserRegistration usr = new RegistrationBAL().GetUserByIP(userNm, pass,ip);
                if (usr != null)
                {

                    balance = usr.User_Amount.First().Actual_Amount; 
                  
                    //UserWalletBAL.CheckBalance(usr.Pk_Register_ID);

                    if (balance >= 1000 || balance != null)
                    {
                        if (amt >= 10)
                        {
                           // return "Succ";
                            using (var db = new PaymezonDBEntities1())
                            {
                                DateTime todaydate = System.DateTime.Today;
                                var tt = db.Transaction_Table.OrderByDescending(d => d.Transcatio_Id).FirstOrDefault(c => c.Request_Amount == amt && c.MobileNo == mobile && DbFunctions.TruncateTime(c.Transaction_Date_Time) >= todaydate).Transaction_Date_Time;
                                if (tt != null)
                                {
                                    DateTime? dt = tt;
                                    DateTime dtNow = System.DateTime.Now;
                                    ///var hours = (dt - dtNow).TotalMinutes;
                                    TimeSpan varTime = ((DateTime)dtNow - (DateTime)dt);
                                    if (varTime.Minutes > 10)
                                    {
                                        return "Succ";
                                    }
                                    else
                                    {
                                        return "Please wait for 10 min.";
                                    }
                                }
                                else
                                {
                                    return "Succ";
                                }
                            }
                            //string str = TransactionBAL.checkRech(mobile, amt);
                            //DateTime? dt = null;
                            //DateTime dtNow = System.DateTime.Now;
                            //dt = usr.Transaction_Table.LastOrDefault(d => d.MobileNo == mobile ).Transaction_Date_Time;
                            /////var hours = (dt - dtNow).TotalMinutes;
                            //TimeSpan varTime = ((DateTime)dtNow - (DateTime)dt);
                            //if (varTime.Minutes > 2)
                            //{
                            //    return "Succ";
                            //}
                            //else
                            //{
                            //    return "Please wait for 5 min.";
                            //}
                        }
                        else
                        {
                            return "Amount must be greater than or equal to Rs. 10.";
                        }
                    }
                    else
                    {
                        return "User balance is less than Rs. 1000";
                    }
                }
                else
                {
                    return "User does not exist.";
                }

            }
            catch (System.Exception ex)
            {
                return "Succ";
            }
        }


        public static string checkblockopbyuser(long registerid, string opname)
        {
            try
            {
              //  BlockOperator_Details blockop = new BlockOperator_Details();
                using (var db = new PaymezonDBEntities1())
                {
                 var blockop = db.BlockOperator_Details.Where(d => d.Oprator_Code == opname && d.Fk_User_id == registerid && d.Is_active == false).FirstOrDefault();
                    if (blockop == null)
                    {
                        return "Succ";
                    }
                    else
                    {
                        return "Fail";
                    }
                }
            }
            catch (Exception)
            {

                return "Fail";
            }
            
            
        }
        public static TransactionStatus CountTransactiondetail()
        {

            try
            {
                DateTime todaydate = System.DateTime.Now.Date;
                using (var db = new PaymezonDBEntities1())
                {
                    var CountSuccecss = (from td in db.Transaction_Table where td.Status == "SUCCESS" && (td.Transaction_Date_Time >= todaydate) select td.Status).Count();
                    var CountFailure = (from td in db.Transaction_Table where td.Status == "FAILED" && (td.Transaction_Date_Time > todaydate) select td.Status).Count();
                    var CountPending = (from td in db.Transaction_Table where td.Status == "PENDING" && (td.Transaction_Date_Time > todaydate) select td.Status).Count();
                    var CountSuccecssamount = (from td in db.Transaction_Table where td.Status == "SUCCESS" && (td.Transaction_Date_Time > todaydate) select td.Net_Amount).Sum();
                    var CountFailureamount = (from td in db.Transaction_Table where td.Status == "FAILED" && (td.Transaction_Date_Time > todaydate) select td.Net_Amount).Sum();
                    var CountPendingamount = (from td in db.Transaction_Table where td.Status == "PENDING" && (td.Transaction_Date_Time > todaydate) select td.Net_Amount).Sum();
                    var counttxn = (from td in db.Transaction_Table where td.Transaction_Date_Time > todaydate select td.Status).Count();
                    var counttxnamount = (from td in db.Transaction_Table where td.Transaction_Date_Time > todaydate select td.Net_Amount).Sum();
                    //var totalamount = (from td in db.User_Amount where td.Fk_User_ID == id select td.Actual_Amount).FirstOrDefault();
                  
                    TransactionStatus objstts = new TransactionStatus();

                    objstts.SuccessTxn = CountSuccecss == null ? 0 : CountSuccecss;
                    objstts.FailTxn = CountFailure == null ? 0 : CountFailure;
                    objstts.PendingTxn = CountPending == null ? 0 : CountPending;
                    objstts.SuccessAmount = CountSuccecssamount == null ? 0 : CountSuccecssamount;
                    objstts.PendingAmount = CountPendingamount == null ? 0 : CountPendingamount;
                    objstts.FailureAmount = CountFailureamount == null ? 0 : CountFailureamount;
                    objstts.TotalTxn = counttxn == null ? 0 : counttxn;
                    objstts.TotalBalance = counttxnamount == null ? 0 : counttxnamount;
                    //    objstts.balance = totalamount == null ? 0 : totalamount;

                    CountSuccecssamount = CountSuccecssamount == null ? 0 : CountSuccecssamount;
                    CountPendingamount = CountPendingamount == null ? 0 : CountPendingamount;
                    CountFailureamount = CountFailureamount == null ? 0 : CountFailureamount;
                    counttxnamount = counttxnamount == null ? 0 : counttxnamount;
                    //balance = totalamount == null ? 0 : totalamount;



                    return (objstts);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;

            }
        }

        public static List<Transaction_Table> liststatusbyapiname()
        {

            try
            {
                DateTime todaydate = System.DateTime.Now.Date;
                using (var db = new PaymezonDBEntities1())
                {
                    List<Transaction_Table> lst = new List<Transaction_Table>();
                    lst = db.Transaction_Table.Where(c=> c.Status=="Success").Distinct().ToList();

                    return lst;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static TransactionStatus CountTransaction(long id, DateTime? todaydate)
        {
            try
            {
               
                using (var db = new PaymezonDBEntities1())
                {
                    db.Configuration.ProxyCreationEnabled = false;
                    var CountSuccecss = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "SUCCESS" && (td.Transaction_Date_Time >= todaydate) select td.Status).Count();
                    var CountFailure = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "FAILED" && (td.Transaction_Date_Time > todaydate) select td.Status).Count();
                    var CountPending = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "PENDING" && (td.Transaction_Date_Time > todaydate) select td.Status).Count();
                    var CountSuccecssamount = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "SUCCESS" && (td.Transaction_Date_Time > todaydate) select td.Net_Amount).Sum();
                    var CountFailureamount = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "FAILED" && (td.Transaction_Date_Time > todaydate) select td.Net_Amount).Sum();
                    var CountPendingamount = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "PENDING" && (td.Transaction_Date_Time > todaydate) select td.Net_Amount).Sum();
                    var counttxn = (from td in db.Transaction_Table where td.Created_By == id && (td.Transaction_Date_Time > todaydate) select td.Status).Count();
                    var counttxnamount = (from td in db.Transaction_Table where td.Created_By == id && (td.Transaction_Date_Time > todaydate) select td.Net_Amount).Sum();
                    var totalamount = (from td in db.User_Amount where td.Fk_User_ID == id select td.Actual_Amount).FirstOrDefault();
                    var totaldebit = (from td in db.User_Wallet where td.Created_By == id && (td.Created_Date > todaydate) select td.Debit_Amount).Sum();
                    var totalcredit = (from td in db.User_Wallet where td.Created_By == id && (td.Created_Date > todaydate) select td.Credit_Amount).Sum();
                    var totalfund = (from td in db.User_Wallet where td.Created_By == id && (td.Created_Date > todaydate) select td.Fund_Amount).Sum();
                    var totalcommision = (from td in db.Transaction_Table where td.Created_By == id && (td.Transaction_Date_Time > todaydate) select td.Total_Commission).Sum();
                    var totalrecharge = (from td in db.Transaction_Table where td.Created_By == id && (td.Transaction_Date_Time > todaydate) select td.Request_Amount).Sum();
                    var totalcreditamount = (from td in db.User_Amount where td.Fk_User_ID == id select td.Total_Credit).FirstOrDefault();
                    var totaldebitamount = (from td in db.User_Amount where td.Fk_User_ID == id select td.Total_Debit).FirstOrDefault();
                   //  var Balance = (from td in db.User_Amount where td.Fk_User_ID == id  select td.Actual_Amount).Sum();
                    TransactionStatus objstts = new TransactionStatus();

                    objstts.SuccessTxn = CountSuccecss == null ? 0 : CountSuccecss;
                    objstts.FailTxn = CountFailure == null ? 0 : CountFailure;
                    objstts.PendingTxn = CountPending == null ? 0 : CountPending;
                    objstts.SuccessAmount = CountSuccecssamount == null ? 0 : CountSuccecssamount;
                    objstts.PendingAmount = CountPendingamount == null ? 0 : CountPendingamount;
                    objstts.FailureAmount = CountFailureamount == null ? 0 : CountFailureamount;
                    objstts.TotalTxn = counttxn == null ? 0 : counttxn;
                    objstts.TotalBalance = counttxnamount == null ? 0 : counttxnamount;
                    objstts.balance = totalamount == null ? 0 : totalamount;
                    objstts.debitamount = totaldebit == null ? 0 : totaldebit;
                    objstts.creditamount = totalcredit == null ? 0 : totalcredit;
                    objstts.fundamount = totalfund == null ? 0 : totalfund;
                    objstts.totalcommision = totalcommision == null ? 0 : totalcommision;
                    objstts.Rechargeamount = totalrecharge == null ? 0 : totalrecharge;
                    objstts.totalcreditamount = totalcreditamount == null ? 0 : totalcreditamount;
                    objstts.totaldebitamount = totaldebitamount == null ? 0 : totaldebitamount;

                  //  objstts.balance = Balance == null? 0: Balance;
                    
                    Rechargeamount = totalrecharge == null ? 0 : totalrecharge;
                    totalcommision = totalcommision == null ? 0 : totalcommision;
                    debitamount = totaldebit == null ? 0 : totaldebit;
                    creditamount = totalcredit == null ? 0 : totalcredit;
                    fundamount = fundamount == null ? 0 : fundamount;
                    CountSuccecssamount = CountSuccecssamount == null ? 0 : CountSuccecssamount;
                    CountPendingamount = CountPendingamount == null ? 0 : CountPendingamount;
                    CountFailureamount = CountFailureamount == null ? 0 : CountFailureamount;
                    counttxnamount = counttxnamount == null ? 0 : counttxnamount;
                    totalamount = totalamount == null ? 0 : totalamount;
                    balance = totalamount == null ? 0 : totalamount;
                    totalcreditamount = totalcreditamount == null ? 0 : totalcreditamount;
                    totaldebitamount = totaldebitamount == null ? 0 : totaldebitamount;

                  

                    return (objstts);
                }


            }
            catch (System.Exception ex)
            {
                throw ex;

            }
        }

        public static List<TransactionStatus> CountTransactionforuser(long id, string date)
        {

            try
            {
                DateTime todaydate = new DateTime();
                if (date != "")
                {
                   
                    todaydate = Convert.ToDateTime(DateTime.ParseExact(date.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture));

                }
                else
                {
                    todaydate = System.DateTime.Now.Date;
                }
                

                using (var db = new PaymezonDBEntities1())
                {
                    db.Configuration.ProxyCreationEnabled = false;
                    var CountSuccecss = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "SUCCESS" && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Status).Count();
                    var CountFailure = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "FAILED" && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Status).Count();
                    var CountPending = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "PENDING" && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Status).Count();
                    var CountSuccecssamount = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "SUCCESS" && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Net_Amount).Sum();
                    var CountFailureamount = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "FAILED" && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Net_Amount).Sum();
                    var CountPendingamount = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "PENDING" && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Net_Amount).Sum();
                    var counttxn = (from td in db.Transaction_Table where td.Created_By == id && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Status).Count();
                    var counttxnamount = (from td in db.Transaction_Table where td.Created_By == id && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Net_Amount).Sum();
                    var countopeningbalance = (from td in db.Transaction_Table where td.Created_By == id && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) orderby td.Created_Date ascending select td.Amount_Before_Due_Date).FirstOrDefault();
                    var countclosingbalance = (from td in db.Transaction_Table where td.Created_By == id && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) orderby td.Created_Date descending select td.Amount_After_Deu_Date).FirstOrDefault();
                    var totalamount = (from td in db.User_Amount where td.Fk_User_ID == id select td.Actual_Amount).FirstOrDefault();
                    var totaldebit = (from td in db.User_Wallet where td.Created_By == id && (DbFunctions.TruncateTime(td.Created_Date) <= todaydate && DbFunctions.TruncateTime(td.Created_Date) >= todaydate) select td.Debit_Amount).Sum();
                    var totalcredit = (from td in db.User_Wallet where td.Created_By == id && (DbFunctions.TruncateTime(td.Created_Date) <= todaydate && DbFunctions.TruncateTime(td.Created_Date) >= todaydate) select td.Credit_Amount).Sum();
                    var totalfund = (from td in db.User_Wallet where td.Created_By == id && (DbFunctions.TruncateTime(td.Created_Date) <= todaydate && DbFunctions.TruncateTime(td.Created_Date) >= todaydate) select td.Fund_Amount).Sum();
                    var totalcommision = (from td in db.Transaction_Table where td.Created_By == id && (DbFunctions.TruncateTime(td.Created_Date) <= todaydate && DbFunctions.TruncateTime(td.Created_Date) >= todaydate) select td.Total_Commission).Sum();
                    var totalrecharge = (from td in db.Transaction_Table where td.Created_By == id && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Request_Amount).Sum();
                    var totalcreditamount = (from td in db.User_Amount where td.Fk_User_ID == id select td.Total_Credit).FirstOrDefault();
                    var totaldebitamount = (from td in db.User_Amount where td.Fk_User_ID == id select td.Total_Debit).FirstOrDefault();
                 
                    TransactionStatus objstts = new TransactionStatus();
                    List<TransactionStatus> objlst = new List<TransactionStatus>();
                    objstts.SuccessTxn = CountSuccecss == null ? 0 : CountSuccecss;
                    objstts.FailTxn = CountFailure == null ? 0 : CountFailure;
                    objstts.PendingTxn = CountPending == null ? 0 : CountPending;
                    objstts.SuccessAmount = CountSuccecssamount == null ? 0 : CountSuccecssamount;
                    objstts.PendingAmount = CountPendingamount == null ? 0 : CountPendingamount;
                    objstts.FailureAmount = CountFailureamount == null ? 0 : CountFailureamount;
                    objstts.TotalTxn = counttxn == null ? 0 : counttxn;
                    objstts.TotalBalance = counttxnamount == null ? 0 : counttxnamount;
                    objstts.balance = totalamount == null ? 0 : totalamount;
                    objstts.debitamount = totaldebit == null ? 0 : totaldebit;
                    objstts.creditamount = totalcredit == null ? 0 : totalcredit;
                    objstts.fundamount = totalfund == null ? 0 : totalfund;
                    objstts.totalcommision = totalcommision == null ? 0 : totalcommision;
                    objstts.Rechargeamount = totalrecharge == null ? 0 : totalrecharge;
                    objstts.totalcreditamount = totalcreditamount == null ? 0 : totalcreditamount;
                    objstts.totaldebitamount = totaldebitamount == null ? 0 : totaldebitamount;
                    objstts.openingbalance = countopeningbalance == null ? 0 : countopeningbalance;
                    objstts.closingbalance = countclosingbalance == null ? 0 : countclosingbalance;
                   

                    Rechargeamount = totalrecharge == null ? 0 : totalrecharge;
                    totalcommision = totalcommision == null ? 0 : totalcommision;
                    debitamount = totaldebit == null ? 0 : totaldebit;
                    creditamount = totalcredit == null ? 0 : totalcredit;
                    fundamount = fundamount == null ? 0 : fundamount;
                    CountSuccecssamount = CountSuccecssamount == null ? 0 : CountSuccecssamount;
                    CountPendingamount = CountPendingamount == null ? 0 : CountPendingamount;
                    CountFailureamount = CountFailureamount == null ? 0 : CountFailureamount;
                    counttxnamount = counttxnamount == null ? 0 : counttxnamount;
                    totalamount = totalamount == null ? 0 : totalamount;
                    balance = totalamount == null ? 0 : totalamount;
                    totalcreditamount = totalcreditamount == null ? 0 : totalcreditamount;
                    totaldebitamount = totaldebitamount == null ? 0 : totaldebitamount;
                    openingbalance = countopeningbalance == null ? 0 : countopeningbalance;
                    closingbalance = countclosingbalance == null ? 0 : countclosingbalance;

                    objlst.Add(objstts);
                    return (objlst);
                }


            }
            catch (System.Exception ex)
            {
                throw ex;

            }
        }

        public static List<TransactionStatus> CountTransactionList(long id, string date)
        {

            try
            {

               // DateTime todaydate = Convert.ToDateTime(date); 
                DateTime todaydate = new DateTime();
                //DateTime result;
                //if (!DateTime.TryParse(date, out result))
                //{
                //    result = DateTime.ParseExact(date, "yyyy-MM-ddT24:mm:ssK", System.Globalization.CultureInfo.InvariantCulture);
                //    result = result.AddDays(1);
                //}
               
                todaydate = Convert.ToDateTime(DateTime.ParseExact(date.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture));

                using (var db = new PaymezonDBEntities1())
                {
                    db.Configuration.ProxyCreationEnabled = false;
                    var CountSuccecss = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "SUCCESS" && (DbFunctions.TruncateTime(td.Transaction_Date_Time)  <=todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Status).Count();
                    var CountFailure = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "FAILED" && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Status).Count();
                    var CountPending = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "PENDING" && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Status).Count();
                    var CountSuccecssamount = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "SUCCESS" && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Net_Amount).Sum();
                    var CountFailureamount = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "FAILED" && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Net_Amount).Sum();
                    var CountPendingamount = (from td in db.Transaction_Table where td.Created_By == id && td.Status.ToUpper() == "PENDING" && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Net_Amount).Sum();
                    var counttxn = (from td in db.Transaction_Table where td.Created_By == id && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Status).Count();
                    var counttxnamount = (from td in db.Transaction_Table where td.Created_By == id && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Net_Amount).Sum();
                    var countopeningbalance = (from td in db.Transaction_Table where td.Created_By == id && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) orderby td.Created_Date ascending  select td.Amount_Before_Due_Date).FirstOrDefault();
                    var countclosingbalance = (from td in db.Transaction_Table where td.Created_By == id && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) orderby td.Created_Date descending select td.Amount_After_Deu_Date).FirstOrDefault();
                    var totalamount = (from td in db.User_Amount where td.Fk_User_ID == id select td.Actual_Amount).FirstOrDefault();
                    var totaldebit = (from td in db.User_Wallet where td.Created_By == id && (DbFunctions.TruncateTime(td.Created_Date) <= todaydate && DbFunctions.TruncateTime(td.Created_Date) >= todaydate) select td.Debit_Amount).Sum();
                    var totalcredit = (from td in db.User_Wallet where td.Created_By == id && (DbFunctions.TruncateTime(td.Created_Date) <= todaydate && DbFunctions.TruncateTime(td.Created_Date) >= todaydate) select td.Credit_Amount).Sum();
                    var totalfund = (from td in db.User_Wallet where td.Created_By == id && (DbFunctions.TruncateTime(td.Created_Date) <= todaydate && DbFunctions.TruncateTime(td.Created_Date) >= todaydate) select td.Fund_Amount).Sum();
                    var totalcommision = (from td in db.Transaction_Table where td.Created_By == id && (DbFunctions.TruncateTime(td.Created_Date) <= todaydate && DbFunctions.TruncateTime(td.Created_Date) >= todaydate) select td.Total_Commission).Sum();
                    var totalrecharge = (from td in db.Transaction_Table where td.Created_By == id && (DbFunctions.TruncateTime(td.Transaction_Date_Time) <= todaydate && DbFunctions.TruncateTime(td.Transaction_Date_Time) >= todaydate) select td.Request_Amount).Sum();
                    var totalcreditamount = (from td in db.User_Amount where td.Fk_User_ID == id select td.Total_Credit).FirstOrDefault();
                    var totaldebitamount = (from td in db.User_Amount where td.Fk_User_ID == id select td.Total_Debit).FirstOrDefault();
                    //  var Balance = (from td in db.User_Amount where td.Fk_User_ID == id  select td.Actual_Amount).Sum();
                    TransactionStatus objstts = new TransactionStatus();
                    List<TransactionStatus> objlst = new List<TransactionStatus>();
                    objstts.SuccessTxn = CountSuccecss == null ? 0 : CountSuccecss;
                    objstts.FailTxn = CountFailure == null ? 0 : CountFailure;
                    objstts.PendingTxn = CountPending == null ? 0 : CountPending;
                    objstts.SuccessAmount = CountSuccecssamount == null ? 0 : CountSuccecssamount;
                    objstts.PendingAmount = CountPendingamount == null ? 0 : CountPendingamount;
                    objstts.FailureAmount = CountFailureamount == null ? 0 : CountFailureamount;
                    objstts.TotalTxn = counttxn == null ? 0 : counttxn;
                    objstts.TotalBalance = counttxnamount == null ? 0 : counttxnamount;
                    objstts.balance = totalamount == null ? 0 : totalamount;
                    objstts.debitamount = totaldebit == null ? 0 : totaldebit;
                    objstts.creditamount = totalcredit == null ? 0 : totalcredit;
                    objstts.fundamount = totalfund == null ? 0 : totalfund;
                    objstts.totalcommision = totalcommision == null ? 0 : totalcommision;
                    objstts.Rechargeamount = totalrecharge == null ? 0 : totalrecharge;
                    objstts.totalcreditamount = totalcreditamount == null ? 0 : totalcreditamount;
                    objstts.totaldebitamount = totaldebitamount == null ? 0 : totaldebitamount;
                    objstts.openingbalance = countopeningbalance == null ? 0 : countopeningbalance;
                    objstts.closingbalance = countclosingbalance == null ? 0 : countclosingbalance;
                    //  objstts.balance = Balance == null? 0: Balance;

                    Rechargeamount = totalrecharge == null ? 0 : totalrecharge;
                    totalcommision = totalcommision == null ? 0 : totalcommision;
                    debitamount = totaldebit == null ? 0 : totaldebit;
                    creditamount = totalcredit == null ? 0 : totalcredit;
                    fundamount = fundamount == null ? 0 : fundamount;
                    CountSuccecssamount = CountSuccecssamount == null ? 0 : CountSuccecssamount;
                    CountPendingamount = CountPendingamount == null ? 0 : CountPendingamount;
                    CountFailureamount = CountFailureamount == null ? 0 : CountFailureamount;
                    counttxnamount = counttxnamount == null ? 0 : counttxnamount;
                    totalamount = totalamount == null ? 0 : totalamount;
                    balance = totalamount == null ? 0 : totalamount;
                    totalcreditamount = totalcreditamount == null ? 0 : totalcreditamount;
                    totaldebitamount = totaldebitamount == null ? 0 : totaldebitamount;
                    openingbalance = countopeningbalance == null ? 0 : countopeningbalance;
                    closingbalance = countclosingbalance == null ? 0 : countclosingbalance;

                    objlst.Add(objstts);
                    return (objlst);
                }


            }
            catch (System.Exception ex)
            {
                throw ex;

            }
        }

        public static List<Recharge_Detail> counttransactionbyserviceprovider(long id)
        {
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    List<Recharge_Detail> lstrechrge = new List<Recharge_Detail>();
                    DateTime todaydate = System.DateTime.Now.Date; // Convert.ToDateTime("2017-02-15"); 
                    lstrechrge = db.Recharge_Detail.AsEnumerable().Where(c=> c.Recharge_Date >= todaydate && c.Fk_RegisterId == id).GroupBy(p => p.Operator_Code.ToUpper() ).Select(g => new Recharge_Detail { Operator_Code = g.Key, Status = g.Count().ToString() }).ToList();

                  return lstrechrge;
               
                }


            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public static List<Recharge_Detail> counttransactionbyserviceproviderforadmin()
        {
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    List<Recharge_Detail> lstrechrge = new List<Recharge_Detail>();
                    DateTime todaydate = System.DateTime.Now.Date; // Convert.ToDateTime("2017-02-15"); 
                    lstrechrge = db.Recharge_Detail.AsEnumerable().Where(c => c.Recharge_Date >= todaydate).GroupBy(p => p.Operator_Code.ToUpper()).Select(g => new Recharge_Detail 
                    { 
                        Operator_Code = g.Key,
                        Status = g.Count().ToString(),
                        sumope = g.Sum(d=> d.Amount).ToString()

                    }).ToList();

                    return lstrechrge;

                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static List<Recharge_Detail> counttransactionbyapiforadmin()
        {
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    List<Recharge_Detail> lstrechrge = new List<Recharge_Detail>();
                    DateTime todaydate =  System.DateTime.Now.Date; // Convert.ToDateTime("2017-02-15"); 
                    lstrechrge = db.Recharge_Detail.AsEnumerable().Where(c => c.Recharge_Date >= todaydate).GroupBy(p => p.API_Name.ToUpper()).Select(g => new Recharge_Detail { 
                        API_Name = g.Key,
                        succcount = g.Where(j => j.Status.ToUpper() == "SUCCESS").Count().ToString(),
                        pencount = g.Where(j => j.Status.ToUpper() == "PENDING").Count().ToString(),
                        Sumapisucc = g.Where(j => j.Status.ToUpper() == "SUCCESS").Sum(d => d.Amount).ToString(),
                        failcount = g.Where(j => j.Status.ToUpper() == "FAILED").Count().ToString()
                        
                    }).ToList();

                    return lstrechrge;

                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        
        public static string GetMacAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        public static Api_Detail GetAPiURL(string apiName)
        {
            Api_Detail apiDetail = new Api_Detail();
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    apiDetail = db.Api_Detail.FirstOrDefault(c => c.Api_Name == apiName);
                }
            }
            catch (Exception ex)
            {
                return apiDetail;
            }
            return apiDetail;
        }

        public static string GetOperatorCode(string oprator, string apiName)
        {
            string op = "";
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    op = db.VendorDiversions.FirstOrDefault(c => c.ServiceProvider == apiName && c.Operator.ToLower() == oprator.ToLower()).OperatorCode;
                }
            }
            catch (Exception ex)
            {
                op = "Your Operator Not Match..!";
            }
            return op;
        }
        public static string GetRechargeType(string oprator, string apiName)
        {
            string RT = "";
            try
            {
                using (var db = new PaymezonDBEntities1())
                {
                    RT = db.VendorDiversions.FirstOrDefault(c => c.ServiceProvider == apiName && c.Operator.ToLower() == oprator.ToLower()).RechargeType;
                }
            }
            catch (Exception ex)
            {
                RT = ex.Message;
            }
            return RT;
        }

        public static void SendMailmsg(string body, string msgto, string msgcc, string msgfrom, string sub)
        {
            try
            {
                using (MailMessage mm = new MailMessage("paymezon.manish@gmail.com", msgto))
                {
                    if (!string.IsNullOrEmpty(msgcc))
                    {
                        mm.CC.Add(msgcc);
                    }
                    mm.Subject = sub;
                    mm.Body = body;
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("paymezon.manish@gmail.com", "stephen@2016");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    try
                    {
                        smtp.Send(mm);
                    }
                    catch (System.Threading.ThreadAbortException ec)
                    {
                        ec.ToString();

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


      

    }
}