﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Web.Script.Serialization;
using PaymezonDAL;
using System.Net.NetworkInformation;
using PaymezonBAL;
using PaymezonApp.Models;


namespace PaymezonApp
{
    public partial class otomax : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string resurl = "";
            string status = "";
            string apiopid = "";

            try
            {
                if (Request.QueryString["refid"] != null )
                {

                    string refid, message = "";
                    refid = Request.QueryString["refid"];
                    message = Request.QueryString["message"];
                    resurl = "@refid:" + refid + "@message:" + message;

                    using (var db = new PaymezonDBEntities1())
                    {
                        Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                        input.Service_name = "AnshApi";
                        input.REFERENCE_NUMBER = refid;
                        input.COURENTLEVEL = null;
                        input.Input_Request = "789";
                        input.Output_Responce = resurl;
                        input.ReqDate = System.DateTime.Now;
                        input.ResDate = System.DateTime.Now;
                        db.Vendor_InptOutPut_ReqDetails.Add(input);
                        db.SaveChanges();

                    }

                    string[] msg = message.Split('#');
                    string numericPhone = new String(message.ToString().ToCharArray().Where(c => Char.IsDigit(c)).Take(10).ToArray());
                    string txnid = refid;


                    if (message.Contains("Done"))
                    {
                        string[] operatorid = message.Split(':');
                        string[] apiid = operatorid[1].Split(' ');
                        status = "SUCCESS";
                        apiopid = apiid[0].ToString();
                        string updateapiid = TransactionBAL.updateapiidbytxnid(refid, apiopid);
                    }
                    else if (message.Contains("RESPWAIT"))
                    {
                        status = "PENDING";
                    }
                    else
                    {
                        status = "FAILED";
                        apiopid = "NA";
                    }

                    //  string str = TransactionBAL.UpdateStatusOfAnsh(status,numericPhone);

                    TransactionBAL.UpdateStatus(status, txnid, "");

                    // string txnid = TransactionBAL.gettxnbymobno(numericPhone);
                    string requsturl = TransactionBAL.Getrequrl(txnid);
                    string clienttxid = TransactionBAL.Getctxid(txnid);

                    string retUrl = requsturl + "?ctxnid=" + clienttxid + "&status=" + status + "&apitransactionID=" + apiopid;

                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
                    string dataString = "";
                    string respons;
                    request.Method = "GET";
                    request.Timeout = 180000;
                    request.ReadWriteTimeout = 180000;
                    request.KeepAlive = false;
                    request.Proxy = null;
                    request.ServicePoint.ConnectionLeaseTimeout = 60000;
                    request.ServicePoint.MaxIdleTime = 60000;
                    HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
                    Stream s = (Stream)responsee.GetResponseStream();
                    StreamReader readStream = new StreamReader(s);
                    dataString = readStream.ReadToEnd();
                    respons = dataString;
                    responsee.Close();
                    s.Close();
                    readStream.Close();
                }

              


            }
            catch (Exception ex)
            {
                using (var db = new PaymezonDBEntities1())
                {
                    Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                    input.Service_name = "AnshApi";
                    input.REFERENCE_NUMBER = "";
                    input.COURENTLEVEL = null;
                    input.Input_Request = "789";
                    input.Output_Responce = ex.Message.ToString();
                    input.ReqDate = System.DateTime.Now;
                    input.ResDate = System.DateTime.Now;
                    db.Vendor_InptOutPut_ReqDetails.Add(input);
                    db.SaveChanges();

                }
                //throw ex;
            }


            //Response.Write("Key: " + key + " Value: " + Request.QueryString[key]);
            // DataSet ds = new DataSet();
            // string otoresp = new StreamReader(Context.Request.InputStream).ReadToEnd();
            //// string otoresp = "<?xml version='1.0'?><responsetopup><message>IDEA Of Rs.17 Done On 9904260470. ID:UE2120718220379 Today Sale.26.055 Your Balance is 473.945 R##R-Ansh Recharge(go.anshrecharge.com)</message></responsetopup>";
            //   string otores = "IDEA Recharge of 10 Failed MOb 9925457772 Number or Code Or Amount Is Wrong.. Bal. 259.505 26/01 16:53 R##R-Ansh Recharge(go.anshrecharge.com)";

            //ds = CommonApi.ConvertXMLToDataSet(otoresp);

            //string status = ds.Tables[0].Rows[0]["message"].ToString().Contains("Done") ? "Success" : "Fail";
            //string response = ds.Tables[0].Rows[0]["message"].ToString().Split(new[] { "On" }, StringSplitOptions.None)[1];


            // if (otoresp != "")
            // {
            //     using (var db = new PaymezonDBEntities1())
            //     {
            //         Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
            //         input.Service_name = "AnshApi";
            //         input.REFERENCE_NUMBER = numericPhone;
            //         input.COURENTLEVEL = null;
            //         input.Input_Request = "789";
            //         input.Output_Responce = otoresp;
            //         input.ReqDate = System.DateTime.Now;
            //         input.ResDate = System.DateTime.Now;
            //         db.Vendor_InptOutPut_ReqDetails.Add(input);
            //         db.SaveChanges();

            //     }
            // }







        }
    }
}