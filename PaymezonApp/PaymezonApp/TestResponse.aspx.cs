﻿using PaymezonBAL;
using PaymezonDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PaymezonApp
{
    public partial class TestResponse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string TxnID , status;
            TxnID = Request.QueryString["ctxnid"].ToString();
            status = Request.QueryString["status"].ToString();
           
            if (status != "")
            {
                string url = TxnID + "@" + status;
                using (var db = new PaymezonDBEntities1())
                {
                    LogDetail input = new LogDetail();
                    input.Log_Detail = url;
                    input.Create_Date = System.DateTime.Now;
                    input.Page_Name = TxnID;
                    db.LogDetails.Add(input);
                    db.SaveChanges();
                }
            }
            Response.Write("succ");
          //  string str = TransactionBAL.UpdateStatus(status, TxnID, "2");
        }
        
    }
}