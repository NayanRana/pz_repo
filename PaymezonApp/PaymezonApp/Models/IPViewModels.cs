﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaymezonDAL;

namespace PaymezonApp.Models
{
    public class IPViewModels
    {
        public long Pk_Ip_Id { get; set; }
        public Nullable<long> fk_user_Id { get; set; }
        public string Ip_One { get; set; }
        public string Ip_Two { get; set; }
        public string Ip_Three { get; set; }
        public string Ip_Four { get; set; }
        public string Ip_Five { get; set; }
        public Nullable<long> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modify_By { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public Ip_Address_Detail dalIP { get; set; }
        public List<Ip_Address_Detail> List_IP_By_Id { get; set; }
    }
}