﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using PaymezonDAL;

namespace PaymezonApp.Models
{
    public class FundRequestViewModels
    {
        public long Pk_Fund_Request_Id { get; set; }
        public Nullable<System.DateTime> Request_date_time { get; set; }
        public string User_Type { get; set; }
        public Nullable<long> Fk_User_Id { get; set; }
        public string User_Id { get; set; }
        public string Name { get; set; }
        public string Pay_Made { get; set; }
        public string Bank_Name { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string Transaction_Id { get; set; }
        public string Status { get; set; }
        public string  Decline_Reason { get; set; }

       
        public string Bank_Txn_Id { get; set; }
        public string Request_Status { get; set; }
        public Nullable<long> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }

        public List<Fund_Request> LstFund_Req { get; set; }

        public UserRegistration dalfund { get; set; }
        [Required(ErrorMessage = "Please Select From Date")]
        public string fromdate { get; set; }

        [Required(ErrorMessage = "Please Select To Date")]
        public string todate { get; set; }
        public string message { get; set; }


    }
}