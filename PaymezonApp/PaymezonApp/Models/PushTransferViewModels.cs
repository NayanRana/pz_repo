﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaymezonDAL;
using System.ComponentModel;

namespace PaymezonApp.Models
{
    public class PushTransferViewModels    
    {
        [DisplayName("White Label")]
        public long White_Label { get; set; }
        public long Distributor { get; set; }
        public long Agent { get; set; }
        public long User_Id { get; set; }
        public string Name { get; set; }
        public decimal Balance { get; set; }
        public string Select_Type { get; set; }
        public decimal Amount { get; set; }
        public string Remark { get; set; }

        public List<UserRegistration> lstWL { get; set; }
        public List<UserRegistration> lstDis { get; set; }
        public List<UserRegistration> lstAgnt { get; set; }

    }
}