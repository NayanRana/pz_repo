﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaymezonDAL;
using System.ComponentModel.DataAnnotations;


namespace PaymezonApp.Models
{
    public class AssignApiViewModels
    {
        public long Pk_Assign_Api_Id { get; set; }

       [Required(ErrorMessage = "Please Select operator")]
        public Nullable<int> Operator_Id { get; set; }
        [Required(ErrorMessage = "Please Enter graterThan Amount")]
        public Nullable<decimal> Grater_than_Amount { get; set; }

         [Required(ErrorMessage = "Please select API by Amount")]
        public Nullable<long> Api_Id_by_Amount { get; set; }
        [Required(ErrorMessage = "Please select Normal API")]
        public Nullable<long> Normal_Api_Id { get; set; }

        [Required(ErrorMessage = "Please select Emergency API")]
        public Nullable<long> Emergency_Api_Id { get; set; }
        public Nullable<long> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public string message { get; set; }
        public Assign_Api dalassignapi { get; set; }
        public List<Assign_Api> list_assign_api { get; set; }

        public List<VendorDiversion> listvendor { get; set; }

        public List<Company_Detail> listcompany { get; set; }

        public List<Api_Detail> listApi { get; set; }
    }
}