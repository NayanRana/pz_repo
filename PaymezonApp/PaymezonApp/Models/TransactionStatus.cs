﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaymezonDAL;
using PaymezonBAL;
using System.ComponentModel.DataAnnotations;

namespace PaymezonApp.Models
{
    public class TransactionStatus
    {
        public long userid { get; set; }
        public decimal? TotalBalance { get; set; }
        public decimal? PendingAmount { get; set; }
        public decimal? SuccessAmount { get; set; }
        public decimal? FailureAmount { get; set; }

        public decimal? debitamount { get; set; }
        public decimal? Rechargeamount { get; set; }
        public decimal? creditamount { get; set; }
        public decimal? totalcommision { get; set;}
        public decimal? fundamount { get; set; }

        public decimal? balance { get; set; }
        public int TotalTxn { get; set; }
        public int SuccessTxn { get; set; }
        public int FailTxn { get; set; }
        public int PendingTxn { get; set; }

        public string cntPendingbyapi { get; set; }

        public string cntSuccssbyapi { get; set; }

        public string cntFailbyapi { get; set; }

        public decimal? totalcreditamount { get; set; }
        public decimal? totaldebitamount { get; set; }
        public decimal? openingbalance { get; set; }
        public decimal? closingbalance { get; set; }
        public TransactionStatus transtts { get; set; }

        public List<TransactionStatus> listtransaction { get; set; }
        public List<UserRegistration> userregistrationlist { get; set; }

        public DateTime? fdate { get; set; }

        [Required(ErrorMessage = "Please Select To Date")]
        public string dailydate { get; set; }

        public string message { get; set; }
    }
}