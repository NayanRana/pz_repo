﻿using System.ComponentModel.DataAnnotations;
using PaymezonDAL;
using System.Collections.Generic;
using System;


namespace PaymezonApp.Models
{
    public class AdminViewModels
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public SupportViewModels supp { get; set; }


    }

   

}