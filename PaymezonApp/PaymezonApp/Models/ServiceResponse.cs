﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaymezonApp.Models
{
    public class ServiceResponse<T>
    {
        public Response<T> Response { get; set; }

        public ServiceResponse(T response, bool succsess)
        {
            Response = new Response<T>(response, succsess);
        }
    }
}