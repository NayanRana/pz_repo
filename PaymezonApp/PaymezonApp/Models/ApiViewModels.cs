﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaymezonDAL;

namespace PaymezonApp.Models
{
    public class ApiViewModels
    {

        public long Api_Id { get; set; }
        public string Api_Name { get; set; }
        public string Login_Id { get; set; }
        public string User_Id { get; set; }
        public string User_Name { get; set; }
        public string Password { get; set; }
        public string Url { get; set; }
        public string Response_Type { get; set; }
        public string Amount { get; set; }
        public string Operator_code { get; set; }
        public string Number { get; set; }
        public string Client_Id { get; set; }
        public string Circle_Code { get; set; }
        public string Pin { get; set; }
        public string Account { get; set; }
        public string User_Transaction { get; set; }
        public string Transaction_Id { get; set; }
        public string Format { get; set; }
        public string Version { get; set; }
        public string Token { get; set; }
        public string Provider { get; set; }
        public string Agent_Id { get; set; }
        public string Company { get; set; }
        public string Order_Id { get; set; }
        public string Std_Code { get; set; }
        public string Type { get; set; }
        public string Optional_1 { get; set; }
        public string Optional_2 { get; set; }
        public string Other_Value { get; set; }
        public string Request_Url { get; set; }
        public string Response_Url { get; set; }
        public List<Api_Detail> Api_List_Detail { get; set; }

        public Api_Detail dalApi { get; set; }

        public string StatusCode { get; set; }
        public string SuccessCode { get; set; }
        public string FailuerCode { get; set; }
        public string PendingCode { get; set; }
        public string UniqueTransectionid { get; set; }
        public string Transectionnumber { get; set; }
        public string Message { get; set; }
    }
}