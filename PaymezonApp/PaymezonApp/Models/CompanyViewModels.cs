﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaymezonDAL;

namespace PaymezonApp.Models
{
    public class CompanyViewModels
    {
        public int Pk_Company_Id { get; set; }
        public string Company_Name { get; set; }
        public string Company_Code { get; set; }
        public string Company_Detail1 { get; set; }
        public Nullable<bool> Is_deleted { get; set; }
        public List<Company_Detail> List_Company { get; set; }
        public Company_Detail dalcompany { get; set; }
        public List<Company_Detail> listcmpny { get; set; }
    }
}