﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaymezonApp.Models
{
    public class Vendor_InptOutPut_ReqDetailsViewModels
    {
        public long S_No { get; set; }
        public Nullable<long> COURENTLEVEL { get; set; }
        public string REFERENCE_NUMBER { get; set; }
        public Nullable<System.DateTime> ReqDate { get; set; }
        public string Input_Request { get; set; }
        public string Output_Responce { get; set; }
        public Nullable<System.DateTime> ResDate { get; set; }
        public string Service_name { get; set; }
    }
}