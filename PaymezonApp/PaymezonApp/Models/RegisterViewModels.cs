﻿using PaymezonDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaymezonApp.Models
{
    public class RegisterViewModels
    {
        public long Pk_Register_ID { get; set; }
        public long MasterDistributor { get; set; }
        public long Distributor { get; set; }
        public string Company_Name { get; set; }
        public string Logo { get; set; }
        public Nullable<decimal> Mobile_Number { get; set; }
        public string Helpdesk_Email { get; set; }
        public string Domain_Name { get; set; }
        public string Copyright { get; set; }
        public string Panel_Title { get; set; }
        public string Author_Name { get; set; }
        public string Portal_FName { get; set; }
        public string Portal_MName { get; set; }
        public string Portal_LName { get; set; }
        public string Author_Email { get; set; }
        public Nullable<decimal> Author_MobNo { get; set; }
        public string PAN_Num { get; set; }
        public string Office_Address { get; set; }
        public string Fk_Country_Id { get; set; }
        public string Fk_State_Id { get; set; }
        public string City { get; set; }
        public string Pincode { get; set; }

        public Nullable<System.DateTime> Reg_Date { get; set; }
        public string Password { get; set; }

        public string newpassword { get; set; }

        public string retypepassword { get; set; }

        public string Label_Id { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Created_By { get; set; }
        public Nullable<long> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public string User_Type { get; set; }
        public string reg_Type { get; set; }
        public string Return_URL { get; set; }
        public List<UserRegistration> lstWL { get; set; }
        public List<UserRegistration> lstMDis { get; set; }
        public List<UserRegistration> lstDis { get; set; }
        public List<UserRegistration> lstAgnt { get; set; }
        public long CommissionId { get; set; }
       public   List<commision_Detail> lstComm { get; set; }

       public UserRegistration dalusrreg { get; set; }

     
    }
}