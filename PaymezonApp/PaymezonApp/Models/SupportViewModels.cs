﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using PaymezonDAL;


namespace PaymezonApp.Models
{
    public class SupportViewModels
    {
        public int Pk_Support_Id { get; set; }
        public string Token_no { get; set; }
        public string User_type { get; set; }
        public string Message { get; set; }
        public string Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Answer_By { get; set; }
        public Nullable<System.DateTime> Answer_Date { get; set; }
        public string Content_Detail { get; set; }
        public string Is_Deleted { get; set; }

        public string Query_Type { get; set; }
        public string Mobile_Number { get; set; }
        public string Amount { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public int Company { get; set; }
        public List<Support_Detail> List_SupportDetail { get; set; }
        public List<Support_Detail> SupportDetail_byTokenID { get; set; }
        public string Company_name { get; set; }

        public string username { get; set; }
        public List<Company_Detail> List_Company { get; set; }
        [Required(ErrorMessage = "Please Select From Date")]
        public string fromdate { get; set; }
       
        public string todate { get; set; }
        public string message { get; set; }
    }
}