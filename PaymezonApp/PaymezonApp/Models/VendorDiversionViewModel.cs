﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaymezonDAL;

namespace PaymezonApp.Models
{
    public class VendorDiversionViewModel
    {
        public int OperatorID { get; set; }
        public string Operator { get; set; }
        public string ServiceProvider { get; set; }
        public string ServiceType { get; set; }
        public string AvailabilityIn { get; set; }
        public Nullable<bool> Status { get; set; }
        public string Remark { get; set; }
        public string ServiceProviderSms { get; set; }
        public string OperatorCode { get; set; }
        public string RechargeType { get; set; }
        public string ServiceCode { get; set; }

        public List<Api_Detail> listApi { get; set; }

        public List<Company_Detail> listcompany { get; set; }
        public List<VendorDiversion> listvendor { get; set; }
    }
}