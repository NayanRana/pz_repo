﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using PaymezonDAL;

namespace PaymezonApp.Models
{
    public class RechargeViewModels
    {
        public int Pk_Recharge_Id { get; set; }
        public string Operator_Code { get; set; }
        public decimal Mobile_no { get; set; }
        public decimal Amount { get; set; }
        public string Transaction_Id { get; set; }
        public string Type { get; set; }
        public string Agent_Id { get; set; }
        public Nullable<long> Fk_RegisterId { get; set; }
        public string Circle_Code { get; set; }
        public string Action_On_Amount { get; set; }
        public Nullable<System.DateTime> Recharge_Date { get; set; }
        public Nullable<System.DateTime> Response_Date { get; set; }
        public string Transaction_Log { get; set; }
        public string Status { get; set; }
        public string Ref_number { get; set; }
        public string User_Type { get; set; }
        public Nullable<long> Recharge_By { get; set; }
        public List<Company_Detail> List_Company { get; set; }
        public List<Recharge_Detail> RCG_List { get; set; }
        public List<Recharge_Detail> countrech_List { get; set; }
        public List<Transaction_Table> list_transaction { get; set; }
        public TransactionStatus transaction { get; set; }
        [Required(ErrorMessage = "Please Select From Date")]
        public string fromdate { get; set; }
        [Required(ErrorMessage = "Please Select To Date")]
        public string todate { get; set; }
        public string message { get; set; }
    }
}