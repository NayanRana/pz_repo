﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaymezonDAL;
using PaymezonBAL;
using System.ComponentModel.DataAnnotations;

namespace PaymezonApp.Models
{
    public class TransactionModel
    {
        public long Transcatio_Id { get; set; }
        public string Reference_Number { get; set; }
        public string User_Type { get; set; }
        public Nullable<long> LowerLevel { get; set; }
        public Nullable<long> UpperLevel { get; set; }
        public Nullable<System.DateTime> Transaction_Date_Time { get; set; }
        public string Service_Name { get; set; }
        public Nullable<decimal> Amount_Before_Due_Date { get; set; }
        public Nullable<decimal> Request_Amount { get; set; }
        public Nullable<decimal> Total_Service_Charge { get; set; }
        public Nullable<decimal> Total_Commission { get; set; }
        public Nullable<decimal> Net_Amount { get; set; }
        public Nullable<decimal> Amount_After_Deu_Date { get; set; }
        public string Action_On_Amount { get; set; }
        public string Status { get; set; }
        public Nullable<decimal> Final_Bal_Amount { get; set; }
        public Nullable<System.DateTime> Update_Date { get; set; }
        public string Update_User { get; set; }
        public string Update_Remark { get; set; }
        public string Service_Vendor { get; set; }
        public string PortalName { get; set; }
        public string Request_IP_Address { get; set; }
        public string Update_IP_Address { get; set; }
        public Nullable<int> Flag { get; set; }
        public Nullable<long> MobileNo { get; set; }
        public string Ref_No { get; set; }
        public Nullable<long> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public string Api_transaction_Id { get; set; }

        public List<Transaction_Table> transanctionlist { get; set; }

        public List<UserRegistration> userregistrationlist { get; set; }
        [Required(ErrorMessage = "Please Select From Date")]
        public string fromdate { get; set; }

        [Required(ErrorMessage = "Please Select To Date")]
        public string todate { get; set; }
         public string message { get; set; }

        public List<Vendor_InptOutPut_ReqDetails> listvendr { get; set; }

        public List<Vendor_InptOutPut_ReqDetailsViewModels> listvendrreqmdl { get; set; }
        
    }
}