﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaymezonDAL;
namespace PaymezonApp.Models
{
    public class CommisionViewModels
    {
        public long Pk_Comm_Id { get; set; }
        public string Plan_Name { get; set; }
        public string Operator_Name { get; set; }
        public string Operator_Code { get; set; }
        public string Commision_Per { get; set; }
        public Nullable<long> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Modify_By { get; set; }
        public Nullable<System.DateTime> Modify_Date { get; set; }

        public string Company_name { get; set; }

        public Nullable<int> Plan_Id { get; set; }
        public List<commision_Detail> List_Commision { get; set; }

        public List<Company_Detail> List_Company { get; set; }

        public List<commision_Detail> lstComm { get; set; }
    }
}