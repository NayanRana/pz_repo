﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Web;

namespace PaymezonApp.Models
{
    public enum ErrorCode
    {
        #region Generic
        /// <summary>
        /// Generic for generic
        /// </summary>
       
        ERROR100 = 100,

        /// <summary>
        /// Exception
        /// </summary>
    
        ERROR101 = 101,

        /// <summary>
        /// No data available.
        /// </summary>
     
        ERROR102 = 102,

        /// <summary>
        /// Invalid model.
        /// </summary>
       
        ERROR103 = 103,

        /// <summary>
        /// Invalid city id.
        /// </summary>
       
        ERROR104 = 104,

        /// <summary>
        /// Invalid restaurant id.
        /// </summary>
      
        ERROR105 = 105,

        /// <summary>
        /// Address not found.
        /// </summary>
       
        ERROR106 = 106,

        #endregion
    }
    public static class ErrorCodeClass
    {
        /// <summary>
        /// Thwo in implemented exception
        /// </summary>
        /// <param name="errorMessage">Error message</param>
        /// <returns>Exception string</returns>
        public static Exception MyException(ErrorCode code, string errorMessage = "")
        {
            return new NotImplementedException(errorMessage) { HelpLink = code.ToString() };
        }
    }
}