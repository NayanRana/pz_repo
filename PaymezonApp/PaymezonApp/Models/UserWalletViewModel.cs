﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using PaymezonDAL;

namespace PaymezonApp.Models
{
    public class UserWalletViewModel
    {
        public long Pk_User_ID { get; set; }
        public Nullable<long> Fk_User_Id { get; set; }
        public string User_Name { get; set; }
        public string Label_Id { get; set; }
        public Nullable<decimal> Fund_Amount { get; set; }
        public Nullable<decimal> Debit_Amount { get; set; }
        public Nullable<decimal> Credit_Amount { get; set; }
        public string Description { get; set; }
        public Nullable<long> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }

        public List<User_Wallet> List_UserWallet_BYId { get; set; }
        public List<User_Wallet> List_UserWallet { get; set; }
        public virtual UserRegistration UserRegistration { get; set; }

        public Nullable<decimal> before_balance { get; set; }
        public Nullable<decimal> after_balance { get; set; }
        [Required(ErrorMessage = "Please Select From Date")]
        public string fromdate { get; set; }
        [Required(ErrorMessage = "Please Select To Date")]
        public string todate { get; set; }

        public string message { get; set; }
    }
}