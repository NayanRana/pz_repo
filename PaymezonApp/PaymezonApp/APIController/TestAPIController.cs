﻿using PaymezonBAL;
using PaymezonDAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PaymezonApp.APIController
{
    public class TestAPIController : BaseApiController
    {
        [HttpGet]
        public object AnshApi(string mobile, string amt, string ctxnid, string oprator, string cid)
        {
            string dataString = "";
            string transactionId = "TN" + DateTime.Now.Year + DateTime.Now.Ticks.ToString("D" + 3);
            ///string str = "http://103.231.32.126:85/?mobile=" + mobile + "&amt=" + amt + "&ctxnid=" + transactionId + "&operator=" + oprator + "&type=Recharge&rt=4&cid=" + cid + "";
            //string str = "http://128.199.78.41/ansh.php?act=recharge&custid=8154014405&pin=4405&sign=man1234&product=" + oprator + "&qty=" + amt + "&counter=1&msisdn=" + mobile + "&trxid=" + transactionId + "&date=" + System.DateTime.Now.ToString() + "&waittrx=0&replyto=http://www.nilkanth.info/MobilePortal/test.aspx";
            string ipAdd = Utility.GetMacAddress();
           string str= "http://128.199.78.41/ansh.php?act=recharge&custid=8154014405&pin=4405&sign=man1234&product=bs&qty=10&counter=1&msisdn=9838181570&trxid=147852369&date=29/04%2011:32:08&waittrx=0&replyto=http://www.nilkanth.info/MobilePortal/test.aspx";
            UserRegistration usr = new RegistrationBAL().GetUserByIP("info@paymezon.com", "1234567", ipAdd);
            //if (usr != null && usr.Pk_Register_ID > 0)
            {

                string strmsg = "Succ"; ///Utility.CheckRechargeDetail(Convert.ToDecimal(amt), usr);

                if (strmsg == "Succ")
                {
                    
                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(str);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream s = (Stream)response.GetResponseStream();
                    
                    StreamReader readStream = new StreamReader(s);
                    dataString = readStream.ReadToEnd();
                    response.Close();
                    
                    if (dataString.Trim().Split('|')[1].ToString() == "Transaction Successful ")
                    {
                        using (var db = new PaymezonDBEntities1())
                        {
                            string labelID = usr.Label_Id;
                            string usertype = usr.User_Type;
                            Recharge_Detail rech = new Recharge_Detail();
                            rech.Action_On_Amount = "Dr";
                            rech.Agent_Id = labelID;
                            rech.Amount = Convert.ToDecimal(amt);
                            rech.Circle_Code = "8";
                            rech.Fk_RegisterId = usr.Pk_Register_ID;
                            rech.Mobile_no = Convert.ToDecimal(mobile);
                            rech.Operator_Code = oprator;
                            rech.Recharge_By = usr.Pk_Register_ID;
                            rech.Recharge_Date = System.DateTime.Now;
                            rech.Transaction_Id = transactionId;
                            rech.Ref_number = "RF" + DateTime.Now.Ticks.ToString("D" + 5);
                            rech.Status = "Pending";
                            rech.User_Type = usertype;
                            rech.ipAdd = ipAdd;
                            rech.C_Tranx_ID = ctxnid;
                            string strMsgAmt = PaymezonBAL.RechargeBAL.InsertRegBAL(rech);
                            Utility.balance = Convert.ToDecimal(strMsgAmt);
                            return SuccessResponse(dataString.Trim().Split('|')[1].ToString());

                        }
                    }
                    else
                    {
                        return SuccessResponse(dataString.Trim().Split('|')[1].ToString());
                    }
                }
                else
                {
                    return SuccessResponse(strmsg);
                }
            }

            //else
            //{
            //    return SuccessResponse(ipAdd + " User does not exist or Ip Address not registered.");
            //}
        }
    }
}