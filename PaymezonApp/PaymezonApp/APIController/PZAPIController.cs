﻿using PaymezonApp.Models;
using PaymezonBAL;
using PaymezonDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Web.Http;
using System.Net.Http;
using System.Runtime.Remoting.Contexts;
using PaymezonApp.Models;
using System.Net.NetworkInformation;
using System.Net;
using System.Net.Mail;
using System.Web.Script.Serialization;

namespace PaymezonApp.APIController
{
    public class PZAPIController : BaseApiController
    {
        [HttpGet]
        public object MYApi(string mobile, string amt, string ctxnid, string oprator, string cid, string userName, string pass)
        {
            if (mobile != "" && amt != "" && ctxnid != "" && oprator != "" && cid != "" && userName != "" && pass != "")
            {

                try
                {
                    //string ownapi = "pzapi.com/API/PZAPI/MYApi?mobile=7567604258&amt=10&ctxnid=61145665268446&oprator=idea&cid=5&userName=info@paymezon.com&pass=607010700";

                    CommonApi api = new CommonApi();
                    DataSet ds = new DataSet();
                    // string transactionId = "TN" + DateTime.Now.Year + DateTime.Now.Ticks.ToString("D" + 3);
                    string tn = TID_GEN();
                    string transactionId = "TN" + tn + DateTime.Now.Millisecond.ToString();
                    string rf = TID_GEN();
                    string refNo = "RF" + rf + DateTime.Now.Second.ToString("D" + 5);
                   // string ipAdd = "60.254.47.182";
                    string finalres = "";
                    string ipAdd = Utility.GetMacAddress();


                    //string str = "http://128.199.78.41/ansh.php?act=recharge&custid=8154014405&pin=4405&sign=man1234&product=" + oprator + "&qty=" + amt + "&counter=1&msisdn=" + mobile + "&trxid=" + transactionId + "&date=12/08%2011:32:08&waittrx=0&replyto=http://74.208.129.24/otomax.aspx";
                    // string str = "http://128.199.78.41/ansh.php?act=recharge&custid=8154014405&pin=4405&sign=man1234&product=id&qty=1&counter=1&msisdn=1234567890&trxid=1478523690&date=29/04%2011:32:08&waittrx=0&replyto=http://www.nilkanth.info/otomax.aspx";

                    Recharge_Detail usrDet = new Recharge_Detail();
                    var usr = new RegistrationBAL().GetUserByIP(userName, pass, ipAdd);
                    if (usr != null && usr.Pk_Register_ID > 0)
                    {
                        var blocop = Utility.checkblockopbyuser(usr.Pk_Register_ID, oprator);

                        if (blocop == "Succ")
                        {


                            if (Convert.ToDecimal(amt) > 9)
                            {
                                string strmsg = Utility.CheckRechargeDetail(Convert.ToDecimal(amt), usr, Convert.ToInt64(mobile));
                                if (strmsg == "Succ")
                                {
                                    string apiName = AssignApiBAL.ApiDetail(Convert.ToDecimal(amt), oprator);
                                    if (apiName != "Sorry..Your Operator is Blocked.")
                                    {


                                        usrDet.Action_On_Amount = "Dr";
                                        usrDet.Circle_Code = "8";
                                        usrDet.Operator_Code = oprator;
                                        usrDet.Mobile_no = Convert.ToDecimal(mobile);
                                        usrDet.Amount = Convert.ToDecimal(amt);
                                        usrDet.Transaction_Id = transactionId;  //new Random().Next();
                                        usrDet.Ref_number = refNo;  // new Random().Next();
                                        usrDet.Agent_Id = usr.Label_Id;
                                        usrDet.User_Type = usr.User_Type;
                                        usrDet.Recharge_Date = System.DateTime.Now;
                                        usrDet.Recharge_By = usr.Pk_Register_ID;
                                        usrDet.Fk_RegisterId = usr.Pk_Register_ID;
                                        usrDet.C_Tranx_ID = ctxnid;
                                        usrDet.API_Name = apiName;
                                        usrDet.Status = "PENDING";
                                        usrDet.APi_Transaction_Id = "";

                                        string strMsgAmt = PaymezonBAL.RechargeBAL.InsertRegBAL(usrDet);

                                        RechargeRequestModel response = api.APIDetail(apiName, oprator, transactionId, mobile.ToString(), amt.ToString(), "8", ctxnid);
                                        if (response.status != "")
                                        {

                                            if (response.status.ToUpper().Contains("FAILED") || response.status.ToUpper().Contains("NOT ALLOWED") || response.status.ToUpper().Contains("ERROR") || response.status.ToUpper().Contains("FAILURE") || response.status.ToUpper().Contains("NOT") || response.status.ToUpper().Contains("FAIL"))
                                            {
                                                string updateapiid = TransactionBAL.updateapiidbytxnid(transactionId, "NA");
                                                finalres = "FAILED";
                                                TransactionBAL.UpdateStatus(finalres, transactionId, oprator);
                                            }
                                            else if (response.status.ToUpper().Contains("SUCCESS"))
                                            {
                                                string updateapiid = TransactionBAL.updateapiidbytxnid(transactionId, response.apitransactionID);
                                                finalres = "SUCCESS";
                                                TransactionBAL.UpdateStatus(finalres, transactionId, oprator);
                                            }



                                            Utility.balance = Convert.ToDecimal(strMsgAmt);
                                        }

                                        if (usr.Return_URL != null && finalres != "")
                                        {
                                            try
                                            {
                                                string retUrl = usr.Return_URL + "?ctxnid=" + ctxnid + "&status=" + finalres + "&apitransactionID=" + response.apitransactionID;
                                                string dataString = "";
                                                string respons;
                                                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
                                                request.Method = "GET";
                                                request.Timeout = 180000;
                                                request.ReadWriteTimeout = 180000;
                                                request.KeepAlive = false;
                                                request.Proxy = null;
                                                request.ServicePoint.ConnectionLeaseTimeout = 60000;
                                                request.ServicePoint.MaxIdleTime = 60000;
                                                HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
                                                Stream s = (Stream)responsee.GetResponseStream();
                                                StreamReader readStream = new StreamReader(s);
                                                dataString = readStream.ReadToEnd();
                                                respons = dataString;
                                                responsee.Close();
                                                s.Close();
                                                readStream.Close();
                                            }
                                            catch (Exception ex)
                                            {

                                                return SuccessResponse(response);
                                            }

                                        }

                                        return SuccessResponse(response);
                                    }

                                    else
                                    {


                                        return FailureResponse(Models.ErrorCode.ERROR100, "Sorry..Your Operator is Blocked.");

                                        //15-04-2017 found not use DJ
                                        //string retUrl = usr.Return_URL + "?ctxnid=" + ctxnid + "&status=FAILED";
                                        //string dataString = "";
                                        //string respons;
                                        //HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
                                        //request.Method = "GET";
                                        //request.Timeout = 180000;
                                        //request.ReadWriteTimeout = 180000;
                                        //request.KeepAlive = false;
                                        //request.Proxy = null;
                                        //request.ServicePoint.ConnectionLeaseTimeout = 60000;
                                        //request.ServicePoint.MaxIdleTime = 60000;
                                        //HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
                                        //Stream s = (Stream)responsee.GetResponseStream();
                                        //StreamReader readStream = new StreamReader(s);
                                        //dataString = readStream.ReadToEnd();
                                        //respons = dataString;
                                        //responsee.Close();
                                        //s.Close();
                                        //readStream.Close();

                                    }
                                }
                                else
                                {
                                    string retUrl = usr.Return_URL + "?ctxnid=" + ctxnid + "&status=FAILED";

                                    return FailureResponse(Models.ErrorCode.ERROR100, strmsg);
                                }
                            }
                            else
                            {
                                return FailureResponse(Models.ErrorCode.ERROR100, ctxnid + "Amount Value < 10 RS Not Allowed . Please Enter Correct Amount.");

                            }
                        }
                        else
                        {
                            return FailureResponse(Models.ErrorCode.ERROR100, ctxnid + "Sorry..Your Operator is Blocked.");

                        }
                    }

                    else
                    {
                        return FailureResponse(Models.ErrorCode.ERROR100, ipAdd + " User does not exist or Ip Address not registered.");
                    }

                }
                catch (Exception ex)
                {
                    //return FailureResponse(Models.ErrorCode.ERROR100, ex.Message);
                    return FailureResponse(Models.ErrorCode.ERROR100, "Have Some Error Please Contact Pz Api Admin Tell him to Error IS:" + ex.Message);
                }

            }
            else
            {
                return FailureResponse(Models.ErrorCode.ERROR100, "Plaese Check Your Request URL ....!");
            }

        }

        [HttpGet]
        public object CheckBalance(string username, string Password)
        {
            try
            {
                if (username != "" && Password != "")
                {

                    RegistrationBAL balReg = new RegistrationBAL();
                    UserRegistration usrdetail = balReg.CheckUser(username, Password);

                    if (usrdetail != null)
                    {
                        string ipAdd = Utility.GetMacAddress();
                        //string ipAdd = "60.254.47.182";
                        UserRegistration usr = new RegistrationBAL().GetUserByIP(username, Password, ipAdd);
                        if (usr != null && usr.Pk_Register_ID > 0)
                        {

                            decimal? dv = usrdetail.User_Amount.Count > 0 ? usrdetail.User_Amount.FirstOrDefault().Actual_Amount : 0;


                            return SuccessResponse("Your Current Balance is " + dv.ToString());
                        }
                        else
                        {
                            return FailureResponse(Models.ErrorCode.ERROR100, "This " + ipAdd + " Ip Address is not registered.");
                        }

                    }
                    else
                    {
                        return FailureResponse(Models.ErrorCode.ERROR100, "Please Check UserName And Password..!");

                    }


                }
                else
                {
                    return FailureResponse(Models.ErrorCode.ERROR100, "Please Check User Details..!");
                }

            }
            catch (Exception ex)
            {

                return FailureResponse(Models.ErrorCode.ERROR100, "Have Some Error Please Contact PZAPI Admin..!");
            }

        }
        [HttpGet]
        public object CheckStatus(string username, string Password, string ctxnid)
        {
            try
            {
                if (username != "" && Password != "" && ctxnid != "")
                {
                    RegistrationBAL balReg = new RegistrationBAL();
                    UserRegistration usrdetail = balReg.CheckUser(username, Password);

                    if (usrdetail != null)
                    {

                        string ipAdd = Utility.GetMacAddress();
                        //string ipAdd = "60.254.47.182";
                        UserRegistration usr = new RegistrationBAL().GetUserByIP(username, Password, ipAdd);

                        if (usr != null && usr.Pk_Register_ID > 0)
                        {
                            Transaction_Table txtndetail = new Transaction_Table();
                            txtndetail = TransactionBAL.CheckStatusbyctxnid(ctxnid);
                            if (txtndetail != null)
                            {
                                return SuccessResponse(txtndetail.Status);
                            }
                            else
                            {
                                return FailureResponse(Models.ErrorCode.ERROR100, "Sorry..Your Transaction ID Not Found or Not Matched..!");
                            }


                        }
                        else
                        {
                            return FailureResponse(Models.ErrorCode.ERROR100, "Sorry ...This " + ipAdd + " Ip Address is not registered.");
                        }
                    }
                    else
                    {
                        return FailureResponse(Models.ErrorCode.ERROR100, "Sorry..Please Check Username And Password..!");
                    }
                }
                else
                {
                    return FailureResponse(Models.ErrorCode.ERROR100, "Please Check Request URL ..!");
                }

            }
            catch (Exception ex)
            {
                return FailureResponse(Models.ErrorCode.ERROR100, "Have Some Error Please Contact PZAPI Admin..!");
            }

        }

        public static string TID_GEN()
        {
            string new_tid = System.Guid.NewGuid().ToString();
            new_tid = new_tid.Replace("-", string.Empty).ToLower();
            new_tid = new_tid.Substring(0, 15).Replace("a", "1");
            new_tid = new_tid.Substring(0, 15).Replace("b", "2");
            new_tid = new_tid.Substring(0, 15).Replace("c", "3");
            new_tid = new_tid.Substring(0, 15).Replace("d", "4");
            new_tid = new_tid.Substring(0, 15).Replace("e", "5");
            new_tid = new_tid.Substring(0, 15).Replace("f", "6");
            new_tid = new_tid.Substring(0, 15).Replace("g", "14");
            new_tid = new_tid.Substring(0, 15).Replace("h", "8");
            new_tid = new_tid.Substring(0, 15).Replace("i", "9");
            new_tid = new_tid.Substring(0, 15).Replace("j", "0");
            new_tid = new_tid.Substring(0, 15).Replace("k", "1");
            new_tid = new_tid.Substring(0, 15).Replace("l", "2");
            new_tid = new_tid.Substring(0, 15).Replace("m", "3");
            new_tid = new_tid.Substring(0, 15).Replace("n", "4");
            new_tid = new_tid.Substring(0, 15).Replace("o", "5");
            new_tid = new_tid.Substring(0, 15).Replace("p", "0");
            new_tid = new_tid.Substring(0, 15).Replace("q", "9");
            new_tid = new_tid.Substring(0, 15).Replace("r", "8");
            new_tid = new_tid.Substring(0, 15).Replace("s", "14");
            new_tid = new_tid.Substring(0, 15).Replace("t", "6");
            new_tid = new_tid.Substring(0, 15).Replace("u", "5");
            new_tid = new_tid.Substring(0, 15).Replace("v", "4");
            new_tid = new_tid.Substring(0, 15).Replace("w", "3");
            new_tid = new_tid.Substring(0, 15).Replace("x", "2");
            new_tid = new_tid.Substring(0, 15).Replace("y", "1");
            new_tid = new_tid.Substring(0, 15).Replace("z", "0");
            return new_tid;
        }
    }
}