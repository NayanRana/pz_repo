﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using PaymezonBAL;
using PaymezonApp.Models;
using PaymezonDAL;
using System.Text;

namespace PaymezonApp.APIController
{
    public class CommonController : BaseApiController
    {
        public object getcount() 
        {
            return Utility.CountTransactiondetail();
            
        }

        //public object getcountBYUSER()
        //{
        //    long ID = 55;
        //   // long ID = Convert.ToInt64(Session["U_ID"]);
        //    DateTime todaydate = System.DateTime.Now.Date;
        //    return Utility.CountTransaction(ID, todaydate);

        //}

        public object GetdailyreportList(long User_ID)
        {
            DateTime todaydate = System.DateTime.Now.Date;
            return Utility.CountTransaction(User_ID, todaydate);
        }
    }
}
