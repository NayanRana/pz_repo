﻿using PaymezonBAL;
using PaymezonDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Remoting.Contexts;
using System.Web.Http;

namespace PaymezonApp.APIController
{
    public class PZAPIResponseController : System.Web.UI.Page
    {
        [HttpGet]
        public object leoRechargeAPIResponse(string accountId, string txid, string status, string opr_id= "2")
        {
         
            try
            {
                accountId = Request.QueryString["accountId"].ToString();
                txid = Request.QueryString["txid"].ToString();
                status = Request.QueryString["Transtype"].ToString();

                string requsturl = TransactionBAL.Getrequrl(txid);

                string retUrl = requsturl + "?ctxnid=" + txid + "&status=" + status;
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
                string dataString = "";
                string respons;
                request.Method = "GET";
                request.Timeout = 180000;
                request.ReadWriteTimeout = 180000;
                request.KeepAlive = false;
                request.Proxy = null;
                request.ServicePoint.ConnectionLeaseTimeout = 60000;
                request.ServicePoint.MaxIdleTime = 60000;
                HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
                Stream s = (Stream)responsee.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                dataString = readStream.ReadToEnd();
                respons = dataString;
                responsee.Close();
                s.Close();
                readStream.Close();

                if (status != "")
                {
                    string url = txid + "@" + accountId + "@" + status;
                    using (var db = new PaymezonDBEntities1())
                    {
                        Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                        input.Service_name = "leoRechargeAPI";
                        input.REFERENCE_NUMBER = txid;
                        input.COURENTLEVEL = null;
                        input.Input_Request = "789";
                        input.Output_Responce = url;
                        input.ReqDate = System.DateTime.Now;
                        input.ResDate = System.DateTime.Now;
                        db.Vendor_InptOutPut_ReqDetails.Add(input);
                        db.SaveChanges();

                    }
                }
                string str = TransactionBAL.UpdateStatus(status, txid, opr_id);

                return "Succ";
            }
            catch (Exception ex)
            {
                
                throw;
            }

        }

        public object MyRioAPIResponse ( string ipay_id,string cust_no,string amt,string opr_id,string status,string bal, string agent_id)
        {

            try
            {
                agent_id = Request.QueryString["rid"].ToString();
                ipay_id = Request.QueryString["txid"].ToString();
                opr_id = Request.QueryString["prvdr"].ToString();
                cust_no = Request.QueryString["rchNo"].ToString();
                status = Request.QueryString["transtype"].ToString();
                amt = Request.QueryString["amt"].ToString();
                bal = Request.QueryString["bal"].ToString();

                string rtnturl = TransactionBAL.Getrequrl(ipay_id);
                string retUrl = rtnturl + "?ctxnid=" + ipay_id + "&status=" + status;
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);

                if (!string.IsNullOrEmpty(Request.QueryString["transtype"]))
                {
                    status = Request.QueryString["transtype"];
                }
                else if (!string.IsNullOrEmpty(Request["transtype"]))
                {
                    status = Request["transtype"];
                }
                else if (!string.IsNullOrEmpty(Request.Form["transtype"]))
                {
                    status = Request.Form["transtype"];
                }
                if (status != "")
                {
                    string url = ipay_id + "@" + agent_id + "@" + opr_id + "@" + status + "@" + cust_no;
                    using (var db = new PaymezonDBEntities1())
                    {
                        Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                        input.Service_name = "MyRioAPI";
                        input.REFERENCE_NUMBER = ipay_id;
                        input.COURENTLEVEL = null;
                        input.Input_Request = "789";
                        input.Output_Responce = url;
                        input.ReqDate = System.DateTime.Now;
                        input.ResDate = System.DateTime.Now;
                        db.Vendor_InptOutPut_ReqDetails.Add(input);
                        db.SaveChanges();

                    }
                }
                string str = TransactionBAL.UpdateStatus(status, ipay_id, opr_id);
                return "Succ";
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public object NilkanthAPIResponse(string status, string refnumber, string operatorid, string vendorid)
        {
            try
            {

                 status = Request.QueryString["status"];
                 refnumber = Request.QueryString["uniqueid"];
                 operatorid = Request.QueryString["operator_id"];
                 vendorid = Request.QueryString["transaction_id"];

                 string requsturl = TransactionBAL.Getrequrl(vendorid);
                 string retUrl = requsturl + "?ctxnid=" + vendorid + "&status=" + status;
                 HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
                 string dataString = "";
                 string respons;
                 request.Method = "GET";
                 request.Timeout = 180000;
                 request.ReadWriteTimeout = 180000;
                 request.KeepAlive = false;
                 request.Proxy = null;
                 request.ServicePoint.ConnectionLeaseTimeout = 60000;
                 request.ServicePoint.MaxIdleTime = 60000;
                 HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
                 Stream s = (Stream)responsee.GetResponseStream();
                 StreamReader readStream = new StreamReader(s);
                 dataString = readStream.ReadToEnd();
                 respons = dataString;
                 responsee.Close();
                 s.Close();
                 readStream.Close();

                 if (!string.IsNullOrEmpty(Request.QueryString["transtype"]))
                 {
                     status = Request.QueryString["transtype"];
                 }
                 else if (!string.IsNullOrEmpty(Request["transtype"]))
                 {
                     status = Request["transtype"];
                 }
                 else if (!string.IsNullOrEmpty(Request.Form["transtype"]))
                 {
                     status = Request.Form["transtype"];
                 }
                 if (status != "")
                 {
                     string url =  refnumber + "@" + operatorid + "@" + status + "@" + vendorid;
                     using (var db = new PaymezonDBEntities1())
                     {
                         Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                         input.Service_name = "NilkanthAPI";
                         input.REFERENCE_NUMBER = vendorid;
                         input.COURENTLEVEL = null;
                         input.Input_Request = "789";
                         input.Output_Responce = url;
                         input.ReqDate = System.DateTime.Now;
                         input.ResDate = System.DateTime.Now;
                         db.Vendor_InptOutPut_ReqDetails.Add(input);
                         db.SaveChanges();

                     }
                 }
                 string str = TransactionBAL.UpdateStatus(status, vendorid, operatorid);

                return "succ";
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

        public object MangalamAPIResponse(string STATUS, string TRID, string CLIENTID, string OPID, string BALANCE) 
        {
            //string response = "http://www.yourdomainname.com/yourresponsepage?STATUS=Success&TRID=10&CLIENTID=1193221&OPID=RM14150032078190&BALANCE=9920.00";
            STATUS = Request.QueryString["STATUS"].ToString();
            TRID = Request.QueryString["TRID"].ToString();
            CLIENTID = Request.QueryString["CLIENTID"].ToString();
            OPID = Request.QueryString["OPID"].ToString();
            BALANCE = Request.QueryString["BALANCE"].ToString();

            string requsturl = TransactionBAL.Getrequrl(TRID);
            string retUrl = requsturl + "?ctxnid=" + TRID + "&status=" + STATUS;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
            string dataString = "";
            string respons;
            request.Method = "GET";
            request.Timeout = 180000;
            request.ReadWriteTimeout = 180000;
            request.KeepAlive = false;
            request.Proxy = null;
            request.ServicePoint.ConnectionLeaseTimeout = 60000;
            request.ServicePoint.MaxIdleTime = 60000;
            HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
            Stream s = (Stream)responsee.GetResponseStream();
            StreamReader readStream = new StreamReader(s);
            dataString = readStream.ReadToEnd();
            respons = dataString;
            responsee.Close();
            s.Close();
            readStream.Close();
            if (STATUS != "")
            {
                string url = CLIENTID + "@" + OPID + "@" + STATUS + "@" + TRID;
                using (var db = new PaymezonDBEntities1())
                {
                    Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                    input.Service_name = "MangalamAPI";
                    input.REFERENCE_NUMBER = TRID;
                    input.COURENTLEVEL = null;
                    input.Input_Request = "789";
                    input.Output_Responce = url;
                    input.ReqDate = System.DateTime.Now;
                    input.ResDate = System.DateTime.Now;
                    db.Vendor_InptOutPut_ReqDetails.Add(input);
                    db.SaveChanges();

                }
            }
            string str = TransactionBAL.UpdateStatus(STATUS, TRID, OPID);

            return "succ";

        }

        public object oyepeAPIResponse(string transtype, string txid, string accountId, string OPID = "2")
        {
            //string response = "http://74.208.129.24/API/PZAPIResponse/oyepeAPIResponse?accountId=2&txid=2323&transtype=s";
            transtype = Request.QueryString["Transtype"].ToString();
            txid = Request.QueryString["Txid"].ToString();
            accountId = Request.QueryString["accountId"].ToString();
           // OPID = Request.QueryString["OPID"].ToString();
           // BALANCE = Request.QueryString["BALANCE"].ToString();
            if (transtype == "s")
            {
                transtype = "SUCCESS";
            }
            else
            {
                transtype = "FAILED";
            }
            string requsturl = TransactionBAL.Getrequrl(txid);
            string retUrl = requsturl + "?ctxnid=" + txid + "&status=" + transtype;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
            string dataString = "";
            string respons;
            request.Method = "GET";
            request.Timeout = 180000;
            request.ReadWriteTimeout = 180000;
            request.KeepAlive = false;
            request.Proxy = null;
            request.ServicePoint.ConnectionLeaseTimeout = 60000;
            request.ServicePoint.MaxIdleTime = 60000;
            HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
            Stream s = (Stream)responsee.GetResponseStream();
            StreamReader readStream = new StreamReader(s);
            dataString = readStream.ReadToEnd();
            respons = dataString;
            responsee.Close();
            s.Close();
            readStream.Close();
            if (transtype != "")
            {
                string url = accountId + "@" + transtype + "@" + txid;
                using (var db = new PaymezonDBEntities1())
                {
                    Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                    input.Service_name = "oyope";
                    input.REFERENCE_NUMBER = txid;
                    input.COURENTLEVEL = null;
                    input.Input_Request = "789";
                    input.Output_Responce = url;
                    input.ReqDate = System.DateTime.Now;
                    input.ResDate = System.DateTime.Now;
                    db.Vendor_InptOutPut_ReqDetails.Add(input);
                    db.SaveChanges();

                }
            }
            string str = TransactionBAL.UpdateStatus(transtype, accountId, OPID);

            return "succ";

        }

        public object OOSwebAPI(string txid,string rchNo,string amt,string prvdr,string transtype,string bal,string rid)
        {
            string response = "txid=Operator Id&rchNo=Recharge Number&amt=Amount&prvdr=Operator Code&transtype=SUCCESS&bal=Remain Balance&rid=yourrecharge ID";
            try
            {
                txid = Request.QueryString["txid"].ToString();
                rchNo = Request.QueryString["rchNo"].ToString();
                amt = Request.QueryString["amt"].ToString();
                prvdr = Request.QueryString["prvdr"].ToString();
                transtype = Request.QueryString["transtype"].ToString();
                bal = Request.QueryString["bal"].ToString();
                rid = Request.QueryString["rid"].ToString();

                string requsturl = TransactionBAL.Getrequrl(txid);

                string retUrl = requsturl + "?ctxnid=" + rid + "&status=" + transtype;
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(retUrl);
                string dataString = "";
                string respons;
                request.Method = "GET";
                request.Timeout = 180000;
                request.ReadWriteTimeout = 180000;
                request.KeepAlive = false;
                request.Proxy = null;
                request.ServicePoint.ConnectionLeaseTimeout = 60000;
                request.ServicePoint.MaxIdleTime = 60000;
                HttpWebResponse responsee = (HttpWebResponse)request.GetResponse();
                Stream s = (Stream)responsee.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                dataString = readStream.ReadToEnd();
                respons = dataString;
                responsee.Close();
                s.Close();
                readStream.Close();

                if (transtype != "")
                {
                    string url = rid + "@" + txid + "@" + transtype;
                    using (var db = new PaymezonDBEntities1())
                    {
                        Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                        input.Service_name = "OOSwebAPI";
                        input.REFERENCE_NUMBER = rid;
                        input.COURENTLEVEL = null;
                        input.Input_Request = "789";
                        input.Output_Responce = url;
                        input.ReqDate = System.DateTime.Now;
                        input.ResDate = System.DateTime.Now;
                        db.Vendor_InptOutPut_ReqDetails.Add(input);
                        db.SaveChanges();
                    }
                }
                string str = TransactionBAL.UpdateStatus(transtype, rid, txid);

                return "Succ";
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



    }
}