﻿using PaymezonBAL;
using PaymezonDAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PaymezonApp.APIController
{
    public class RechargeController : BaseApiController
    {
        [HttpGet]
        public object Recharge(string mobile, string amt, string ctxnid, string oprator, string cid, string userName, string pass)
        {
            string dataString = "";
            string transactionId = "TN" + DateTime.Now.Year + DateTime.Now.Ticks.ToString("D" + 3);
            string str = "http://103.231.32.126:85/?mobile=" + mobile + "&amt=" + amt + "&ctxnid=" + transactionId + "&operator=" + oprator + "&type=Recharge&rt=4&cid=" + cid + "";
            	 
            string ipAdd = Utility.GetMacAddress();

            UserRegistration usr = new RegistrationBAL().GetUserByIP(userName, pass, ipAdd);
            if (usr != null && usr.Pk_Register_ID > 0)
            {

                string strmsg = Utility.CheckRechargeDetail(Convert.ToDecimal(amt), usr, Convert.ToInt64(mobile));

                if (strmsg == "Succ")
                {
                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(str);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream s = (Stream)response.GetResponseStream();
                    StreamReader readStream = new StreamReader(s);
                    dataString = readStream.ReadToEnd();
                    response.Close();


                    if (dataString.Trim().Split('|')[1].ToString() == "Transaction Successful ")
                    {
                        using (var db = new PaymezonDBEntities1())
                        {
                            string labelID = usr.Label_Id;
                            string usertype = usr.User_Type;
                            Recharge_Detail rech = new Recharge_Detail();
                            rech.Action_On_Amount = "Dr";
                            rech.Agent_Id = labelID;
                            rech.Amount = Convert.ToDecimal(amt);
                            rech.Circle_Code = "8";
                            rech.Fk_RegisterId = usr.Pk_Register_ID;
                            rech.Mobile_no = Convert.ToDecimal(mobile);
                            rech.Operator_Code = oprator;
                            rech.Recharge_By = usr.Pk_Register_ID;
                            rech.Recharge_Date = System.DateTime.Now;
                            rech.Transaction_Id = transactionId;
                            rech.Ref_number = "RF" + DateTime.Now.Ticks.ToString("D" + 5);
                            rech.Status = "Pending";
                            rech.User_Type = usertype;
                            rech.ipAdd = ipAdd;
                            rech.C_Tranx_ID = ctxnid;
                            string strMsgAmt = PaymezonBAL.RechargeBAL.InsertRegBAL(rech);
                            Utility.balance = Convert.ToDecimal(strMsgAmt);
                            return SuccessResponse(dataString.Trim().Split('|')[1].ToString());

                        }
                    }
                    else
                    {
                        return SuccessResponse(dataString.Trim().Split('|')[1].ToString());
                    }
                }
                else
                {
                    return SuccessResponse(strmsg);
                }
            }

            else
            {
                return SuccessResponse(ipAdd + " User does not exist or Ip Address not registered.");
            }
        }

      

    }
}