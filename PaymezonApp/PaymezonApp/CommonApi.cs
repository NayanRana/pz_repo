using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PaymezonDAL;
using System.Net;
using System.IO;
using System.Xml;
using System.Data;
using PaymezonBAL;
using System.Net.Http;
using System.Runtime.Remoting.Contexts;
using System.Web.Http;
using PaymezonApp.Models;
using System.Net.NetworkInformation;
using System.Net;
using System.Net.Mail;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;


namespace PaymezonApp
{
    public class CommonApi
    {
        public RechargeRequestModel APIDetail(string api_name, string OperatorCode, string ClientID, string Number, string Amount, string CircleCode, string customertxnId)
        {
            string respons;
            int rchamt = Convert.ToInt32(Amount);
            RechargeRequestModel objresponce = new RechargeRequestModel();
            try
            {
                string dataString = "";

                string opCode = Utility.GetOperatorCode(OperatorCode, api_name);
                if (api_name == "VishnuAPI")
                {
                    objresponce.status = "PENDING";
                    objresponce.apiTranscode = customertxnId;
                    objresponce.mobilenumber = Number;
                    objresponce.amount = Amount;
                    objresponce.transactionid = ClientID;
                    using (var db = new PaymezonDBEntities1())
                    {
                        string strUrl1 = customertxnId + "@" + Number + "@" + Amount + "@" + ClientID;
                        Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                        input.Service_name = api_name;
                        input.REFERENCE_NUMBER = ClientID;
                        input.COURENTLEVEL = null;
                        input.Input_Request = strUrl1;
                        input.Output_Responce = dataString;
                        input.ReqDate = System.DateTime.Now;
                        input.ResDate = System.DateTime.Now;
                        db.Vendor_InptOutPut_ReqDetails.Add(input);
                        db.SaveChanges();

                    }
                    return objresponce;
                }
                Api_Detail apiDetail = Utility.GetAPiURL(api_name);
                string strUrl = apiDetail.Request_Url;
                string RechargeType = Utility.GetRechargeType(OperatorCode, api_name);
                if (opCode != "Your Operator Not Match..!")
                {
                    if (api_name == "leoRechargeAPI" || api_name == "MangalamApi" || api_name == "oyepe" || api_name == "JioApi" || api_name == "SmsAcharyaAPI" || api_name == "AnshApi" || api_name == "NeelKanthAPI" || api_name == "OOSwebAPI")
                    {
                        strUrl = strUrl.Replace("Mobile", Number);
                        strUrl = strUrl.Replace("Amount", Amount);
                        strUrl = strUrl.Replace("OperatorCode", opCode);
                        strUrl = strUrl.Replace("ClientID", ClientID);
                        strUrl = strUrl.Replace("CircleCode", CircleCode);
                        strUrl = strUrl.Replace("RechargeType", RechargeType);
                    }
                    else if (api_name == "NomzyPayAPI")
                    {
                        string ProductCode = "";
                        decimal amt = Convert.ToDecimal(Amount);
                        nomzy.Service1 client = new nomzy.Service1();
                        string strUrl1 = customertxnId + "@" + Number + "@" + Amount + "@" + ClientID;
                        dataString = client.Recharge("PAYMEZON", "YMQpF8fJ", ClientID, 1, true, opCode, ProductCode, CircleCode, Number, amt, true);
                        string[] res = dataString.Split('~');
                        string[] apitxncode = res[5].ToString().Split('=');
                        objresponce.apitransactionID = apitxncode[1].ToString();

                        if (res[2].ToString().Contains("0"))
                        {

                            objresponce.status = "SUCCESS";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                            objresponce.transactionid = ClientID;
                        }
                        else if (res[2].ToString().Contains("1"))
                        {
                            objresponce.status = "PENDING";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                            objresponce.transactionid = ClientID;
                        }
                        else if (!res[2].ToString().Contains("0") || !res[2].ToString().Contains("1"))
                        {
                            objresponce.apitransactionID = "NA";
                            objresponce.status = "FAILED";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                            objresponce.transactionid = ClientID;
                        }
                        using (var db = new PaymezonDBEntities1())
                        {

                            Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                            input.Service_name = api_name;
                            input.REFERENCE_NUMBER = ClientID;
                            input.COURENTLEVEL = null;
                            input.Input_Request = strUrl1;
                            input.Output_Responce = dataString;
                            input.ReqDate = System.DateTime.Now;
                            input.ResDate = System.DateTime.Now;
                            db.Vendor_InptOutPut_ReqDetails.Add(input);
                            db.SaveChanges();
                        }
                        return objresponce;
                    }
                    else
                    {
                        strUrl = strUrl.Replace("=Mobile", "=" + Number);
                        strUrl = strUrl.Replace("=Amount", "=" + Amount);
                        strUrl = strUrl.Replace("=OperatorCode", "=" + opCode);
                        strUrl = strUrl.Replace("=ClientID", "=" + ClientID);
                        strUrl = strUrl.Replace("=CircleCode", "=" + CircleCode);
                        strUrl = strUrl.Replace("=RechargeType", "=" + RechargeType);
                    }

                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(strUrl);
                    request.Method = "GET";
                    request.Timeout = 180000;
                    request.ReadWriteTimeout = 180000;
                    request.KeepAlive = false;
                    request.Proxy = null;
                    request.ServicePoint.ConnectionLeaseTimeout = 60000;
                    request.ServicePoint.MaxIdleTime = 60000;
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream s = (Stream)response.GetResponseStream();
                    StreamReader readStream = new StreamReader(s);
                    dataString = readStream.ReadToEnd();
                    respons = dataString;
                    response.Close();
                    s.Close();
                    readStream.Close();

                    if (strUrl != "")
                    {
                        using (var db = new PaymezonDBEntities1())
                        {
                            Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                            input.Service_name = api_name;
                            input.REFERENCE_NUMBER = ClientID;
                            input.COURENTLEVEL = null;
                            input.Input_Request = strUrl;
                            input.Output_Responce = dataString;
                            input.ReqDate = System.DateTime.Now;
                            input.ResDate = System.DateTime.Now;
                            db.Vendor_InptOutPut_ReqDetails.Add(input);
                            db.SaveChanges();

                        }

                    }

                    if (api_name == "MangalamApi")
                    {
                        string[] res = respons.Split(',');
                        objresponce.status = res[0];
                        objresponce.apiTranscode = customertxnId;
                        objresponce.mobilenumber = Number;
                        objresponce.amount = Amount;
                        objresponce.apitransactionID = res[1];
                        objresponce.transactionid = ClientID;

                    }
                    else if (api_name == "oyepe")
                    {
                        if (respons == "you can't send same Recharge Request for 10 min." || respons == "Please Check your Request format.You have Entered an API Request with Invalid format.")
                        {
                            objresponce.status = "FAILED";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.transactionid = ClientID;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                            return objresponce;
                        }

                        string[] res = respons.Split(',');
                        objresponce.apitransactionID = res[2].Split(':')[1];

                        if (res[0].ToUpper().Contains("SUCCESS"))
                        {
                            objresponce.status = "SUCCESS";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                            objresponce.transactionid = ClientID;
                        }
                        else if (res[0].ToUpper().Contains("PROCESSED"))
                        {
                            objresponce.status = "PENDING";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                            objresponce.transactionid = ClientID;
                        }
                        else
                        {
                            objresponce.status = "FAILED";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.transactionid = ClientID;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                        }

                        //objresponce.apitransactionID = res[1].ToString();

                    }
                    else if (api_name == "leoRechargeAPI")
                    {

                        DataSet ds = new DataSet();
                        // string otoresp = "<?xml version='1.0' encoding='ISO-8859-1'?><_ApiResponse><statusCode>10008</statusCode><statusDescription>REQUEST ACCEPTED.</statusDescription><availableBalance>970.52</availableBalance><requestID>148179938675754</requestID></_ApiResponse>";

                        ds = CommonApi.ConvertXMLToDataSet(respons);
                        string apiresponseID = ds.Tables[0].Rows[0]["requestID"].ToString();
                        string responses = ds.Tables[0].Rows[0]["statusDescription"].ToString();

                        if (responses == "REQUEST ACCEPTED.")
                        {
                            responses = "PENDING";
                        }
                        else
                        {
                            responses = "FAILED";
                        }
                        objresponce.transactionid = ClientID;
                        objresponce.mobilenumber = Number;
                        objresponce.amount = Amount;
                        objresponce.status = responses;
                    }
                    else if (api_name == "SmsAcharyaAPI")
                    {
                        DataSet ds = new DataSet();
                        ds = CommonApi.ConvertXMLToDataSet(respons);
                        string status = Convert.ToString(ds.Tables[0].Rows[0]["status"]);


                        if (status.Equals("SUCCESS"))
                        {
                            objresponce.status = "SUCCESS";
                            objresponce.apitransactionID = Convert.ToString(ds.Tables[0].Rows[0]["operator_ref"]);
                        }
                        else if (status.Equals("PENDING"))
                        {
                            objresponce.status = "PENDING";
                        }
                        else
                        {
                            objresponce.status = "FAILED";
                            objresponce.apitransactionID = "NA";
                        }
                        objresponce.apiTranscode = customertxnId;
                        objresponce.transactionid = ClientID;
                        objresponce.mobilenumber = Number;
                        objresponce.amount = Amount;
                    }
                    else if (api_name == "AnshApi")
                    {
                        if (respons.ToUpper().Contains("ACCEPTED") || respons.ToUpper().Contains("PROCEED"))
                        {
                            objresponce.status = "PENDING";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.transactionid = ClientID;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                        }
                        else if (respons.ToUpper().Contains("Done"))
                        {
                            objresponce.status = "SUCCESS";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.transactionid = ClientID;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                        }
                        else
                        {
                            objresponce.status = "FAILED";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.transactionid = ClientID;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                        }
                    }
                    else if (api_name == "AnikBansal")
                    {
                        string[] res = respons.Split(' ');
                        if (respons.ToUpper().Contains("REQUEST ACCEPTED"))
                        {
                            objresponce.status = "PENDING";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.transactionid = ClientID;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                        }
                        else if (respons.ToUpper().Contains("ERROR"))
                        {
                            objresponce.status = "FAILED";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.transactionid = ClientID;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                        }
                        else
                        {
                            objresponce.status = "PENDING";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.transactionid = ClientID;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                        }

                    }
                    else if (api_name == "MYRECHARGE")
                    {
                        string[] res = respons.Split('^');

                        // string exresponse = "RequestAccepted^TXN789456^MLID0057093700^Idea^10^Roaming";
                        //  objresponce.apitransactionID = respons.Split('=')[2].Split('\r')[0].ToString();  //"TU2943295581";
                        objresponce.apitransactionID = res[2].ToString();

                        if (res[0].ToUpper().Contains("REQUESTACCEPTED"))
                        {
                            objresponce.status = "PENDING";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.transactionid = ClientID;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                        }
                        else
                        {
                            objresponce.status = "FAILED";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.transactionid = ClientID;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                        }
                    }
                    else if (api_name == "JioApi")
                    {
                        dynamic data = JObject.Parse(respons);
                        string status = data.Status.ToString();
                        objresponce.apitransactionID = data.TXID.ToString();

                        //{"OpeningBalance":"47429.00","TranactionID":"BR00004SPFQT","CustomerNumber":"8849972175","Amount":"309","Name":"MUNNA__CHAUHAN","Status":"SUCCESS","Message":"Dear Partner Order BR00004SPFQT successfully Processed on 8849972175 of MUNNA__CHAUHAN with amount 309.00 Balance 47120.00"}
                        //string status =  new Microsoft.CSharp.RuntimeBinder.DynamicMetaObjectProviderDebugView(data).Items[6];
                        //  string message = "7678471874;Success;110;BR000052VPMG;309;Sonu Rathor;Time:1;0;BAL:74159.00";
                        string message = data.Message.ToString();
                        if (message.ToUpper().Contains("SUCCESS"))
                        {
                            objresponce.status = "SUCCESS";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.transactionid = ClientID;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                        }

                        else
                        {
                            objresponce.status = "FAILED";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.transactionid = ClientID;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                            objresponce.apitransactionID = data.TXID.ToString();
                        }
                        //if (data.Status.ToString() == "SUCCESS")
                        //{
                        //    objresponce.status = "SUCCESS";
                        //    objresponce.apiTranscode = customertxnId;
                        //    objresponce.transactionid = ClientID;
                        //    objresponce.mobilenumber = Number;
                        //    objresponce.amount = Amount;
                        //}
                        //else if (data.Status.ToString() == "FAILED")
                        //{
                        //    objresponce.status = "FAILED";
                        //    objresponce.apiTranscode = customertxnId;
                        //    objresponce.transactionid = ClientID;
                        //    objresponce.mobilenumber = Number;
                        //    objresponce.amount = Amount;
                        //    objresponce.apitransactionID = data.TXID.ToString();
                        //}
                    }
                    else if (api_name == "NeelKanthAPI")
                    {
                        string[] res = respons.Split(',');

                        objresponce.apitransactionID = res[2].ToString();

                        if (res[0].ToUpper().Contains("PENDING"))
                        {
                            objresponce.status = "PENDING";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.transactionid = ClientID;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                        }
                        else if (res[0].ToUpper().Contains("SUCCESS"))
                        {
                            objresponce.status = "SUCCESS";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.transactionid = ClientID;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                        }
                        else
                        {
                            objresponce.status = "FAILED";
                            objresponce.apiTranscode = customertxnId;
                            objresponce.transactionid = ClientID;
                            objresponce.mobilenumber = Number;
                            objresponce.amount = Amount;
                        }
                    }

                    else if (api_name == "OOSwebAPI")
                    {
                        DataSet ds = new DataSet();
                        ds = CommonApi.ConvertXMLToDataSet(respons);
                        string apiresponseID = ds.Tables[0].Rows[0]["Operatorid"].ToString();
                        string responses = ds.Tables[0].Rows[0]["status"].ToString();

                        if (responses == "PROCESS")
                        {
                            responses = "PENDING";
                        }
                        else if (responses == "FAILED")
                        {
                            responses = "FAILED";
                        }
                        else
                        {
                            responses = "PENDING";
                        }
                        objresponce.transactionid = ClientID;
                        objresponce.mobilenumber = Number;
                        objresponce.amount = Amount;
                        objresponce.status = responses;
                    }
                    else
                    {
                        objresponce.status = respons;
                    }
                }
                else
                {
                    objresponce.status = "Your Operator was not matched....";
                    objresponce.apiTranscode = customertxnId;
                    objresponce.transactionid = ClientID;
                    objresponce.mobilenumber = Number;
                    objresponce.amount = Amount;
                    return objresponce;
                }
            }
            catch (Exception ex)
            {
                objresponce.apiTranscode = customertxnId;
                objresponce.status = "PENDING";
                using (var db = new PaymezonDBEntities1())
                {
                    Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                    input.Service_name = api_name;
                    input.REFERENCE_NUMBER = ClientID;
                    input.COURENTLEVEL = null;
                    input.Input_Request = null;
                    input.Output_Responce = ex.ToString();
                    input.ReqDate = System.DateTime.Now;
                    input.ResDate = System.DateTime.Now;
                    db.Vendor_InptOutPut_ReqDetails.Add(input);
                    db.SaveChanges();
                }
                return objresponce;
            }
            return objresponce;
        }
        public static DataSet ConvertXMLToDataSet(string xmlData)
        {
            StringReader stream = null;
            XmlTextReader reader = null;
            try
            {
                DataSet xmlDS = new DataSet();
                stream = new StringReader(xmlData);
                // Load the XmlTextReader from the stream
                reader = new XmlTextReader(stream);
                xmlDS.ReadXml(reader);
                return xmlDS;
            }
            catch (Exception ex)
            {
                // WriteErrorToLogFile("CommonFunction :: ConvertXMLToDataSet :: " + ex.Message);
                return null;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }// Use this function to get XML string from a dataset
        public string AnshApi(string mobile, string amt, string ctxnid, string oprator, string cid, string userName, string pass)
        {
            try
            {

                string dataString = "";
                DataSet ds = new DataSet();
                string transactionId = "TN" + DateTime.Now.Year + DateTime.Now.Ticks.ToString("D" + 3);
                string str = "http://128.199.78.41/ansh.php?act=recharge&custid=8154014405&pin=4405&sign=man1234&product=" + oprator + "&qty=" + amt + "&counter=1&msisdn=" + mobile + "&trxid=" + transactionId + "&date=12/08%2011:32:08&waittrx=0&replyto=http://74.208.129.24/otomax.aspx";
                string ipAdd = Utility.GetMacAddress();
                // string str = "http://128.199.78.41/ansh.php?act=recharge&custid=8154014405&pin=4405&sign=man1234&product=id&qty=1&counter=1&msisdn=1234567890&trxid=1478523690&date=29/04%2011:32:08&waittrx=0&replyto=http://www.nilkanth.info/otomax.aspx";
                UserRegistration usr = new RegistrationBAL().GetUserByIP(userName, pass, ipAdd);
                if (usr != null && usr.Pk_Register_ID > 0)
                {

                    string strmsg = "Succ";// Utility.CheckRechargeDetail(Convert.ToDecimal(amt), usr);

                    if (strmsg == "Succ")
                    {

                        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(str);
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        Stream s = (Stream)response.GetResponseStream();
                        StreamReader readStream = new StreamReader(s);
                        dataString = readStream.ReadToEnd();
                        response.Close();

                        if (str != "")
                        {
                            using (var db = new PaymezonDBEntities1())
                            {
                                Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                                input.Service_name = "456";
                                input.REFERENCE_NUMBER = "456";
                                input.COURENTLEVEL = null;
                                input.Input_Request = str;
                                input.Output_Responce = dataString;
                                input.ReqDate = System.DateTime.Now;
                                input.ResDate = System.DateTime.Now;
                                db.Vendor_InptOutPut_ReqDetails.Add(input);
                                db.SaveChanges();

                            }
                        }

                        if (dataString.Contains("proceed") || dataString.Contains("ACCEPTED"))
                        {
                            using (var db = new PaymezonDBEntities1())
                            {
                                string labelID = usr.Label_Id;
                                string usertype = usr.User_Type;
                                Recharge_Detail rech = new Recharge_Detail();
                                rech.Action_On_Amount = "Dr";
                                rech.Agent_Id = labelID;
                                rech.Amount = Convert.ToDecimal(amt);
                                rech.Circle_Code = "8";
                                rech.Fk_RegisterId = usr.Pk_Register_ID;
                                rech.Mobile_no = Convert.ToDecimal(mobile);
                                rech.Operator_Code = oprator;
                                rech.Recharge_By = usr.Pk_Register_ID;
                                rech.Recharge_Date = System.DateTime.Now;
                                rech.Transaction_Id = transactionId;
                                rech.Ref_number = "RF" + DateTime.Now.Ticks.ToString("D" + 5);
                                rech.Status = "Pending";
                                rech.User_Type = usertype;
                                rech.ipAdd = ipAdd;
                                rech.C_Tranx_ID = ctxnid;
                                string strMsgAmt = PaymezonBAL.RechargeBAL.InsertRegBAL(rech);
                                Utility.balance = Convert.ToDecimal(strMsgAmt);
                                return dataString;

                            }
                        }
                        else
                        {
                            return dataString;
                        }
                    }
                    else
                    {
                        return dataString;
                    }
                }

                else
                {
                    return (ipAdd + " User does not exist or Ip Address not registered.");
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}