﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PaymezonApp.Startup))]
namespace PaymezonApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
