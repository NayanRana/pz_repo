﻿using PaymezonBAL;
using PaymezonDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PaymezonApp
{
    public partial class MyRioAPIResponse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string ipay_id, cust_no, amt, opr_id, status, bal, agent_id;

            agent_id = Request.QueryString["rid"].ToString();
            ipay_id = Request.QueryString["txid"].ToString();
            opr_id = Request.QueryString["prvdr"].ToString();
            cust_no = Request.QueryString["rchNo"].ToString();
            status = Request.QueryString["transtype"].ToString();
            amt = Request.QueryString["amt"].ToString();
            bal = Request.QueryString["bal"].ToString();


            if (!string.IsNullOrEmpty(Request.QueryString["transtype"]))
            {
                status = Request.QueryString["transtype"];
            }
            else if (!string.IsNullOrEmpty(Request["transtype"]))
            {
                status = Request["transtype"];
            }
            else if (!string.IsNullOrEmpty(Request.Form["transtype"]))
            {
                status = Request.Form["transtype"];
            }
            if (status != "")
            {
                string url = ipay_id + "@" + agent_id + "@" + opr_id + "@" + status + "@" + cust_no;
                using (var db = new PaymezonDBEntities1())
                {
                    Vendor_InptOutPut_ReqDetails input = new Vendor_InptOutPut_ReqDetails();
                    input.Service_name = "MyRioAPI";
                    input.REFERENCE_NUMBER = ipay_id;
                    input.COURENTLEVEL = null;
                    input.Input_Request = "789";
                    input.Output_Responce = url;
                    input.ReqDate = System.DateTime.Now;
                    input.ResDate = System.DateTime.Now;
                    db.Vendor_InptOutPut_ReqDetails.Add(input);
                    db.SaveChanges();

                }
            }
            string str = TransactionBAL.UpdateStatus(status,ipay_id,opr_id);
        }
    }
}