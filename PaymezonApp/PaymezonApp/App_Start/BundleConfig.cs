﻿using System.Web;
using System.Web.Optimization;

namespace PaymezonApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/js/plugins/bootstrap/bootstrap-datepicker.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                "~/js/plugins/jquery/jquery-ui.min.js",
                    "~/js/plugins.js",
                //"~/js/settings.js",
                    "~/js/actions.js",
                    "~/js/plugins/icheck/icheck.min.js",
                    "~/js/plugins/bootstrap/bootstrap-select.js",
                    "~/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js",
                    "~/js/plugins/bootstrap/bootstrap-datepicker.js",
                    "~/js/plugins/bootstrap/bootstrap-file-input.js",
                    "~/js/plugins/bootstrap/bootstrap-file-input.js",
                    "~/js/plugins/datatables/jquery.dataTables.min.js",
                    "~/js/plugins/bootstrap/bootstrap-datepicker.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/dashboard").Include(
                //"~/admin/plugins/morrisjs/morris.js",
                 "~/js/plugins/morris/raphael-min.js",
                "~/js/plugins/morris/morris.min.js",
                //"~/js/plugins/rickshaw/d3.v3.js",
                //"~/js/plugins/rickshaw/rickshaw.min.js",
                "~/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
                "~/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
                "~/js/plugins/bootstrap/bootstrap-datepicker.js",
                 "~/js/plugins/moment.min.js",
                 "~/js/plugins/daterangepicker/daterangepicker.js",
                 "~/js/plugins.js",
                 "~/js/actions.js",
                 "~/js/demo_dashboard.js"
                ));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/css/bootstrap/bootstrap.css",
                      "~/css/theme-default.css"));

            bundles.Add(new StyleBundle("~/bundles/adminjs").Include(
                "~/Scripts/modernizr-2.6.2.js",
                "~/js/plugins/jquery/jquery-ui.min.js",
                //"~/admin/plugins/bootstrap-select/js/bootstrap-select.js",
                "~/admin/plugins/jquery-slimscroll/jquery.slimscroll.js",

                "~/admin/plugins/node-waves/waves.js",
                "~/admin/plugins/jquery-countto/jquery.countTo.js",
                "~/admin/plugins/raphael/raphael.min.js",
                "~/admin/plugins/morrisjs/morris.js",
                "~/admin/plugins/chartjs/Chart.bundle.js",
                "~/admin/plugins/jquery-sparkline/jquery.sparkline.js",
                "~/admin/js/admin.js",
                "~/admin/plugins/jquery-datatable/jquery.dataTables.js",
                "~/admin.plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js",
                "~/admin/js/pages/tables/jquery-datatable.js",
                "~/admin/plugins/autosize/autosize.js",
                "~/admin/js/pages/forms/basic-form-elements.js",
                //"~/admin/js/pages/forms/bootstrap-select.js",
                "~/admin/plugins/momentjs/moment.js",
                "~/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js",
                "~/admin/js/pages/ui/tooltips-popovers.js",
                "~/admin/plugins/node-waves/waves.js",
                "~/admin/plugins/flot-charts/jquery.flot.js",
                "~/admin/plugins/flot-charts/jquery.flot.resize.js",
                 "~/admin/plugins/flot-charts/jquery.flot.pie.js",
                 "~/admin/plugins/flot-charts/jquery.flot.categories.js",
                 "~/admin/plugins/flot-charts/jquery.flot.time.js",
                 "~/admin/plugins/jquery-sparkline/jquery.sparkline.js",
                 "~/admin/js/pages/index.js"
                  
               


                //"~/admin/js/demo.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/datatablejs").Include(
   "~/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js",
   "~/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js",
    "~/admin/plugins/jquery-datatable/extensions/export/jszip.min.js",
    "~/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js",
   "~/admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js",
    "~/admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js",
    "~/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js",
    "~/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"
                ));

        }
    }
}
