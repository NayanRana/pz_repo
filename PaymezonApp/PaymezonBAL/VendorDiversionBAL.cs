﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymezonDAL;

namespace PaymezonBAL
{
  public  class VendorDiversionBAL
    {
      public static List<Company_Detail> listvendorbyapiname(string apiname)
      {
          return VendorDiversionDAL.listvendorbyapiname(apiname);
      }

      public static string addoreditvendorlist(List<VendorDiversion> listdata)
      {
          return VendorDiversionDAL.addoreditvendorlist(listdata);
      }
    }
}
