﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymezonDAL;

namespace PaymezonBAL
{
    public class AdminBAL
    {
        public bool CheckUser(string userName, string pass)
        {
            return AdminDAL.CheckUser(userName, pass);
        }

        public List<Fund_Request> FundRequestlist(string fromdate,string todate)
        {
            return AdminDAL.FundRequestList(fromdate,todate);
        }
        public List<Api_Detail> Api_List()
        {
            return AdminDAL.Api_List();
        }
        public static Api_Detail GetApiByid(int Apiid)
        {
            return new AdminDAL().GetApiByid(Apiid);
        }

        public static Api_Detail getapibyapiname(string Apiname)
        {
            return new AdminDAL().getapibyapiname(Apiname);
        }
        
        public static Fund_Request GetfndByid(long FndId)
        {
            return new AdminDAL().GetfndByid(FndId);
        }
        public static string UpdateAPI(Api_Detail data)
        {
            return new AdminDAL().UpdateAPI(data);
        }
        public static string UpdateAmount(User_Amount data)
        {
            return new AdminDAL().UpdateAmount(data);
        }
        public static string UpdateFund(Fund_Request data)
        {
            return new AdminDAL().UpdateFund(data);
        }
        public static bool DeleteAPI(long Apiid)
        {
            return new AdminDAL().DeleteAPI(Apiid);
        }
        public static bool Deleteuser(long userid)
        {
            return new AdminDAL().Deleteuser(userid);
        }

        public static bool DeleteSupport(long id)
        {
            return new AdminDAL().DeleteSupport(id);
        }
        public static string UpdateReason(Fund_Request data)
        {
            return new AdminDAL().UpdateReason(data);
        }

        public static string UpdateAnswer(Support_Detail data)
        {
            return new AdminDAL().UpdateAnswer(data);
        }
    }
}
