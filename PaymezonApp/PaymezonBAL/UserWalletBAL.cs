﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymezonDAL;

namespace PaymezonBAL
{
   public class UserWalletBAL
    {
       public static List<User_Wallet> ListWallet_By_Id(long ID,string fdate,string tdate)
       {
           return UserWalletDAL.ListWallet_By_Id(ID, fdate, tdate);
       }
       public static List<User_Wallet> ListWallet(string fdate, string tdate)
       {
           return UserWalletDAL.ListWallet(fdate,tdate);
       }

       public static decimal? CheckBalance(long id)
       {
           return UserWalletDAL.GetUserBalance(id);
       }
    }
}
