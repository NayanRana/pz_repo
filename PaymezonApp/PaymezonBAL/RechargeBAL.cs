﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymezonDAL;


namespace PaymezonBAL
{
     public class RechargeBAL
    {
         public static List<Recharge_Detail> RCG_List_byId(long ID, string fromdate, string todate)
         {
             return RechargeDAL.RCG_List_byId(ID,fromdate,todate);
         }
         public static List<Recharge_Detail> pendingBAL()
         {
             return RechargeDAL.pendingBAL();
         }

         public static string changestatus (string TXID, string status)
         {
                     try 
	                {
                        return RechargeDAL.changestatus(TXID, status);  
	                }
	                catch (Exception ex)
	                {

                        return ex.Message;
	                }
                  
         }
         public static string InsertRegBAL(Recharge_Detail rec)
         {
             try
             {
                 return RechargeDAL.InsertRecharge(rec);
             }
             catch (Exception ex)
             {
                 return ex.Message;
             }
         }
    }
}
