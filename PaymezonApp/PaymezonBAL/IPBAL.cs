﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymezonDAL;

namespace PaymezonBAL
{
   public class IPBAL
    {
       public static List<Ip_Address_Detail> ListIP_By_Id(long ID)
       {
           return IPDAL.ListIP_By_Id(ID);
       }
       public static Ip_Address_Detail GetIpByid(int id)
       {
           return new IPDAL().GetIpByid(id);
       }
       public static string UpdateIP(Ip_Address_Detail data)
       {
           return new IPDAL().UpdateIP(data);
       }

       public static bool CheckIP(string userName, string pass)
       {
           return new IPDAL().CheckIP(userName, pass);
       }
    }
}
