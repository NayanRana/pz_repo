﻿using PaymezonDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymezonBAL
{
    public class TransactionBAL
    {
        public static List<Transaction_Table> Transaction_List(long id, string fdate, string tdate)
        {
            return TransactionDAL.Transaction_List(id, fdate, tdate);
        }
        public static List<Transaction_Table> Transaction_Listdata(string fdate,string tdate)
        {
            return TransactionDAL.Transaction_Listdata(fdate, tdate);
        }
        public static List<Transaction_Table> Transaction_Listforvishnuapi()
        {
            return TransactionDAL.Transaction_Listforvishnuapi();
        }
        public static string updateapiidbytxnid(string txnid, string apiid)
        {
            return TransactionDAL.updateapiidbytxnid(txnid, apiid);
        }

        public static List<Transaction_Table> checkpendingtransaction()
        {
            return TransactionDAL.checkpendingtransaction();
        }

        public static List<Transaction_Table> checkpendingtransactionofanshapi()
        {
            return TransactionDAL.checkpendingtransactionofanshapi();
        }
        public static string checkRech(decimal mobile, decimal amt)
        {
            return TransactionDAL.checkRech(mobile, amt);
        }

        public static string UpdateStatus(string status, string txnId,string opId)
        {
            return TransactionDAL.UpdateStatus(status, txnId,opId);
        }
        public static string UpdateStatusOfAnsh(string status, string Mobno)
        {
            return TransactionDAL.UpdateStatusOfAnsh(status, Mobno);
        }
        public static string Getrequrl(string txid)
        {
            return TransactionDAL.Getrequrl(txid);
        }
        public static string Getctxid(string txid)
        {
            return TransactionDAL.Getctxid(txid);
        }
        public static string Gettxnidbyapitxnid(string mobno)
        {
            return TransactionDAL.Gettxnidbyapitxnid(mobno);
        }

        public static string gettxnbymobno(string mobno)
        {
            return TransactionDAL.gettxnbymobno(mobno);
        }

        public static Transaction_Table CheckStatusbyctxnid(string ctxnid)
        {
            return TransactionDAL.CheckStatusbyctxnid(ctxnid);
        }

    }
}
