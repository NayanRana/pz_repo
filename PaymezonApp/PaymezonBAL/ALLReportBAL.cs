﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymezonDAL;
using System.IO;
using System.Web;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace PaymezonBAL
{
    public class ALLReportBAL
    {

        public static object AccountStatement_ById(long Id)
        {
            return ALLReportDAL.AccountStatement_ById(Id);
        }
        public static object AccountStatement()
        {
            return ALLReportDAL.AccountStatement();
        }
        public static List<Vendor_InptOutPut_ReqDetails> jioresponsereport(string fdate, string tdate)
        {
            return ALLReportDAL.jioresponsereport(fdate,tdate);
        }


        public static List<Transaction_Table> listtransaction()
        {
            return ALLReportDAL.listtransaction();
        }
        public static List<Transaction_Table> opwise_by_id(long Id)
        {
            return ALLReportDAL.opwise_by_id(Id);
        }
        public static List<Transaction_Table> account_report_byID(long Id)
        {
            return ALLReportDAL.account_report_byID(Id);
        }
        public static List<User_Wallet> ListWallet_By_Id(long Id,string fdate,string tdate)
        {
            return ALLReportDAL.ListWallet_By_Id(Id, fdate, tdate);
        }
      
    }
}
