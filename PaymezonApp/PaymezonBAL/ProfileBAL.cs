﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymezonDAL;


namespace PaymezonBAL
{
   public class ProfileBAL
    {
       public static UserRegistration Editprofile(long ID)
       {
           return new ProfileDAL().Editprofile(ID);
       }
       public static string Updateprofile(UserRegistration data)
       {
           return new ProfileDAL().Updateprofile(data);
       }
     
    }
}
