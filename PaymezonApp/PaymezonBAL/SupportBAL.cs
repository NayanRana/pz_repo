﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymezonDAL;

namespace PaymezonBAL
{
  public class SupportBAL
    {
      public static List<Support_Detail> MSG_List_byId(long ID,string fromdate,string todate)
      {
          return SupportDAL.MSG_List_byId(ID,fromdate,todate);
      }

      public static List<Support_Detail> MSG_LIST(string fromdate)
      {
          return SupportDAL.MSG_List(fromdate);
      }
      public static List<Support_Detail> MSG_LIST_Noti(int display, int lenght)
      {
          return SupportDAL.MSG_List_Noti(display, lenght);
      }
      public static List<Support_Detail> GetsinglesupportByid(string TokenId)
      {
          return new SupportDAL().GetsinglesupportByid(TokenId);
      }
    }
}
