﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymezonDAL;

namespace PaymezonBAL
{
    public class AssignApiBAL
    {
        public static List<Assign_Api> ListAssignApi()
        {
            return AssignApiDAL.ListAssignApi();
        }
        public static List<VendorDiversion> VendorList()
        {
            return AssignApiDAL.VendorList();
        }
      
        public static List<Api_Detail> ListApi()
        {
            return AssignApiDAL.ListApi();
        }
        public static bool checkoperatorforassign(int? id)
        {
            return AssignApiDAL.checkoperatorforassign(id);
        }

        public static bool checkoperatorforalreadyassign(int? opid)
        {
            return AssignApiDAL.checkoperatorforalreadyassign(opid);
        }
        public static int getvendoridbycmpnyid(int? id)
        {
            return AssignApiDAL.getvendoridbycmpnyid(id);
        }
        public static Assign_Api GetAssignApiByid(int id)
        {
            return new AssignApiDAL().GetAssignApiByid(id);
        }
        public static string AddAssignApi(Assign_Api data)
        {
            return new AssignApiDAL().AddAssignApi(data);
        }
        public static string UpdateAssignApi(Assign_Api data)
        {
            return new AssignApiDAL().UpdateAssignApi(data);
        }
        public static bool DeleteAssignApi(long id)
        {
            return new AssignApiDAL().DeleteAssignApi(id);
        }

        public static string ApiDetail(decimal amt, string opId)
        {
            return AssignApiDAL.GetApiDetail(amt, opId);
        }
    }
}
