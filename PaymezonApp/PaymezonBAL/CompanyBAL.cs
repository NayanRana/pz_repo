﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymezonDAL;

namespace PaymezonBAL
{
    public class CompanyBAL
    {
        public static List<Company_Detail> CompanyList()
        {
            return CompanyDAL.Company_List();
        }

        public static List<Company_Detail> CompanyListorderbyname()
        {
            return CompanyDAL.CompanyListorderbyname();
        }

        public static Company_Detail GetCompanyByid(int id)
        {
            return new CompanyDAL().GetCompanyByid(id);
        }
        public static bool DeleteCompany(long id)
        {
            return new CompanyDAL().DeleteCompany(id);
        }
        public static string UpdateCompany(Company_Detail data)
        {
            return new CompanyDAL().UpdateCompany(data);
        }
    }
}
