﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymezonDAL;

namespace PaymezonBAL
{
    public class FundRequestBAL
    {
        public static List<Fund_Request> Fund_Request_List_byId(long ID, string fromdate, string todate)
        {
            return FundRequestDAL.Fund_Request_List_byId(ID,fromdate,todate);
        }

        public static UserRegistration usr_detail_byId(long ID)
        {
            return FundRequestDAL.usr_detail_byId(ID);
        }

        public static List<Fund_Request> Success_Fund_List(string fdate, string tdate)
        {
            return FundRequestDAL.Success_Fund_List(fdate,tdate);
        }

    }
}
