﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymezonDAL;

namespace PaymezonBAL
{
  public  class CommisionBAL
    {
      public static List<commision_Detail> List_Commision()
      {
          return CommisionDAL.List_Commision();
      }
      public static List<commision_Detail> List_CommisionByPlanname()
      {
          return CommisionDAL.List_CommisionByPlanname();
      }
      public static List<commision_Detail> List_CommisionByPackage(long ID)
      {
          return CommisionDAL.List_CommisionByPackage(ID);
      }

      public static List<commision_Detail> updatecommbyid(long userid)
      {
          return CommisionDAL.updatecommbyid(userid);
      }

      public static List<Company_Detail> getcommisionlistbyplanid(long planid)
      {
          return CommisionDAL.getcommisionlistbyplanid(planid);
      }

      public static List<Company_Detail> getcommisionlistbyuserid(long userid)
      {
          return CommisionDAL.getcommisionlistbyuserid(userid);
      }

      public static string Editcommision(commision_Detail commision)
      {
          return CommisionDAL.Editcommision(commision);

      }

      public static string editcommisionlist(List<commision_Detail> commision)
      {
          return CommisionDAL.editcommisionlist(commision);

      }

      public static int getplanidbyuserid (long userid)
      {
          return CommisionDAL.getplanidbyuserid(userid);
      }
      public static string addnewcommisionfornewuser(int userid)
      {
          return CommisionDAL.addnewcommisionfornewuser(userid);
      }
    }
}
